<CalcPaths_Export_File>
	<Simulator after_time="0" max_steps="10000" sample_time="0.01" simulation_rate="10" simulation_ratio="1" start_time="1984-01-01T00:00:00.000" stop_condition="3" stop_time="1984-01-01T00:00:00.000"/>
	<Paths>
		<PATH ID="2" block_id="-1" index="-1" name="Path 1">
			<Paths>
				<PATH ID="9" block_id="7" index="-1" name="Subpath 1">
					<Paths/>
					<Objects>
						<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="33"/>
									<Property name="Top" type="int" value="74"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="48"/>
									<Property name="Top" type="int" value="165"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="3"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="48"/>
									<Property name="Top" type="int" value="212"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="3" flip="false" index="3" name="dCV" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="i1[0]-i1[1]"/>
									<Property name="Inputs" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="162"/>
									<Property name="Top" type="int" value="39"/>
									<Property name="Width" type="int" value="80"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="5" flip="false" index="4" name="a" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="1/(1-i1[0]-i2[0])"/>
									<Property name="Inputs" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="289"/>
									<Property name="Top" type="int" value="389"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="8" flip="false" index="5" name="b1" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="1-1/(1-i1[0]-i2[0])"/>
									<Property name="Inputs" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="265"/>
									<Property name="Top" type="int" value="479"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="9" flip="false" index="6" name="b2" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="-i2[0]/(1-i1[0]-i2[0])"/>
									<Property name="Inputs" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="265"/>
									<Property name="Top" type="int" value="537"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="10" flip="false" index="7" name="Lfun1" type="logical">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs number" type="int" value="2"/>
									<Property name="Type" type="enum" value="0">
										<Enumeration index="0" name="AND"/>
										<Enumeration index="1" name="NAND"/>
										<Enumeration index="2" name="NOR"/>
										<Enumeration index="3" name="NOT"/>
										<Enumeration index="4" name="OR"/>
										<Enumeration index="5" name="XOR"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="761"/>
									<Property name="Top" type="int" value="148"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="11" flip="false" index="8" name="Lfun3" type="logical">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs number" type="int" value="2"/>
									<Property name="Type" type="enum" value="0">
										<Enumeration index="0" name="AND"/>
										<Enumeration index="1" name="NAND"/>
										<Enumeration index="2" name="NOR"/>
										<Enumeration index="3" name="NOT"/>
										<Enumeration index="4" name="OR"/>
										<Enumeration index="5" name="XOR"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="766"/>
									<Property name="Top" type="int" value="304"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="14" flip="false" index="9" name="b3" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="i1[0]+i2[0]/(1-i1[0]-i2[0])"/>
									<Property name="Inputs" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="264"/>
									<Property name="Top" type="int" value="615"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="13" flip="false" index="10" name="b4" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="i2[0]/(1-i1[0]-i2[0])"/>
									<Property name="Inputs" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="265"/>
									<Property name="Top" type="int" value="690"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="15" flip="false" index="11" name="Pochodna ujemna" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="4">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="350"/>
									<Property name="Top" type="int" value="91"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="12" flip="false" index="12" name="Pochodna  dodatnia" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="348"/>
									<Property name="Top" type="int" value="33"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="16" flip="false" index="13" name="" type="constant">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Value" type="real" value="0"/>
									<Property name="Min" type="real" value="0"/>
									<Property name="Max" type="real" value="1"/>
									<Property name="Limit output" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="247"/>
									<Property name="Top" type="int" value="127"/>
									<Property name="Width" type="int" value="40"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="17" flip="false" index="14" name="alfa+beta" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="1"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="347"/>
									<Property name="Top" type="int" value="181"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="18" flip="false" index="15" name="Sum 1" type="sum">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Signs" type="string" value="++"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="264"/>
									<Property name="Top" type="int" value="178"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="19" flip="false" index="16" name="rosnacaPlus" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="602"/>
									<Property name="Top" type="int" value="111"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="20" flip="false" index="17" name="CV" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="1"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="435"/>
									<Property name="Top" type="int" value="120"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="21" flip="false" index="18" name="CV" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="1"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="577"/>
									<Property name="Top" type="int" value="400"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="22" flip="false" index="19" name="A*x" type="product">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Actions string" type="string" value="**"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="666"/>
									<Property name="Top" type="int" value="429"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="23" flip="false" index="20" name="A*x+B1" type="sum">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Signs" type="string" value="++"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="761"/>
									<Property name="Top" type="int" value="464"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="24" flip="false" index="21" name="Ax+B2" type="sum">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Signs" type="string" value="++"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="759"/>
									<Property name="Top" type="int" value="531"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="25" flip="false" index="22" name="Ax+B3" type="sum">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Signs" type="string" value="++"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="758"/>
									<Property name="Top" type="int" value="590"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="26" flip="false" index="23" name="Ax+B4" type="sum">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Signs" type="string" value="++"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="757"/>
									<Property name="Top" type="int" value="658"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="33" flip="false" index="24" name="Signal limiter 1" type="signal_limiter">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Low limit" type="real" value="-1"/>
									<Property name="High limit" type="real" value="1"/>
									<Property name="Rising rate limit" type="real" value="0"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1718"/>
									<Property name="Top" type="int" value="396"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="34" flip="false" index="25" name="malejacaPlus" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="595"/>
									<Property name="Top" type="int" value="186"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="35" flip="false" index="26" name="Lfun2" type="logical">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs number" type="int" value="2"/>
									<Property name="Type" type="enum" value="0">
										<Enumeration index="0" name="AND"/>
										<Enumeration index="1" name="NAND"/>
										<Enumeration index="2" name="NOR"/>
										<Enumeration index="3" name="NOT"/>
										<Enumeration index="4" name="OR"/>
										<Enumeration index="5" name="XOR"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="763"/>
									<Property name="Top" type="int" value="231"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="36" flip="false" index="27" name="rosnacaMinus" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="4">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="603"/>
									<Property name="Top" type="int" value="267"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="37" flip="false" index="28" name="malejacaMinus" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="4">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="599"/>
									<Property name="Top" type="int" value="332"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="38" flip="false" index="29" name="-beta" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="-1"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="313"/>
									<Property name="Top" type="int" value="285"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="39" flip="false" index="30" name="-(alfa+beta)" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="-1"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="445"/>
									<Property name="Top" type="int" value="347"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="40" flip="false" index="31" name="Lfun4" type="logical">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs number" type="int" value="2"/>
									<Property name="Type" type="enum" value="0">
										<Enumeration index="0" name="AND"/>
										<Enumeration index="1" name="NAND"/>
										<Enumeration index="2" name="NOR"/>
										<Enumeration index="3" name="NOT"/>
										<Enumeration index="4" name="OR"/>
										<Enumeration index="5" name="XOR"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="768"/>
									<Property name="Top" type="int" value="383"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="41" flip="false" index="32" name="u" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="1825"/>
									<Property name="Top" type="int" value="392"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="49"/>
									<Segment index="1" value="-30"/>
									<Segment index="2" value="55"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="8" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="73"/>
									<Segment index="1" value="317"/>
									<Segment index="2" value="119"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="8" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="83"/>
									<Segment index="1" value="283"/>
									<Segment index="2" value="109"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="5" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="63"/>
									<Segment index="1" value="227"/>
									<Segment index="2" value="153"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="5" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="184"/>
									<Segment index="1" value="193"/>
									<Segment index="2" value="32"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="9" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="375"/>
									<Segment index="2" value="136"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="9" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="166"/>
									<Segment index="1" value="341"/>
									<Segment index="2" value="26"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="14" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="453"/>
									<Segment index="2" value="135"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="14" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="151"/>
									<Segment index="1" value="419"/>
									<Segment index="2" value="40"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="13" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="528"/>
									<Segment index="2" value="136"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="13" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="151"/>
									<Segment index="1" value="494"/>
									<Segment index="2" value="41"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="3" begin_idx="0" end_id="12" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="83"/>
									<Segment index="1" value="-6"/>
									<Segment index="2" value="28"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="16" begin_idx="0" end_id="12" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="46"/>
									<Segment index="1" value="-79"/>
									<Segment index="2" value="20"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="3" begin_idx="0" end_id="15" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="83"/>
									<Segment index="1" value="52"/>
									<Segment index="2" value="30"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="16" begin_idx="0" end_id="15" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="46"/>
									<Segment index="1" value="-21"/>
									<Segment index="2" value="22"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="18" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="176"/>
									<Segment index="1" value="16"/>
									<Segment index="2" value="15"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="18" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="151"/>
									<Segment index="1" value="-18"/>
									<Segment index="2" value="40"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="18" begin_idx="0" end_id="17" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="22"/>
									<Segment index="1" value="-2"/>
									<Segment index="2" value="6"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="20" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="93"/>
									<Segment index="1" value="51"/>
									<Segment index="2" value="284"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="20" begin_idx="0" end_id="19" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="84"/>
									<Segment index="1" value="-9"/>
									<Segment index="2" value="28"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="17" begin_idx="0" end_id="19" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="148"/>
									<Segment index="1" value="-55"/>
									<Segment index="2" value="52"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="20" begin_idx="0" end_id="21" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="53"/>
									<Segment index="1" value="280"/>
									<Segment index="2" value="34"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="21" begin_idx="0" end_id="22" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="19"/>
									<Segment index="1" value="27"/>
									<Segment index="2" value="15"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="5" begin_idx="0" end_id="22" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="120"/>
									<Segment index="1" value="46"/>
									<Segment index="2" value="142"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="22" begin_idx="0" end_id="23" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="17"/>
									<Segment index="1" value="28"/>
									<Segment index="2" value="23"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="8" begin_idx="0" end_id="23" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="190"/>
									<Segment index="1" value="-9"/>
									<Segment index="2" value="191"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="13" begin_idx="0" end_id="26" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="187"/>
									<Segment index="1" value="-26"/>
									<Segment index="2" value="190"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="14" begin_idx="0" end_id="25" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="186"/>
									<Segment index="1" value="-19"/>
									<Segment index="2" value="193"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="9" begin_idx="0" end_id="24" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="186"/>
									<Segment index="1" value="0"/>
									<Segment index="2" value="193"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="22" begin_idx="0" end_id="24" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="17"/>
									<Segment index="1" value="95"/>
									<Segment index="2" value="21"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="22" begin_idx="0" end_id="25" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="17"/>
									<Segment index="1" value="154"/>
									<Segment index="2" value="20"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="22" begin_idx="0" end_id="26" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="17"/>
									<Segment index="1" value="222"/>
									<Segment index="2" value="19"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="38" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="151"/>
									<Segment index="1" value="78"/>
									<Segment index="2" value="89"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="38" begin_idx="0" end_id="36" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="136"/>
									<Segment index="1" value="-3"/>
									<Segment index="2" value="99"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="20" begin_idx="0" end_id="36" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="40"/>
									<Segment index="1" value="147"/>
									<Segment index="2" value="73"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="17" begin_idx="0" end_id="39" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="25"/>
									<Segment index="1" value="166"/>
									<Segment index="2" value="18"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="39" begin_idx="0" end_id="37" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="47"/>
									<Segment index="1" value="0"/>
									<Segment index="2" value="52"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="12" begin_idx="0" end_id="11" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="323"/>
									<Segment index="1" value="264"/>
									<Segment index="2" value="40"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="15" begin_idx="0" end_id="40" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="295"/>
									<Segment index="1" value="285"/>
									<Segment index="2" value="68"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="36" begin_idx="0" end_id="11" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="21"/>
									<Segment index="1" value="45"/>
									<Segment index="2" value="87"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="37" begin_idx="0" end_id="40" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="89"/>
									<Segment index="1" value="59"/>
									<Segment index="2" value="25"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="20" begin_idx="0" end_id="37" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="27"/>
									<Segment index="1" value="212"/>
									<Segment index="2" value="82"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="19" begin_idx="0" end_id="10" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="62"/>
									<Segment index="1" value="45"/>
									<Segment index="2" value="42"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="15" begin_idx="0" end_id="35" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="156"/>
									<Segment index="1" value="133"/>
									<Segment index="2" value="202"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="34" begin_idx="0" end_id="35" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="71"/>
									<Segment index="1" value="53"/>
									<Segment index="2" value="42"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="33" begin_idx="0" end_id="41" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="36"/>
									<Segment index="1" value="-9"/>
									<Segment index="2" value="16"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="12" begin_idx="0" end_id="10" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="306"/>
									<Segment index="1" value="108"/>
									<Segment index="2" value="52"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="20" begin_idx="0" end_id="34" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="48"/>
									<Segment index="1" value="66"/>
									<Segment index="2" value="57"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="34" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="267"/>
									<Segment index="1" value="-6"/>
									<Segment index="2" value="255"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
			</Paths>
			<Objects>
				<Object ID="1" flip="false" index="0" name="Constant 1" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0.1"/>
							<Property name="Min" type="real" value="0"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="169"/>
							<Property name="Top" type="int" value="157"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="2" flip="false" index="1" name="Constant 2" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0.05"/>
							<Property name="Min" type="real" value="0"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="171"/>
							<Property name="Top" type="int" value="205"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="3" flip="false" index="2" name="Time event 1" type="src_time_event">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Start time" type="real" value="1"/>
							<Property name="Stop time" type="real" value="6"/>
							<Property name="Amplitude" type="real" value="3"/>
							<Property name="Bias" type="real" value="-1.5"/>
							<Property name="Kind" type="enum" value="1">
								<Enumeration index="0" name="impulse"/>
								<Enumeration index="1" name="ramp"/>
								<Enumeration index="2" name="step"/>
							</Property>
							<Property name="Reset time" type="real" value="12"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="129"/>
							<Property name="Top" type="int" value="45"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="4" flip="false" index="3" name="Time event 2" type="src_time_event">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Start time" type="real" value="6"/>
							<Property name="Stop time" type="real" value="11"/>
							<Property name="Amplitude" type="real" value="-3"/>
							<Property name="Bias" type="real" value="0"/>
							<Property name="Kind" type="enum" value="1">
								<Enumeration index="0" name="impulse"/>
								<Enumeration index="1" name="ramp"/>
								<Enumeration index="2" name="step"/>
							</Property>
							<Property name="Reset time" type="real" value="12"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="127"/>
							<Property name="Top" type="int" value="96"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="5" flip="false" index="4" name="Sum 1" type="sum">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Signs" type="string" value="++"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="299"/>
							<Property name="Top" type="int" value="40"/>
							<Property name="Width" type="int" value="60"/>
							<Property name="Height" type="int" value="60"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="6" flip="false" index="5" name="Display 1" type="display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="120"/>
							<Property name="Inputs count" type="uint" value="2"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="730"/>
							<Property name="Top" type="int" value="77"/>
							<Property name="Width" type="int" value="170"/>
							<Property name="Height" type="int" value="60"/>
							<Property name="Chart min" type="real" value="-1.5"/>
							<Property name="Chart max" type="real" value="1.5"/>
							<Property name="Autoscale" type="bool" value="false"/>
							<Property name="Scale each sep." type="bool" value="false"/>
							<Property name="Precision" type="int" value="3"/>
							<Property name="Digits" type="int" value="3"/>
							<Property name="Type" type="enum" value="1">
								<Enumeration index="0" name="Fixed"/>
								<Enumeration index="1" name="General"/>
								<Enumeration index="2" name="Scientific"/>
							</Property>
							<Property name="Bkg color" type="color" value="0"/>
							<Property name="Axes color" type="color" value="8421504"/>
							<Property name="Bad sample color" type="color" value="255"/>
							<Property name="Show sample nr" type="bool" value="false"/>
							<Property name="Color scheme" type="enum" value="0">
								<Enumeration index="0" name="dark"/>
								<Enumeration index="1" name="light"/>
							</Property>
						</Properties>
					</Display>
				</Object>
				<Object ID="8" flip="false" index="6" name="XY Display 1" type="xy_display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="2000"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="786"/>
							<Property name="Top" type="int" value="260"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="66"/>
							<Property name="X axis min" type="real" value="-0.779999999999999"/>
							<Property name="X axis max" type="real" value="0.779999999999974"/>
							<Property name="Y axis min" type="real" value="-1"/>
							<Property name="Y axis max" type="real" value="1"/>
							<Property name="Chart bkg color" type="color" value="16777215"/>
							<Property name="Chart axes color" type="color" value="12632256"/>
							<Property name="Chart plot color" type="color" value="0"/>
							<Property name="Chart bad sample color" type="color" value="255"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="7" flip="false" index="7" name="Subpath 1" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="400"/>
							<Property name="Top" type="int" value="133"/>
							<Property name="Width" type="int" value="200"/>
							<Property name="Height" type="int" value="75"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="9" flip="false" index="8" name="Gain 1" type="gain">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Gain" type="real" value="0.5"/>
							<Property name="Clear sig. status" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="377"/>
							<Property name="Top" type="int" value="56"/>
							<Property name="Width" type="int" value="60"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
			</Objects>
			<Connections>
				<Connection begin_id="3" begin_idx="0" end_id="5" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="65"/>
							<Segment index="1" value="0"/>
							<Segment index="2" value="10"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="4" begin_idx="0" end_id="5" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="43"/>
							<Segment index="1" value="-31"/>
							<Segment index="2" value="34"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="69"/>
							<Segment index="1" value="-3"/>
							<Segment index="2" value="67"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="2" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="68"/>
							<Segment index="1" value="-33"/>
							<Segment index="2" value="66"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="7" begin_idx="0" end_id="6" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="68"/>
							<Segment index="1" value="-73"/>
							<Segment index="2" value="67"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="7" begin_idx="0" end_id="8" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="96"/>
							<Segment index="1" value="134"/>
							<Segment index="2" value="95"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="5" begin_idx="0" end_id="9" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="11"/>
							<Segment index="1" value="1"/>
							<Segment index="2" value="12"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="7" end_idx="0" type="1">
					<Display type="1">
						<Segments size="5">
							<Segment index="0" value="10"/>
							<Segment index="1" value="40"/>
							<Segment index="2" value="-50"/>
							<Segment index="3" value="40"/>
							<Segment index="4" value="8"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="6" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="149"/>
							<Segment index="1" value="46"/>
							<Segment index="2" value="149"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="8" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="176"/>
							<Segment index="1" value="211"/>
							<Segment index="2" value="178"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
			</Connections>
		</PATH>
	</Paths>
</CalcPaths_Export_File>
