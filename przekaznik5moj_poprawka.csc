<CalcPaths_Export_File>
	<Simulator after_time="0" max_steps="10000" sample_time="0.01" simulation_rate="10" simulation_ratio="1" start_time="1984-01-01T00:00:00.000" stop_condition="3" stop_time="1984-01-01T00:00:00.000"/>
	<Paths>
		<PATH ID="2" block_id="-1" index="-1" name="Path 1">
			<Paths>
				<PATH ID="9" block_id="7" index="-1" name="Subpath 1">
					<Paths>
						<PATH ID="4" block_id="46" index="-1" name="przypadekXmniejszyOd1dlaPlus">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="65"/>
											<Property name="Top" type="int" value="140"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="659"/>
											<Property name="Top" type="int" value="165"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="57"/>
											<Property name="Top" type="int" value="216"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="+-"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="187"/>
											<Property name="Top" type="int" value="135"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="63"/>
											<Property name="Top" type="int" value="296"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="a" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="367"/>
											<Property name="Top" type="int" value="80"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="6" name="b2" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="-i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="369"/>
											<Property name="Top" type="int" value="204"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="7" name="Sum 2" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="551"/>
											<Property name="Top" type="int" value="155"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="50"/>
											<Segment index="1" value="5"/>
											<Segment index="2" value="47"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="3" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="48"/>
											<Segment index="1" value="-51"/>
											<Segment index="2" value="57"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="40"/>
											<Segment index="1" value="-63"/>
											<Segment index="2" value="85"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="211"/>
											<Segment index="1" value="-182"/>
											<Segment index="2" value="68"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="147"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="140"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="7" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="235"/>
											<Segment index="1" value="-58"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="23"/>
											<Segment index="1" value="55"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="8" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="21"/>
											<Segment index="1" value="-56"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="1" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="10"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="43"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="6" block_id="52" index="-1" name="przypadekXmniejszyOd1dlaMinus">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="65"/>
											<Property name="Top" type="int" value="140"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="659"/>
											<Property name="Top" type="int" value="165"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="70"/>
											<Property name="Top" type="int" value="216"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="176"/>
											<Property name="Top" type="int" value="144"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="71"/>
											<Property name="Top" type="int" value="296"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="a" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="367"/>
											<Property name="Top" type="int" value="80"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="6" name="Sum 2" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="551"/>
											<Property name="Top" type="int" value="155"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="7" name="Math expression 1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="393"/>
											<Property name="Top" type="int" value="204"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="8" begin_idx="0" end_id="1" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="10"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="43"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="43"/>
											<Segment index="1" value="14"/>
											<Segment index="2" value="43"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="3" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="40"/>
											<Segment index="1" value="-42"/>
											<Segment index="2" value="41"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="68"/>
											<Segment index="1" value="-72"/>
											<Segment index="2" value="68"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="228"/>
											<Segment index="1" value="-182"/>
											<Segment index="2" value="43"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="10" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="255"/>
											<Segment index="1" value="-58"/>
											<Segment index="2" value="42"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="8" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="10"/>
											<Segment index="1" value="-56"/>
											<Segment index="2" value="33"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="34"/>
											<Segment index="1" value="55"/>
											<Segment index="2" value="35"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="139"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="159"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="5" block_id="56" index="-1" name="Krzywa">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="A" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="251"/>
											<Property name="Top" type="int" value="89"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="B1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1-1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="254"/>
											<Property name="Top" type="int" value="190"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="B2" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="-i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="256"/>
											<Property name="Top" type="int" value="292"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="B3" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="(i1[0]+i2[0])/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="257"/>
											<Property name="Top" type="int" value="397"/>
											<Property name="Width" type="int" value="140"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="B4" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="253"/>
											<Property name="Top" type="int" value="509"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="5" name="Ax+B1" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="875"/>
											<Property name="Top" type="int" value="112"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="6" name="Ax+B2" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="883"/>
											<Property name="Top" type="int" value="186"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="7" name="Ax+B3" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="875"/>
											<Property name="Top" type="int" value="291"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="8" name="Ax+B4" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="4"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="892"/>
											<Property name="Top" type="int" value="488"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="9" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="590"/>
											<Property name="Top" type="int" value="107"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="10" name="Sum 2" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="615"/>
											<Property name="Top" type="int" value="257"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="11" name="Sum 3" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="593"/>
											<Property name="Top" type="int" value="375"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="14" flip="false" index="12" name="Sum 4" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="604"/>
											<Property name="Top" type="int" value="474"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="15" flip="false" index="13" name="Product 1" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="447"/>
											<Property name="Top" type="int" value="43"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="16" flip="false" index="14" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="28"/>
											<Property name="Top" type="int" value="77"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="17" flip="false" index="15" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="24"/>
											<Property name="Top" type="int" value="198"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="18" flip="false" index="16" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="101"/>
											<Property name="Top" type="int" value="460"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="0" begin_idx="0" end_id="15" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="31"/>
											<Segment index="1" value="-53"/>
											<Segment index="2" value="50"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="11" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="116"/>
											<Segment index="1" value="-90"/>
											<Segment index="2" value="105"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="12" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="120"/>
											<Segment index="1" value="-42"/>
											<Segment index="2" value="124"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="13" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="109"/>
											<Segment index="1" value="-29"/>
											<Segment index="2" value="92"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="14" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="141"/>
											<Segment index="1" value="-42"/>
											<Segment index="2" value="95"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="11" begin_idx="0" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="61"/>
											<Segment index="1" value="-5"/>
											<Segment index="2" value="169"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="44"/>
											<Segment index="1" value="-81"/>
											<Segment index="2" value="169"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="13" begin_idx="0" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="70"/>
											<Segment index="1" value="-94"/>
											<Segment index="2" value="157"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="14" begin_idx="0" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="74"/>
											<Segment index="1" value="4"/>
											<Segment index="2" value="159"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="1" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="151"/>
											<Segment index="1" value="4"/>
											<Segment index="2" value="54"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="0" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="134"/>
											<Segment index="1" value="-97"/>
											<Segment index="2" value="68"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="2" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="144"/>
											<Segment index="1" value="106"/>
											<Segment index="2" value="63"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="126"/>
											<Segment index="1" value="211"/>
											<Segment index="2" value="82"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="178"/>
											<Segment index="1" value="323"/>
											<Segment index="2" value="26"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="0" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="34"/>
											<Segment index="1" value="-337"/>
											<Segment index="2" value="91"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="1" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="86"/>
											<Segment index="1" value="-236"/>
											<Segment index="2" value="42"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="3" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="79"/>
											<Segment index="1" value="-29"/>
											<Segment index="2" value="52"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="4" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="80"/>
											<Segment index="1" value="83"/>
											<Segment index="2" value="47"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="2" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="66"/>
											<Segment index="1" value="-134"/>
											<Segment index="2" value="64"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="15" begin_idx="0" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="22"/>
											<Segment index="1" value="57"/>
											<Segment index="2" value="66"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="16" begin_idx="0" end_id="15" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="173"/>
											<Segment index="1" value="-31"/>
											<Segment index="2" value="221"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="15" begin_idx="0" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="45"/>
											<Segment index="1" value="325"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="15" begin_idx="0" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="56"/>
											<Segment index="1" value="207"/>
											<Segment index="2" value="57"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="15" begin_idx="0" end_id="14" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="51"/>
											<Segment index="1" value="424"/>
											<Segment index="2" value="51"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="7" block_id="57" index="-1" name="pochodna">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="106"/>
											<Property name="Top" type="int" value="171"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="dodatni" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="627"/>
											<Property name="Top" type="int" value="163"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="ujemna" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="630"/>
											<Property name="Top" type="int" value="218"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="Constant 1" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="166"/>
											<Property name="Top" type="int" value="294"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="dCV" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i1[0]-i1[1]"/>
											<Property name="Inputs" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="180"/>
											<Property name="Top" type="int" value="166"/>
											<Property name="Width" type="int" value="80"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="348"/>
											<Property name="Top" type="int" value="107"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="347"/>
											<Property name="Top" type="int" value="211"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="0" begin_idx="0" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="24"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="25"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="46"/>
											<Segment index="1" value="-59"/>
											<Segment index="2" value="47"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="6" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="45"/>
											<Segment index="1" value="45"/>
											<Segment index="2" value="47"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="43"/>
											<Segment index="1" value="-172"/>
											<Segment index="2" value="44"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="6" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="42"/>
											<Segment index="1" value="-68"/>
											<Segment index="2" value="44"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="1" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="111"/>
											<Segment index="1" value="44"/>
											<Segment index="2" value="113"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="2" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="113"/>
											<Segment index="1" value="-5"/>
											<Segment index="2" value="115"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="8" block_id="58" index="-1" name="przedzialy">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="44"/>
											<Property name="Top" type="int" value="122"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="48"/>
											<Property name="Top" type="int" value="220"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="53"/>
											<Property name="Top" type="int" value="276"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="396"/>
											<Property name="Top" type="int" value="92"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="396"/>
											<Property name="Top" type="int" value="164"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Relational operator 3" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="396"/>
											<Property name="Top" type="int" value="280"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="Relational operator 4" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="395"/>
											<Property name="Top" type="int" value="355"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="123"/>
											<Property name="Top" type="int" value="217"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="CV>alfa+beta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="730"/>
											<Property name="Top" type="int" value="105"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="CV>beta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="726"/>
											<Property name="Top" type="int" value="178"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="CV<-beta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="725"/>
											<Property name="Top" type="int" value="256"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="CV<-(alfa+beta)" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="4"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="725"/>
											<Property name="Top" type="int" value="330"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="Gain 1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="-1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="150"/>
											<Property name="Top" type="int" value="319"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="13" name="Gain 2" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="-1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="301"/>
											<Property name="Top" type="int" value="390"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="14" flip="false" index="14" name="Display 1" type="display">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer length" type="uint" value="900"/>
											<Property name="Inputs count" type="uint" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="183"/>
											<Property name="Top" type="int" value="482"/>
											<Property name="Width" type="int" value="170"/>
											<Property name="Height" type="int" value="75"/>
											<Property name="Chart min" type="real" value="-1.05"/>
											<Property name="Chart max" type="real" value="1.05"/>
											<Property name="Autoscale" type="bool" value="false"/>
											<Property name="Scale each sep." type="bool" value="false"/>
											<Property name="Precision" type="int" value="3"/>
											<Property name="Digits" type="int" value="3"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="Fixed"/>
												<Enumeration index="1" name="General"/>
												<Enumeration index="2" name="Scientific"/>
											</Property>
											<Property name="Bkg color" type="color" value="0"/>
											<Property name="Axes color" type="color" value="8421504"/>
											<Property name="Bad sample color" type="color" value="255"/>
											<Property name="Show sample nr" type="bool" value="false"/>
											<Property name="Color scheme" type="enum" value="0">
												<Enumeration index="0" name="dark"/>
												<Enumeration index="1" name="light"/>
											</Property>
										</Properties>
									</Display>
								</Object>
								<Object ID="15" flip="false" index="15" name="Display 2" type="display">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer length" type="uint" value="900"/>
											<Property name="Inputs count" type="uint" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="704"/>
											<Property name="Top" type="int" value="208"/>
											<Property name="Width" type="int" value="170"/>
											<Property name="Height" type="int" value="45"/>
											<Property name="Chart min" type="real" value="0"/>
											<Property name="Chart max" type="real" value="1"/>
											<Property name="Autoscale" type="bool" value="false"/>
											<Property name="Scale each sep." type="bool" value="false"/>
											<Property name="Precision" type="int" value="3"/>
											<Property name="Digits" type="int" value="3"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="Fixed"/>
												<Enumeration index="1" name="General"/>
												<Enumeration index="2" name="Scientific"/>
											</Property>
											<Property name="Bkg color" type="color" value="0"/>
											<Property name="Axes color" type="color" value="8421504"/>
											<Property name="Bad sample color" type="color" value="255"/>
											<Property name="Show sample nr" type="bool" value="false"/>
											<Property name="Color scheme" type="enum" value="0">
												<Enumeration index="0" name="dark"/>
												<Enumeration index="1" name="light"/>
											</Property>
										</Properties>
									</Display>
								</Object>
								<Object ID="16" flip="false" index="16" name="Display 3" type="display">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer length" type="uint" value="900"/>
											<Property name="Inputs count" type="uint" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="679"/>
											<Property name="Top" type="int" value="400"/>
											<Property name="Width" type="int" value="170"/>
											<Property name="Height" type="int" value="45"/>
											<Property name="Chart min" type="real" value="0"/>
											<Property name="Chart max" type="real" value="1"/>
											<Property name="Autoscale" type="bool" value="false"/>
											<Property name="Scale each sep." type="bool" value="false"/>
											<Property name="Precision" type="int" value="3"/>
											<Property name="Digits" type="int" value="3"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="Fixed"/>
												<Enumeration index="1" name="General"/>
												<Enumeration index="2" name="Scientific"/>
											</Property>
											<Property name="Bkg color" type="color" value="0"/>
											<Property name="Axes color" type="color" value="8421504"/>
											<Property name="Bad sample color" type="color" value="255"/>
											<Property name="Show sample nr" type="bool" value="false"/>
											<Property name="Color scheme" type="enum" value="0">
												<Enumeration index="0" name="dark"/>
												<Enumeration index="1" name="light"/>
											</Property>
										</Properties>
									</Display>
								</Object>
								<Object ID="17" flip="false" index="17" name="Display 4" type="display">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer length" type="uint" value="900"/>
											<Property name="Inputs count" type="uint" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="676"/>
											<Property name="Top" type="int" value="124"/>
											<Property name="Width" type="int" value="170"/>
											<Property name="Height" type="int" value="45"/>
											<Property name="Chart min" type="real" value="0"/>
											<Property name="Chart max" type="real" value="1"/>
											<Property name="Autoscale" type="bool" value="false"/>
											<Property name="Scale each sep." type="bool" value="false"/>
											<Property name="Precision" type="int" value="3"/>
											<Property name="Digits" type="int" value="3"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="Fixed"/>
												<Enumeration index="1" name="General"/>
												<Enumeration index="2" name="Scientific"/>
											</Property>
											<Property name="Bkg color" type="color" value="0"/>
											<Property name="Axes color" type="color" value="8421504"/>
											<Property name="Bad sample color" type="color" value="255"/>
											<Property name="Show sample nr" type="bool" value="false"/>
											<Property name="Color scheme" type="enum" value="0">
												<Enumeration index="0" name="dark"/>
												<Enumeration index="1" name="light"/>
											</Property>
										</Properties>
									</Display>
								</Object>
								<Object ID="18" flip="false" index="18" name="Display 5" type="display">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer length" type="uint" value="900"/>
											<Property name="Inputs count" type="uint" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="552"/>
											<Property name="Top" type="int" value="43"/>
											<Property name="Width" type="int" value="170"/>
											<Property name="Height" type="int" value="45"/>
											<Property name="Chart min" type="real" value="0"/>
											<Property name="Chart max" type="real" value="1"/>
											<Property name="Autoscale" type="bool" value="false"/>
											<Property name="Scale each sep." type="bool" value="false"/>
											<Property name="Precision" type="int" value="3"/>
											<Property name="Digits" type="int" value="3"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="Fixed"/>
												<Enumeration index="1" name="General"/>
												<Enumeration index="2" name="Scientific"/>
											</Property>
											<Property name="Bkg color" type="color" value="0"/>
											<Property name="Axes color" type="color" value="8421504"/>
											<Property name="Bad sample color" type="color" value="255"/>
											<Property name="Show sample nr" type="bool" value="false"/>
											<Property name="Color scheme" type="enum" value="0">
												<Enumeration index="0" name="dark"/>
												<Enumeration index="1" name="light"/>
											</Property>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="30"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="20"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="26"/>
											<Segment index="1" value="-43"/>
											<Segment index="2" value="19"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="160"/>
											<Segment index="1" value="-25"/>
											<Segment index="2" value="167"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="186"/>
											<Segment index="1" value="47"/>
											<Segment index="2" value="141"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="153"/>
											<Segment index="1" value="163"/>
											<Segment index="2" value="174"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="6" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="145"/>
											<Segment index="1" value="238"/>
											<Segment index="2" value="181"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="3" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="105"/>
											<Segment index="1" value="-115"/>
											<Segment index="2" value="113"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="128"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="151"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="43"/>
											<Segment index="1" value="48"/>
											<Segment index="2" value="29"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="92"/>
											<Segment index="1" value="-24"/>
											<Segment index="2" value="99"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="143"/>
											<Segment index="1" value="-36"/>
											<Segment index="2" value="131"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="4" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="151"/>
											<Segment index="1" value="-92"/>
											<Segment index="2" value="167"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="103"/>
											<Segment index="1" value="168"/>
											<Segment index="2" value="20"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="13" begin_idx="0" end_id="6" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="19"/>
											<Segment index="1" value="-20"/>
											<Segment index="2" value="20"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="142"/>
											<Segment index="1" value="-37"/>
											<Segment index="2" value="133"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="137"/>
											<Segment index="1" value="2"/>
											<Segment index="2" value="138"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="14" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="57"/>
											<Segment index="1" value="368"/>
											<Segment index="2" value="57"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="14" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="55"/>
											<Segment index="1" value="288"/>
											<Segment index="2" value="55"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="14" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="50"/>
											<Segment index="1" value="250"/>
											<Segment index="2" value="55"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="18" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="23"/>
											<Segment index="1" value="-49"/>
											<Segment index="2" value="78"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="17" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="149"/>
											<Segment index="1" value="-40"/>
											<Segment index="2" value="76"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="15" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="17"/>
											<Segment index="1" value="-72"/>
											<Segment index="2" value="236"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="16" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="166"/>
											<Segment index="1" value="45"/>
											<Segment index="2" value="63"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="10" block_id="12" index="-1" name="CV>1">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="dCV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="130"/>
											<Property name="Top" type="int" value="84"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="1" name="przedzialy" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="139"/>
											<Property name="Top" type="int" value="277"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="2" name="krzywa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="124"/>
											<Property name="Top" type="int" value="577"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="3" name="Demux 2" type="Demux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Outputs" type="uint" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="197"/>
											<Property name="Top" type="int" value="62"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="4" name="Demux 1" type="Demux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Outputs" type="uint" value="4"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="216"/>
											<Property name="Top" type="int" value="242"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="90"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="5" name="Demux 3" type="Demux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Outputs" type="uint" value="4"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="265"/>
											<Property name="Top" type="int" value="542"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="90"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="6" name="dodatnia" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="263"/>
											<Property name="Top" type="int" value="61"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="7" name="ujemna" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="260"/>
											<Property name="Top" type="int" value="110"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="8" name=">alfa+beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="303"/>
											<Property name="Top" type="int" value="197"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="9" name="<-beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="299"/>
											<Property name="Top" type="int" value="336"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="10" name="Ax+B1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="349"/>
											<Property name="Top" type="int" value="462"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="11" name=">beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="305"/>
											<Property name="Top" type="int" value="260"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="14" flip="false" index="12" name="<-(alfa+beta)" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="274"/>
											<Property name="Top" type="int" value="414"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="15" flip="false" index="13" name="Ax+B4" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="364"/>
											<Property name="Top" type="int" value="693"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="16" flip="false" index="14" name="Ax+B3" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="358"/>
											<Property name="Top" type="int" value="627"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="17" flip="false" index="15" name="Ax+B2" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="358"/>
											<Property name="Top" type="int" value="549"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="18" flip="false" index="16" name="Lfun1" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="461"/>
											<Property name="Top" type="int" value="182"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="19" flip="false" index="17" name="fun1" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="672"/>
											<Property name="Top" type="int" value="469"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="20" flip="false" index="18" name="Lfun2" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="463"/>
											<Property name="Top" type="int" value="245"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="21" flip="false" index="19" name="fun2" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="669"/>
											<Property name="Top" type="int" value="532"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="22" flip="false" index="20" name="Lfun3" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="462"/>
											<Property name="Top" type="int" value="398"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="23" flip="false" index="21" name="Lfun4" type="logical">
									<Description>Lfun4</Description>
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="460"/>
											<Property name="Top" type="int" value="321"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="24" flip="false" index="22" name="fun3" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="667"/>
											<Property name="Top" type="int" value="592"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="25" flip="false" index="23" name="fun4" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="663"/>
											<Property name="Top" type="int" value="658"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="26" flip="false" index="24" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="+++++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="897"/>
											<Property name="Top" type="int" value="450"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="300"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="27" flip="false" index="25" name="Constant 1" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="673"/>
											<Property name="Top" type="int" value="736"/>
											<Property name="Width" type="int" value="40"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="28" flip="false" index="26" name="Signal limiter 1" type="signal_limiter">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Low limit" type="real" value="-1"/>
											<Property name="High limit" type="real" value="1"/>
											<Property name="Rising rate limit" type="real" value="0"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1017"/>
											<Property name="Top" type="int" value="590"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="29" flip="false" index="27" name="Out" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1291"/>
											<Property name="Top" type="int" value="602"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="30" flip="false" index="28" name="blokada" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="4"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="139"/>
											<Property name="Top" type="int" value="880"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="31" flip="false" index="29" name="Logic operator 1" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="216"/>
											<Property name="Top" type="int" value="868"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="32" flip="false" index="30" name="Logic operator 2" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1153"/>
											<Property name="Top" type="int" value="590"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="5" begin_idx="0" end_id="7" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="60"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="56"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="6" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="26"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="26"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="36"/>
											<Segment index="1" value="-48"/>
											<Segment index="2" value="36"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="1" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="37"/>
											<Segment index="1" value="-3"/>
											<Segment index="2" value="37"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="2" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="37"/>
											<Segment index="1" value="55"/>
											<Segment index="2" value="31"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="3" end_id="14" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="24"/>
											<Segment index="1" value="115"/>
											<Segment index="2" value="19"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="30"/>
											<Segment index="1" value="-83"/>
											<Segment index="2" value="39"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="1" end_id="17" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="32"/>
											<Segment index="1" value="-14"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="2" end_id="16" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="33"/>
											<Segment index="1" value="46"/>
											<Segment index="2" value="45"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="3" end_id="15" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="19"/>
											<Segment index="1" value="94"/>
											<Segment index="2" value="65"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="25"/>
											<Segment index="1" value="-14"/>
											<Segment index="2" value="26"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="1" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="24"/>
											<Segment index="1" value="15"/>
											<Segment index="2" value="24"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="4" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="21"/>
											<Segment index="1" value="-2"/>
											<Segment index="2" value="21"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="18" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="116"/>
											<Segment index="1" value="129"/>
											<Segment index="2" value="27"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="18" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="86"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="17"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="19" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="35"/>
											<Segment index="1" value="278"/>
											<Segment index="2" value="121"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="19" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="116"/>
											<Segment index="1" value="18"/>
											<Segment index="2" value="152"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="20" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="71"/>
											<Segment index="1" value="143"/>
											<Segment index="2" value="77"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="13" begin_idx="0" end_id="20" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="49"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="54"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="21" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="80"/>
											<Segment index="1" value="278"/>
											<Segment index="2" value="71"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="21" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="133"/>
											<Segment index="1" value="-6"/>
											<Segment index="2" value="123"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="14" begin_idx="0" end_id="22" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="74"/>
											<Segment index="1" value="-1"/>
											<Segment index="2" value="59"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="22" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="62"/>
											<Segment index="1" value="296"/>
											<Segment index="2" value="85"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="11" begin_idx="0" end_id="23" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="56"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="50"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="23" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="83"/>
											<Segment index="1" value="268"/>
											<Segment index="2" value="59"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="16" begin_idx="0" end_id="24" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="126"/>
											<Segment index="1" value="-24"/>
											<Segment index="2" value="128"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="22" begin_idx="0" end_id="24" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="61"/>
											<Segment index="1" value="185"/>
											<Segment index="2" value="89"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="23" begin_idx="0" end_id="25" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="100"/>
											<Segment index="1" value="328"/>
											<Segment index="2" value="48"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="15" begin_idx="0" end_id="25" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="125"/>
											<Segment index="1" value="-24"/>
											<Segment index="2" value="119"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="19" begin_idx="0" end_id="26" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="78"/>
											<Segment index="1" value="11"/>
											<Segment index="2" value="92"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="21" begin_idx="0" end_id="26" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="79"/>
											<Segment index="1" value="-2"/>
											<Segment index="2" value="94"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="24" begin_idx="0" end_id="26" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="82"/>
											<Segment index="1" value="-12"/>
											<Segment index="2" value="93"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="25" begin_idx="0" end_id="26" end_idx="3" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="101"/>
											<Segment index="1" value="-28"/>
											<Segment index="2" value="78"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="27" begin_idx="0" end_id="26" end_idx="4" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="94"/>
											<Segment index="1" value="-51"/>
											<Segment index="2" value="95"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="26" begin_idx="0" end_id="28" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="32"/>
											<Segment index="1" value="5"/>
											<Segment index="2" value="33"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="30" begin_idx="0" end_id="31" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="17"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="35"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="32" begin_idx="0" end_id="29" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="40"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="43"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="28" begin_idx="0" end_id="32" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="40"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="41"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="31" begin_idx="0" end_id="32" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="833"/>
											<Segment index="1" value="-270"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="11" block_id="13" index="-1" name="Sprawdzenie">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="dCV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="131"/>
											<Property name="Top" type="int" value="216"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="przedzialy" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="137"/>
											<Property name="Top" type="int" value="337"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="2" name="Demux 1" type="Demux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Outputs" type="uint" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="258"/>
											<Property name="Top" type="int" value="196"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="3" name="Demux 2" type="Demux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Outputs" type="uint" value="4"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="228"/>
											<Property name="Top" type="int" value="303"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="90"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="4" name="dodatnia" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="332"/>
											<Property name="Top" type="int" value="192"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="5" name="ujemna" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="333"/>
											<Property name="Top" type="int" value="232"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="6" name=">alfa+beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="297"/>
											<Property name="Top" type="int" value="287"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="7" name=">beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="298"/>
											<Property name="Top" type="int" value="331"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="8" name="<-beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="305"/>
											<Property name="Top" type="int" value="394"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="9" name="<-(alfa+beta)" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="304"/>
											<Property name="Top" type="int" value="436"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="16" flip="false" index="10" name="Constant 1" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="1"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="139"/>
											<Property name="Top" type="int" value="124"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="17" flip="false" index="11" name="Constant 2" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="-1"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="148"/>
											<Property name="Top" type="int" value="571"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="18" flip="false" index="12" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="2"/>
											<Property name="Top" type="int" value="53"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="19" flip="false" index="13" name="dodatniaKrzywa<1" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="718"/>
											<Property name="Top" type="int" value="125"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="20" flip="false" index="14" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="3">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="409"/>
											<Property name="Top" type="int" value="61"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="21" flip="false" index="15" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="457"/>
											<Property name="Top" type="int" value="308"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="22" flip="false" index="16" name="Relational operator 3" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="388"/>
											<Property name="Top" type="int" value="529"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="23" flip="false" index="17" name="Relational operator 4" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="3">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="500"/>
											<Property name="Top" type="int" value="629"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="24" flip="false" index="18" name="ujemnaKrzywa>-1" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="701"/>
											<Property name="Top" type="int" value="531"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="25" flip="false" index="19" name="dodatniaKrzywa<1" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="845"/>
											<Property name="Top" type="int" value="128"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="26" flip="false" index="20" name="ujemnaKrzywa>-1" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="822"/>
											<Property name="Top" type="int" value="542"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="56"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="4" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="54"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="12"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="6" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="4"/>
											<Segment index="1" value="-17"/>
											<Segment index="2" value="55"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="1" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="7"/>
											<Segment index="1" value="3"/>
											<Segment index="2" value="53"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="28"/>
											<Segment index="1" value="-27"/>
											<Segment index="2" value="26"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="1" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="30"/>
											<Segment index="1" value="-1"/>
											<Segment index="2" value="25"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="2" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="31"/>
											<Segment index="1" value="44"/>
											<Segment index="2" value="31"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="3" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="15"/>
											<Segment index="1" value="68"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="20" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="245"/>
											<Segment index="1" value="13"/>
											<Segment index="2" value="137"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="16" begin_idx="0" end_id="20" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="37"/>
											<Segment index="1" value="-48"/>
											<Segment index="2" value="138"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="21" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="371"/>
											<Segment index="1" value="260"/>
											<Segment index="2" value="59"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="21" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="59"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="45"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="19" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="90"/>
											<Segment index="1" value="-92"/>
											<Segment index="2" value="240"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="21" begin_idx="0" end_id="19" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="36"/>
											<Segment index="1" value="-172"/>
											<Segment index="2" value="170"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="19" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="52"/>
											<Segment index="1" value="53"/>
											<Segment index="2" value="202"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="22" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="71"/>
											<Segment index="1" value="-27"/>
											<Segment index="2" value="74"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="22" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="53"/>
											<Segment index="1" value="481"/>
											<Segment index="2" value="308"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="23" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="6"/>
											<Segment index="1" value="596"/>
											<Segment index="2" value="467"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="23" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="126"/>
											<Segment index="1" value="243"/>
											<Segment index="2" value="14"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="24" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="186"/>
											<Segment index="1" value="343"/>
											<Segment index="2" value="128"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="22" begin_idx="0" end_id="24" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="137"/>
											<Segment index="1" value="2"/>
											<Segment index="2" value="121"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="23" begin_idx="0" end_id="24" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="81"/>
											<Segment index="1" value="-87"/>
											<Segment index="2" value="65"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="19" begin_idx="0" end_id="25" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="36"/>
											<Segment index="1" value="-9"/>
											<Segment index="2" value="36"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="24" begin_idx="0" end_id="26" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="30"/>
											<Segment index="1" value="-1"/>
											<Segment index="2" value="36"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
					</Paths>
					<Objects>
						<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="6"/>
									<Property name="Top" type="int" value="76"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="9"/>
									<Property name="Top" type="int" value="181"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="3"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="9"/>
									<Property name="Top" type="int" value="259"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="41" flip="false" index="3" name="u" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="1287"/>
									<Property name="Top" type="int" value="72"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="46" flip="false" index="4" name="przypadekXmniejszyOd1dlaPlus" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="318"/>
									<Property name="Top" type="int" value="872"/>
									<Property name="Width" type="int" value="240"/>
									<Property name="Height" type="int" value="75"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="52" flip="false" index="5" name="przypadekXmniejszyOd1dlaMinus" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="365"/>
									<Property name="Top" type="int" value="986"/>
									<Property name="Width" type="int" value="240"/>
									<Property name="Height" type="int" value="75"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="56" flip="false" index="6" name="Krzywa" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="144"/>
									<Property name="Top" type="int" value="272"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="90"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="57" flip="false" index="7" name="pochodna" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="147"/>
									<Property name="Top" type="int" value="56"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="60"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="58" flip="false" index="8" name="przedzialy" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="146"/>
									<Property name="Top" type="int" value="145"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="90"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="12" flip="false" index="9" name="CV>1" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="891"/>
									<Property name="Top" type="int" value="59"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="90"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="14" flip="false" index="10" name="Mux 1" type="Mux">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs" type="uint" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="400"/>
									<Property name="Top" type="int" value="56"/>
									<Property name="Width" type="int" value="20"/>
									<Property name="Height" type="int" value="60"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="15" flip="false" index="11" name="Mux 2" type="Mux">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs" type="uint" value="4"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="403"/>
									<Property name="Top" type="int" value="148"/>
									<Property name="Width" type="int" value="20"/>
									<Property name="Height" type="int" value="90"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="16" flip="false" index="12" name="Mux 3" type="Mux">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs" type="uint" value="4"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="413"/>
									<Property name="Top" type="int" value="271"/>
									<Property name="Width" type="int" value="20"/>
									<Property name="Height" type="int" value="90"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="13" flip="false" index="13" name="Sprawdzenie" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="573"/>
									<Property name="Top" type="int" value="215"/>
									<Property name="Width" type="int" value="250"/>
									<Property name="Height" type="int" value="75"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="1" begin_idx="0" end_id="58" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="77"/>
									<Segment index="1" value="-2"/>
									<Segment index="2" value="35"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="56" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="78"/>
									<Segment index="1" value="125"/>
									<Segment index="2" value="32"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="58" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="52"/>
									<Segment index="1" value="-58"/>
									<Segment index="2" value="60"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="56" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="53"/>
									<Segment index="1" value="69"/>
									<Segment index="2" value="57"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="56" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="70"/>
									<Segment index="1" value="208"/>
									<Segment index="2" value="43"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="58" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="61"/>
									<Segment index="1" value="81"/>
									<Segment index="2" value="54"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="57" begin_idx="0" end_id="14" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="29"/>
									<Segment index="1" value="0"/>
									<Segment index="2" value="29"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="57" begin_idx="1" end_id="14" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="29"/>
									<Segment index="1" value="0"/>
									<Segment index="2" value="29"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="14" begin_idx="0" end_id="12" end_idx="0" type="20">
							<Display type="20">
								<Segments size="3">
									<Segment index="0" value="25"/>
									<Segment index="1" value="-9"/>
									<Segment index="2" value="451"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="57" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="58"/>
									<Segment index="1" value="0"/>
									<Segment index="2" value="58"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="58" begin_idx="0" end_id="15" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="31"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="31"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="58" begin_idx="1" end_id="15" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="31"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="31"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="58" begin_idx="2" end_id="15" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="31"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="31"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="58" begin_idx="3" end_id="15" end_idx="3" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="31"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="31"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="15" begin_idx="0" end_id="12" end_idx="1" type="20">
							<Display type="20">
								<Segments size="3">
									<Segment index="0" value="50"/>
									<Segment index="1" value="-98"/>
									<Segment index="2" value="423"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="0" end_id="16" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="19"/>
									<Segment index="1" value="-1"/>
									<Segment index="2" value="55"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="1" end_id="16" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="19"/>
									<Segment index="1" value="-1"/>
									<Segment index="2" value="55"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="2" end_id="16" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="19"/>
									<Segment index="1" value="-1"/>
									<Segment index="2" value="55"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="3" end_id="16" end_idx="3" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="19"/>
									<Segment index="1" value="-1"/>
									<Segment index="2" value="55"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="16" begin_idx="0" end_id="12" end_idx="2" type="20">
							<Display type="20">
								<Segments size="3">
									<Segment index="0" value="25"/>
									<Segment index="1" value="-203"/>
									<Segment index="2" value="438"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="14" begin_idx="0" end_id="13" end_idx="0" type="20">
							<Display type="20">
								<Segments size="3">
									<Segment index="0" value="25"/>
									<Segment index="1" value="147"/>
									<Segment index="2" value="133"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="15" begin_idx="0" end_id="13" end_idx="1" type="20">
							<Display type="20">
								<Segments size="3">
									<Segment index="0" value="102"/>
									<Segment index="1" value="58"/>
									<Segment index="2" value="53"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="13" end_idx="2" type="1">
							<Display type="1">
								<Segments size="5">
									<Segment index="0" value="32"/>
									<Segment index="1" value="38"/>
									<Segment index="2" value="428"/>
									<Segment index="3" value="145"/>
									<Segment index="4" value="82"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="12" begin_idx="0" end_id="41" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="59"/>
									<Segment index="1" value="-22"/>
									<Segment index="2" value="142"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="13" begin_idx="0" end_id="12" end_idx="3" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="36"/>
									<Segment index="1" value="-109"/>
									<Segment index="2" value="37"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="13" begin_idx="1" end_id="12" end_idx="3" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="36"/>
									<Segment index="1" value="-134"/>
									<Segment index="2" value="37"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
			</Paths>
			<Objects>
				<Object ID="1" flip="false" index="0" name="Constant 1" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0.1"/>
							<Property name="Min" type="real" value="0"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="162"/>
							<Property name="Top" type="int" value="221"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="2" flip="false" index="1" name="Constant 2" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0.05"/>
							<Property name="Min" type="real" value="-0.5"/>
							<Property name="Max" type="real" value="0.5"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="157"/>
							<Property name="Top" type="int" value="286"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="6" flip="false" index="2" name="Display 1" type="display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="120"/>
							<Property name="Inputs count" type="uint" value="2"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="730"/>
							<Property name="Top" type="int" value="79"/>
							<Property name="Width" type="int" value="170"/>
							<Property name="Height" type="int" value="60"/>
							<Property name="Chart min" type="real" value="-0.5"/>
							<Property name="Chart max" type="real" value="1"/>
							<Property name="Autoscale" type="bool" value="false"/>
							<Property name="Scale each sep." type="bool" value="false"/>
							<Property name="Precision" type="int" value="3"/>
							<Property name="Digits" type="int" value="3"/>
							<Property name="Type" type="enum" value="1">
								<Enumeration index="0" name="Fixed"/>
								<Enumeration index="1" name="General"/>
								<Enumeration index="2" name="Scientific"/>
							</Property>
							<Property name="Bkg color" type="color" value="0"/>
							<Property name="Axes color" type="color" value="8421504"/>
							<Property name="Bad sample color" type="color" value="255"/>
							<Property name="Show sample nr" type="bool" value="false"/>
							<Property name="Color scheme" type="enum" value="0">
								<Enumeration index="0" name="dark"/>
								<Enumeration index="1" name="light"/>
							</Property>
						</Properties>
					</Display>
				</Object>
				<Object ID="8" flip="false" index="3" name="XY Display 1" type="xy_display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="2000"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="781"/>
							<Property name="Top" type="int" value="249"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="66"/>
							<Property name="X axis min" type="real" value="-0.52"/>
							<Property name="X axis max" type="real" value="0.52"/>
							<Property name="Y axis min" type="real" value="-0.02"/>
							<Property name="Y axis max" type="real" value="1.02"/>
							<Property name="Chart bkg color" type="color" value="536870912"/>
							<Property name="Chart axes color" type="color" value="12632256"/>
							<Property name="Chart plot color" type="color" value="-16777211"/>
							<Property name="Chart bad sample color" type="color" value="255"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="7" flip="false" index="4" name="Subpath 1" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="404"/>
							<Property name="Top" type="int" value="170"/>
							<Property name="Width" type="int" value="200"/>
							<Property name="Height" type="int" value="75"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="9" flip="false" index="5" name="Gain 1" type="gain">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Gain" type="real" value="1"/>
							<Property name="Clear sig. status" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="221"/>
							<Property name="Top" type="int" value="49"/>
							<Property name="Width" type="int" value="60"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="10" flip="false" index="6" name="Display 2" type="display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="100"/>
							<Property name="Inputs count" type="uint" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="380"/>
							<Property name="Top" type="int" value="347"/>
							<Property name="Width" type="int" value="170"/>
							<Property name="Height" type="int" value="45"/>
							<Property name="Chart min" type="real" value="-0.5"/>
							<Property name="Chart max" type="real" value="0.5"/>
							<Property name="Autoscale" type="bool" value="false"/>
							<Property name="Scale each sep." type="bool" value="false"/>
							<Property name="Precision" type="int" value="3"/>
							<Property name="Digits" type="int" value="3"/>
							<Property name="Type" type="enum" value="1">
								<Enumeration index="0" name="Fixed"/>
								<Enumeration index="1" name="General"/>
								<Enumeration index="2" name="Scientific"/>
							</Property>
							<Property name="Bkg color" type="color" value="0"/>
							<Property name="Axes color" type="color" value="8421504"/>
							<Property name="Bad sample color" type="color" value="255"/>
							<Property name="Show sample nr" type="bool" value="false"/>
							<Property name="Color scheme" type="enum" value="0">
								<Enumeration index="0" name="dark"/>
								<Enumeration index="1" name="light"/>
							</Property>
						</Properties>
					</Display>
				</Object>
				<Object ID="14" flip="false" index="7" name="Periodical signal 1" type="src_periodic">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Frequency" type="real" value="0.2"/>
							<Property name="Phase" type="real" value="0"/>
							<Property name="Amplitude" type="real" value="0.5"/>
							<Property name="Bias" type="real" value="0"/>
							<Property name="Function" type="enum" value="1">
								<Enumeration index="0" name="sawtooth"/>
								<Enumeration index="1" name="sinus"/>
								<Enumeration index="2" name="square"/>
							</Property>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="55"/>
							<Property name="Top" type="int" value="50"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
			</Objects>
			<Connections>
				<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="76"/>
							<Segment index="1" value="-30"/>
							<Segment index="2" value="71"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="2" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="100"/>
							<Segment index="1" value="-77"/>
							<Segment index="2" value="52"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="7" begin_idx="0" end_id="6" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="64"/>
							<Segment index="1" value="-108"/>
							<Segment index="2" value="67"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="7" begin_idx="0" end_id="8" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="106"/>
							<Segment index="1" value="86"/>
							<Segment index="2" value="76"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="7" end_idx="0" type="1">
					<Display type="1">
						<Segments size="5">
							<Segment index="0" value="203"/>
							<Segment index="1" value="47"/>
							<Segment index="2" value="-87"/>
							<Segment index="3" value="77"/>
							<Segment index="4" value="12"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="6" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="305"/>
							<Segment index="1" value="55"/>
							<Segment index="2" value="149"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="8" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="332"/>
							<Segment index="1" value="207"/>
							<Segment index="2" value="173"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="10" end_idx="0" type="1">
					<Display type="1">
						<Segments size="5">
							<Segment index="0" value="101"/>
							<Segment index="1" value="157"/>
							<Segment index="2" value="-1"/>
							<Segment index="3" value="148"/>
							<Segment index="4" value="4"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="14" begin_idx="0" end_id="9" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="28"/>
							<Segment index="1" value="-1"/>
							<Segment index="2" value="43"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
			</Connections>
		</PATH>
	</Paths>
</CalcPaths_Export_File>
