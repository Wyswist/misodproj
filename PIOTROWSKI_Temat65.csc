<CalcPaths_Export_File>
	<Simulator after_time="500" max_steps="0" sample_time="1" simulation_rate="10" simulation_ratio="0.01" start_time="1984-01-01T00:00:00.000" stop_condition="0" stop_time="1984-01-01T00:00:00.000"/>
	<Paths>
		<PATH ID="4" block_id="-1" index="-1" name="Path 1">
			<Paths>
				<PATH ID="17" block_id="7" index="-1" name="ElNL">
					<Paths>
						<PATH ID="15" block_id="56" index="-1" name="Krzywa">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="156"/>
											<Property name="Top" type="int" value="642"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="146"/>
											<Property name="Top" type="int" value="824"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="145"/>
											<Property name="Top" type="int" value="1018"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="A" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="403"/>
											<Property name="Top" type="int" value="711"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="B1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1-1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="396"/>
											<Property name="Top" type="int" value="817"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="B2" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="-i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="399"/>
											<Property name="Top" type="int" value="969"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="B3" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="(i1[0]+i2[0])/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="567"/>
											<Property name="Top" type="int" value="1146"/>
											<Property name="Width" type="int" value="150"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="B4" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="417"/>
											<Property name="Top" type="int" value="1214"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="Product 1" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="678"/>
											<Property name="Top" type="int" value="669"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="834"/>
											<Property name="Top" type="int" value="734"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="Sum 2" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="837"/>
											<Property name="Top" type="int" value="853"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="Sum 3" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="836"/>
											<Property name="Top" type="int" value="970"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="Sum 4" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="840"/>
											<Property name="Top" type="int" value="1108"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="13" name="Ax+B1" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="945"/>
											<Property name="Top" type="int" value="743"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="14" flip="false" index="14" name="Ax+B2" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="956"/>
											<Property name="Top" type="int" value="864"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="15" flip="false" index="15" name="Ax+B3" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="960"/>
											<Property name="Top" type="int" value="984"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="16" flip="false" index="16" name="Ax+B4" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="4"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="964"/>
											<Property name="Top" type="int" value="1116"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="17" flip="false" index="17" name="Math expression 1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="-1+1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="391"/>
											<Property name="Top" type="int" value="1076"/>
											<Property name="Width" type="int" value="150"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="3" begin_idx="0" end_id="8" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="80"/>
											<Segment index="1" value="-49"/>
											<Segment index="2" value="80"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="50"/>
											<Segment index="1" value="58"/>
											<Segment index="2" value="51"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="52"/>
											<Segment index="1" value="177"/>
											<Segment index="2" value="52"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="53"/>
											<Segment index="1" value="294"/>
											<Segment index="2" value="50"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="54"/>
											<Segment index="1" value="432"/>
											<Segment index="2" value="53"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="9" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="164"/>
											<Segment index="1" value="-90"/>
											<Segment index="2" value="159"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="10" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="161"/>
											<Segment index="1" value="-123"/>
											<Segment index="2" value="162"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="12" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="218"/>
											<Segment index="1" value="-113"/>
											<Segment index="2" value="90"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="90"/>
											<Segment index="1" value="-101"/>
											<Segment index="2" value="142"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="248"/>
											<Segment index="1" value="30"/>
											<Segment index="2" value="249"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="116"/>
											<Segment index="1" value="5"/>
											<Segment index="2" value="109"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="128"/>
											<Segment index="1" value="157"/>
											<Segment index="2" value="100"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="123"/>
											<Segment index="1" value="402"/>
											<Segment index="2" value="123"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="4" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="109"/>
											<Segment index="1" value="-167"/>
											<Segment index="2" value="117"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="114"/>
											<Segment index="1" value="-15"/>
											<Segment index="2" value="115"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="118"/>
											<Segment index="1" value="230"/>
											<Segment index="2" value="129"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="28"/>
											<Segment index="1" value="-1"/>
											<Segment index="2" value="28"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="14" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="32"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="32"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="11" begin_idx="0" end_id="15" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="34"/>
											<Segment index="1" value="4"/>
											<Segment index="2" value="35"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="16" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="35"/>
											<Segment index="1" value="-2"/>
											<Segment index="2" value="34"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="3" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="116"/>
											<Segment index="1" value="-273"/>
											<Segment index="2" value="117"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="11" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="165"/>
											<Segment index="1" value="-113"/>
											<Segment index="2" value="135"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="17" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="125"/>
											<Segment index="1" value="264"/>
											<Segment index="2" value="95"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="17" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="125"/>
											<Segment index="1" value="92"/>
											<Segment index="2" value="96"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="19" block_id="43" index="-1" name="Pochodna">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="155"/>
											<Property name="Top" type="int" value="153"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="dodatnia" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="679"/>
											<Property name="Top" type="int" value="119"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="ujemna" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="681"/>
											<Property name="Top" type="int" value="202"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="Math expression 1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i1[0]-i1[1]"/>
											<Property name="Inputs" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="348"/>
											<Property name="Top" type="int" value="148"/>
											<Property name="Width" type="int" value="80"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="507"/>
											<Property name="Top" type="int" value="101"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="509"/>
											<Property name="Top" type="int" value="190"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="Constant 1" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="322"/>
											<Property name="Top" type="int" value="262"/>
											<Property name="Width" type="int" value="40"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="149"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="19"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="4" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="103"/>
											<Segment index="1" value="-146"/>
											<Segment index="2" value="47"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="104"/>
											<Segment index="1" value="-57"/>
											<Segment index="2" value="48"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="21"/>
											<Segment index="1" value="-47"/>
											<Segment index="2" value="63"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="48"/>
											<Segment index="1" value="42"/>
											<Segment index="2" value="38"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="1" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="58"/>
											<Segment index="1" value="6"/>
											<Segment index="2" value="59"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="2" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="58"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="59"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="20" block_id="44" index="-1" name="Przedzialy">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="242"/>
											<Property name="Top" type="int" value="203"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="75"/>
											<Property name="Top" type="int" value="574"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="99"/>
											<Property name="Top" type="int" value="340"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="538"/>
											<Property name="Top" type="int" value="211"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="538"/>
											<Property name="Top" type="int" value="310"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Relational operator 3" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="520"/>
											<Property name="Top" type="int" value="456"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="Relational operator 4" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="516"/>
											<Property name="Top" type="int" value="578"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="alfaplusbeta" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="160"/>
											<Property name="Top" type="int" value="500"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="-(alfaplusbeta)" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="-1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="248"/>
											<Property name="Top" type="int" value="594"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="Gain 1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="-1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="206"/>
											<Property name="Top" type="int" value="406"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="CVbeta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="791"/>
											<Property name="Top" type="int" value="243"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="CValfaplusbeta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="829"/>
											<Property name="Top" type="int" value="326"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="CVbeta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="831"/>
											<Property name="Top" type="int" value="457"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="13" name="CV" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="4"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="830"/>
											<Property name="Top" type="int" value="591"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="7" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="19"/>
											<Segment index="1" value="89"/>
											<Segment index="2" value="14"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="124"/>
											<Segment index="1" value="13"/>
											<Segment index="2" value="147"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="123"/>
											<Segment index="1" value="112"/>
											<Segment index="2" value="148"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="148"/>
											<Segment index="1" value="258"/>
											<Segment index="2" value="105"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="6" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="134"/>
											<Segment index="1" value="380"/>
											<Segment index="2" value="115"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="38"/>
											<Segment index="1" value="-58"/>
											<Segment index="2" value="22"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="18"/>
											<Segment index="1" value="163"/>
											<Segment index="2" value="18"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="6" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="105"/>
											<Segment index="1" value="-1"/>
											<Segment index="2" value="108"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="3" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="208"/>
											<Segment index="1" value="-109"/>
											<Segment index="2" value="206"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="4" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="157"/>
											<Segment index="1" value="-180"/>
											<Segment index="2" value="166"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="41"/>
											<Segment index="1" value="71"/>
											<Segment index="2" value="41"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="129"/>
											<Segment index="1" value="65"/>
											<Segment index="2" value="130"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="99"/>
											<Segment index="1" value="20"/>
											<Segment index="2" value="99"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="113"/>
											<Segment index="1" value="4"/>
											<Segment index="2" value="123"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="129"/>
											<Segment index="1" value="-11"/>
											<Segment index="2" value="127"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="129"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="130"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="23" block_id="11" index="-1" name="Logika">
							<Paths>
								<PATH ID="11" block_id="62" index="-1" name="CV_mniejsze_od_1">
									<Paths/>
									<Objects>
										<Object ID="0" flip="false" index="0" name="In" type="subpath_input">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="1"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="-18"/>
													<Property name="Top" type="int" value="1094"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="1" flip="false" index="1" name="Demux 1" type="Demux">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Outputs" type="uint" value="7"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="68"/>
													<Property name="Top" type="int" value="877"/>
													<Property name="Width" type="int" value="20"/>
													<Property name="Height" type="int" value="450"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="2" flip="false" index="2" name="CV" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="239"/>
													<Property name="Top" type="int" value="891"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="3" flip="false" index="3" name="dodatnia" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="202"/>
													<Property name="Top" type="int" value="1061"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="4" flip="false" index="4" name="ujemna" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="199"/>
													<Property name="Top" type="int" value="1121"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="5" flip="false" index="5" name="CV_w_alfa_beta" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="192"/>
													<Property name="Top" type="int" value="1176"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="6" flip="false" index="6" name="CV_m_alfa_beta" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="186"/>
													<Property name="Top" type="int" value="1231"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="7" flip="false" index="7" name="Ax+B1" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="182"/>
													<Property name="Top" type="int" value="1279"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="8" flip="false" index="8" name="Ax+B3" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="175"/>
													<Property name="Top" type="int" value="1337"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="10" flip="false" index="9" name="Constant 1" type="constant">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Value" type="real" value="1"/>
													<Property name="Min" type="real" value="-1"/>
													<Property name="Max" type="real" value="1"/>
													<Property name="Limit output" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="373"/>
													<Property name="Top" type="int" value="1027"/>
													<Property name="Width" type="int" value="50"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="11" flip="false" index="10" name="Constant 2" type="constant">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Value" type="real" value="-1"/>
													<Property name="Min" type="real" value="-1"/>
													<Property name="Max" type="real" value="1"/>
													<Property name="Limit output" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="352"/>
													<Property name="Top" type="int" value="1224"/>
													<Property name="Width" type="int" value="50"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="12" flip="false" index="11" name="Relational operator 2" type="relational">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Type" type="enum" value="1">
														<Enumeration index="0" name="EQUAL"/>
														<Enumeration index="1" name="GREATER"/>
														<Enumeration index="2" name="GREATER or EQUAL"/>
														<Enumeration index="3" name="LESS"/>
														<Enumeration index="4" name="LESS or EQUAL"/>
														<Enumeration index="5" name="NOT EQUAL"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="453"/>
													<Property name="Top" type="int" value="1190"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="23" flip="false" index="12" name="Logic operator 3" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="3"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="726"/>
													<Property name="Top" type="int" value="965"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="26" flip="false" index="13" name="Logic operator 4" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="3"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="731"/>
													<Property name="Top" type="int" value="1038"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="27" flip="false" index="14" name="Product 2" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="847"/>
													<Property name="Top" type="int" value="968"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="28" flip="false" index="15" name="Product 3" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="854"/>
													<Property name="Top" type="int" value="1069"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="29" flip="false" index="16" name="Sum 2" type="sum">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Signs" type="string" value="++"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="946"/>
													<Property name="Top" type="int" value="1003"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="60"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="30" flip="false" index="17" name="XY Display 1" type="xy_display">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Buffer length" type="uint" value="1000"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12639424"/>
													<Property name="Left" type="int" value="1225"/>
													<Property name="Top" type="int" value="975"/>
													<Property name="Width" type="int" value="100"/>
													<Property name="Height" type="int" value="66"/>
													<Property name="X axis min" type="real" value="-1.13601428232733"/>
													<Property name="X axis max" type="real" value="1.14384341730054"/>
													<Property name="Y axis min" type="real" value="-1.03737454525005"/>
													<Property name="Y axis max" type="real" value="1.03737454525005"/>
													<Property name="Chart bkg color" type="color" value="16777215"/>
													<Property name="Chart axes color" type="color" value="12632256"/>
													<Property name="Chart plot color" type="color" value="0"/>
													<Property name="Chart bad sample color" type="color" value="255"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="31" flip="false" index="18" name="Relational operator 1" type="relational">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Type" type="enum" value="3">
														<Enumeration index="0" name="EQUAL"/>
														<Enumeration index="1" name="GREATER"/>
														<Enumeration index="2" name="GREATER or EQUAL"/>
														<Enumeration index="3" name="LESS"/>
														<Enumeration index="4" name="LESS or EQUAL"/>
														<Enumeration index="5" name="NOT EQUAL"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="526"/>
													<Property name="Top" type="int" value="985"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="32" flip="false" index="19" name="Logic operator 5" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="3"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="734"/>
													<Property name="Top" type="int" value="1155"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="33" flip="false" index="20" name="Logic operator 6" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="3"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="736"/>
													<Property name="Top" type="int" value="1224"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="34" flip="false" index="21" name="Product 4" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="860"/>
													<Property name="Top" type="int" value="1138"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="35" flip="false" index="22" name="Product 5" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="862"/>
													<Property name="Top" type="int" value="1217"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="36" flip="false" index="23" name="Sum 3" type="sum">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Signs" type="string" value="++"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="960"/>
													<Property name="Top" type="int" value="1153"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="60"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="39" flip="false" index="24" name="Sum 4" type="sum">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Signs" type="string" value="++"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="1138"/>
													<Property name="Top" type="int" value="1087"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="60"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="37" flip="false" index="25" name="Out" type="subpath_output">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="1"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12639424"/>
													<Property name="Left" type="int" value="1330"/>
													<Property name="Top" type="int" value="1104"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
									</Objects>
									<Connections>
										<Connection begin_id="1" begin_idx="0" end_id="2" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="89"/>
													<Segment index="1" value="-35"/>
													<Segment index="2" value="67"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="1" begin_idx="1" end_id="3" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="101"/>
													<Segment index="1" value="79"/>
													<Segment index="2" value="18"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="1" begin_idx="2" end_id="4" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="88"/>
													<Segment index="1" value="83"/>
													<Segment index="2" value="28"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="1" begin_idx="3" end_id="5" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="63"/>
													<Segment index="1" value="82"/>
													<Segment index="2" value="46"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="1" begin_idx="4" end_id="6" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="56"/>
													<Segment index="1" value="81"/>
													<Segment index="2" value="47"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="1" begin_idx="5" end_id="7" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="48"/>
													<Segment index="1" value="73"/>
													<Segment index="2" value="51"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="1" begin_idx="6" end_id="8" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="37"/>
													<Segment index="1" value="75"/>
													<Segment index="2" value="55"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="0" end_id="12" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="49"/>
													<Segment index="1" value="307"/>
													<Segment index="2" value="110"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="0" begin_idx="0" end_id="1" end_idx="0" type="20">
											<Display type="20">
												<Segments size="3">
													<Segment index="0" value="18"/>
													<Segment index="1" value="-2"/>
													<Segment index="2" value="43"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="3" begin_idx="0" end_id="23" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="409"/>
													<Segment index="1" value="-81"/>
													<Segment index="2" value="60"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="5" begin_idx="0" end_id="23" end_idx="2" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="432"/>
													<Segment index="1" value="-185"/>
													<Segment index="2" value="47"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="4" begin_idx="0" end_id="26" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="437"/>
													<Segment index="1" value="-68"/>
													<Segment index="2" value="40"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="5" begin_idx="0" end_id="26" end_idx="2" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="455"/>
													<Segment index="1" value="-112"/>
													<Segment index="2" value="29"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="26" begin_idx="0" end_id="28" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="34"/>
													<Segment index="1" value="19"/>
													<Segment index="2" value="34"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="23" begin_idx="0" end_id="27" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="28"/>
													<Segment index="1" value="-9"/>
													<Segment index="2" value="38"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="7" begin_idx="0" end_id="27" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="580"/>
													<Segment index="1" value="-298"/>
													<Segment index="2" value="30"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="7" begin_idx="0" end_id="28" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="599"/>
													<Segment index="1" value="-197"/>
													<Segment index="2" value="18"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="27" begin_idx="0" end_id="29" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="22"/>
													<Segment index="1" value="40"/>
													<Segment index="2" value="22"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="28" begin_idx="0" end_id="29" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="18"/>
													<Segment index="1" value="-41"/>
													<Segment index="2" value="19"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="0" end_id="30" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="732"/>
													<Segment index="1" value="99"/>
													<Segment index="2" value="199"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="10" begin_idx="0" end_id="31" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="64"/>
													<Segment index="1" value="-27"/>
													<Segment index="2" value="44"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="0" end_id="31" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="88"/>
													<Segment index="1" value="102"/>
													<Segment index="2" value="144"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="31" begin_idx="0" end_id="23" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="72"/>
													<Segment index="1" value="-31"/>
													<Segment index="2" value="73"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="31" begin_idx="0" end_id="26" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="69"/>
													<Segment index="1" value="42"/>
													<Segment index="2" value="81"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="11" begin_idx="0" end_id="12" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="46"/>
													<Segment index="1" value="-19"/>
													<Segment index="2" value="10"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="12" begin_idx="0" end_id="32" end_idx="2" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="132"/>
													<Segment index="1" value="-24"/>
													<Segment index="2" value="94"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="12" begin_idx="0" end_id="33" end_idx="2" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="131"/>
													<Segment index="1" value="45"/>
													<Segment index="2" value="97"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="6" begin_idx="0" end_id="32" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="71"/>
													<Segment index="1" value="-61"/>
													<Segment index="2" value="422"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="6" begin_idx="0" end_id="33" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="72"/>
													<Segment index="1" value="8"/>
													<Segment index="2" value="423"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="4" begin_idx="0" end_id="32" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="234"/>
													<Segment index="1" value="38"/>
													<Segment index="2" value="246"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="3" begin_idx="0" end_id="33" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="346"/>
													<Segment index="1" value="167"/>
													<Segment index="2" value="133"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="33" begin_idx="0" end_id="35" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="37"/>
													<Segment index="1" value="-9"/>
													<Segment index="2" value="34"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="32" begin_idx="0" end_id="34" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="38"/>
													<Segment index="1" value="-19"/>
													<Segment index="2" value="33"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="8" begin_idx="0" end_id="35" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="626"/>
													<Segment index="1" value="-117"/>
													<Segment index="2" value="6"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="8" begin_idx="0" end_id="34" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="615"/>
													<Segment index="1" value="-196"/>
													<Segment index="2" value="15"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="34" begin_idx="0" end_id="36" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="22"/>
													<Segment index="1" value="20"/>
													<Segment index="2" value="23"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="35" begin_idx="0" end_id="36" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="21"/>
													<Segment index="1" value="-39"/>
													<Segment index="2" value="22"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="29" begin_idx="0" end_id="39" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="68"/>
													<Segment index="1" value="74"/>
													<Segment index="2" value="69"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="36" begin_idx="0" end_id="39" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="61"/>
													<Segment index="1" value="-56"/>
													<Segment index="2" value="62"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="39" begin_idx="0" end_id="30" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="16"/>
													<Segment index="1" value="-98"/>
													<Segment index="2" value="16"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="39" begin_idx="0" end_id="37" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="69"/>
													<Segment index="1" value="-3"/>
													<Segment index="2" value="68"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
									</Connections>
								</PATH>
								<PATH ID="12" block_id="64" index="-1" name="Cv_wieksze_rowne_od_1">
									<Paths/>
									<Objects>
										<Object ID="1" flip="false" index="0" name="Out" type="subpath_output">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="1"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12639424"/>
													<Property name="Left" type="int" value="1053"/>
													<Property name="Top" type="int" value="578"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="2" flip="false" index="1" name="Demux 1" type="Demux">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Outputs" type="uint" value="10"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="119"/>
													<Property name="Top" type="int" value="105"/>
													<Property name="Width" type="int" value="20"/>
													<Property name="Height" type="int" value="700"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="4" flip="false" index="2" name="dodatnia" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="165"/>
													<Property name="Top" type="int" value="162"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="5" flip="false" index="3" name="ujemna" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="167"/>
													<Property name="Top" type="int" value="223"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="6" flip="false" index="4" name="w_beta" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="167"/>
													<Property name="Top" type="int" value="288"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="7" flip="false" index="5" name="w_alfa_beta" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="166"/>
													<Property name="Top" type="int" value="350"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="8" flip="false" index="6" name="m_m_beta" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="173"/>
													<Property name="Top" type="int" value="410"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="9" flip="false" index="7" name="m_m_alfa_m_beta" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="173"/>
													<Property name="Top" type="int" value="478"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="10" flip="false" index="8" name="Ax+B1" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="168"/>
													<Property name="Top" type="int" value="541"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="11" flip="false" index="9" name="Ax+B2" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="165"/>
													<Property name="Top" type="int" value="596"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="12" flip="false" index="10" name="Ax+B3" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="170"/>
													<Property name="Top" type="int" value="663"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="13" flip="false" index="11" name="Ax+B4" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="165"/>
													<Property name="Top" type="int" value="732"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="14" flip="false" index="12" name="Logic operator 1" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="2"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="438"/>
													<Property name="Top" type="int" value="149"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="15" flip="false" index="13" name="Logic operator 2" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="2"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="439"/>
													<Property name="Top" type="int" value="233"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="16" flip="false" index="14" name="Logic operator 3" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="2"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="439"/>
													<Property name="Top" type="int" value="323"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="17" flip="false" index="15" name="Logic operator 4" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="2"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="437"/>
													<Property name="Top" type="int" value="424"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="18" flip="false" index="16" name="Product 1" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="696"/>
													<Property name="Top" type="int" value="426"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="40"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="19" flip="false" index="17" name="Product 2" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="691"/>
													<Property name="Top" type="int" value="520"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="40"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="20" flip="false" index="18" name="Product 3" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="688"/>
													<Property name="Top" type="int" value="622"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="40"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="21" flip="false" index="19" name="Product 4" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="692"/>
													<Property name="Top" type="int" value="720"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="40"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="23" flip="false" index="20" name="Sum 1" type="sum">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Signs" type="string" value="++++"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="804"/>
													<Property name="Top" type="int" value="387"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="400"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="24" flip="false" index="21" name="Signal limiter 1" type="signal_limiter">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Low limit" type="real" value="-1"/>
													<Property name="High limit" type="real" value="1"/>
													<Property name="Rising rate limit" type="real" value="0"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="954"/>
													<Property name="Top" type="int" value="572"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="22" flip="false" index="22" name="Input" type="subpath_input">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="2"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="16"/>
													<Property name="Top" type="int" value="447"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="25" flip="false" index="23" name="CV" type="subpath_input">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="1"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="47"/>
													<Property name="Top" type="int" value="880"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="26" flip="false" index="24" name="XY Display 1" type="xy_display">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Buffer length" type="uint" value="1000"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12639424"/>
													<Property name="Left" type="int" value="1162"/>
													<Property name="Top" type="int" value="501"/>
													<Property name="Width" type="int" value="100"/>
													<Property name="Height" type="int" value="66"/>
													<Property name="X axis min" type="real" value="-1.244"/>
													<Property name="X axis max" type="real" value="1.244"/>
													<Property name="Y axis min" type="real" value="-1.5711059774203"/>
													<Property name="Y axis max" type="real" value="1.5711059774203"/>
													<Property name="Chart bkg color" type="color" value="16777215"/>
													<Property name="Chart axes color" type="color" value="12632256"/>
													<Property name="Chart plot color" type="color" value="0"/>
													<Property name="Chart bad sample color" type="color" value="255"/>
												</Properties>
											</Display>
										</Object>
									</Objects>
									<Connections>
										<Connection begin_id="4" begin_idx="0" end_id="14" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="109"/>
													<Segment index="1" value="-5"/>
													<Segment index="2" value="109"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="7" begin_idx="0" end_id="14" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="118"/>
													<Segment index="1" value="-178"/>
													<Segment index="2" value="99"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="14" begin_idx="0" end_id="18" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="116"/>
													<Segment index="1" value="268"/>
													<Segment index="2" value="87"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="10" begin_idx="0" end_id="18" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="388"/>
													<Segment index="1" value="-96"/>
													<Segment index="2" value="85"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="5" begin_idx="0" end_id="15" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="108"/>
													<Segment index="1" value="18"/>
													<Segment index="2" value="109"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="6" begin_idx="0" end_id="15" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="108"/>
													<Segment index="1" value="-32"/>
													<Segment index="2" value="109"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="15" begin_idx="0" end_id="19" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="102"/>
													<Segment index="1" value="278"/>
													<Segment index="2" value="95"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="11" begin_idx="0" end_id="19" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="239"/>
													<Segment index="1" value="-57"/>
													<Segment index="2" value="232"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="9" begin_idx="0" end_id="16" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="117"/>
													<Segment index="1" value="-147"/>
													<Segment index="2" value="94"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="5" begin_idx="0" end_id="16" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="108"/>
													<Segment index="1" value="123"/>
													<Segment index="2" value="109"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="16" begin_idx="0" end_id="20" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="101"/>
													<Segment index="1" value="290"/>
													<Segment index="2" value="93"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="12" begin_idx="0" end_id="20" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="231"/>
													<Segment index="1" value="-22"/>
													<Segment index="2" value="232"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="8" begin_idx="0" end_id="17" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="104"/>
													<Segment index="1" value="22"/>
													<Segment index="2" value="105"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="4" begin_idx="0" end_id="17" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="108"/>
													<Segment index="1" value="285"/>
													<Segment index="2" value="109"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="17" begin_idx="0" end_id="21" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="105"/>
													<Segment index="1" value="287"/>
													<Segment index="2" value="95"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="13" begin_idx="0" end_id="21" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="241"/>
													<Segment index="1" value="7"/>
													<Segment index="2" value="231"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="18" begin_idx="0" end_id="23" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="26"/>
													<Segment index="1" value="21"/>
													<Segment index="2" value="27"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="19" begin_idx="0" end_id="23" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="29"/>
													<Segment index="1" value="7"/>
													<Segment index="2" value="29"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="20" begin_idx="0" end_id="23" end_idx="2" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="30"/>
													<Segment index="1" value="-15"/>
													<Segment index="2" value="31"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="21" begin_idx="0" end_id="23" end_idx="3" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="28"/>
													<Segment index="1" value="-33"/>
													<Segment index="2" value="29"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="23" begin_idx="0" end_id="24" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="47"/>
													<Segment index="1" value="0"/>
													<Segment index="2" value="48"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="24" begin_idx="0" end_id="1" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="22"/>
													<Segment index="1" value="1"/>
													<Segment index="2" value="22"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="22" begin_idx="0" end_id="2" end_idx="0" type="20">
											<Display type="20">
												<Segments size="3">
													<Segment index="0" value="35"/>
													<Segment index="1" value="-2"/>
													<Segment index="2" value="43"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="24" begin_idx="0" end_id="26" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="29"/>
													<Segment index="1" value="-42"/>
													<Segment index="2" value="124"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="25" begin_idx="0" end_id="26" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="1033"/>
													<Segment index="1" value="-367"/>
													<Segment index="2" value="57"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="0" end_id="4" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="15"/>
													<Segment index="1" value="1"/>
													<Segment index="2" value="16"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="1" end_id="5" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="16"/>
													<Segment index="1" value="-1"/>
													<Segment index="2" value="17"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="2" end_id="6" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="16"/>
													<Segment index="1" value="1"/>
													<Segment index="2" value="17"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="3" end_id="7" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="16"/>
													<Segment index="1" value="0"/>
													<Segment index="2" value="16"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="4" end_id="8" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="19"/>
													<Segment index="1" value="-3"/>
													<Segment index="2" value="20"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="5" end_id="9" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="19"/>
													<Segment index="1" value="2"/>
													<Segment index="2" value="20"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="6" end_id="10" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="17"/>
													<Segment index="1" value="2"/>
													<Segment index="2" value="17"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="7" end_id="11" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="15"/>
													<Segment index="1" value="-6"/>
													<Segment index="2" value="16"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="8" end_id="12" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="18"/>
													<Segment index="1" value="-2"/>
													<Segment index="2" value="18"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="9" end_id="13" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="15"/>
													<Segment index="1" value="4"/>
													<Segment index="2" value="16"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
									</Connections>
								</PATH>
								<PATH ID="13" block_id="17" index="-1" name="Sterowanie_przeplywem">
									<Paths/>
									<Objects>
										<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="1"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="22"/>
													<Property name="Top" type="int" value="289"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="4" flip="false" index="1" name="Relational operator 1" type="relational">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Type" type="enum" value="2">
														<Enumeration index="0" name="EQUAL"/>
														<Enumeration index="1" name="GREATER"/>
														<Enumeration index="2" name="GREATER or EQUAL"/>
														<Enumeration index="3" name="LESS"/>
														<Enumeration index="4" name="LESS or EQUAL"/>
														<Enumeration index="5" name="NOT EQUAL"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="549"/>
													<Property name="Top" type="int" value="46"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="5" flip="false" index="2" name="Constant 1" type="constant">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Value" type="real" value="1"/>
													<Property name="Min" type="real" value="0"/>
													<Property name="Max" type="real" value="1"/>
													<Property name="Limit output" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="446"/>
													<Property name="Top" type="int" value="154"/>
													<Property name="Width" type="int" value="40"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="3" flip="false" index="3" name="wiekszy_od_1" type="subpath_output">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="2"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12639424"/>
													<Property name="Left" type="int" value="787"/>
													<Property name="Top" type="int" value="135"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="6" flip="false" index="4" name="mniejszy_od_1" type="subpath_output">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="1"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12639424"/>
													<Property name="Left" type="int" value="790"/>
													<Property name="Top" type="int" value="356"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="8" flip="false" index="5" name="Relational operator 2" type="relational">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Type" type="enum" value="3">
														<Enumeration index="0" name="EQUAL"/>
														<Enumeration index="1" name="GREATER"/>
														<Enumeration index="2" name="GREATER or EQUAL"/>
														<Enumeration index="3" name="LESS"/>
														<Enumeration index="4" name="LESS or EQUAL"/>
														<Enumeration index="5" name="NOT EQUAL"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="304"/>
													<Property name="Top" type="int" value="384"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="9" flip="false" index="6" name="Constant 2" type="constant">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Value" type="real" value="1"/>
													<Property name="Min" type="real" value="0"/>
													<Property name="Max" type="real" value="1"/>
													<Property name="Limit output" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="106"/>
													<Property name="Top" type="int" value="440"/>
													<Property name="Width" type="int" value="40"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="7" flip="false" index="7" name="MinMax Selector 1" type="min_max_select">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs count" type="uint" value="2"/>
													<Property name="Function" type="enum" value="0">
														<Enumeration index="0" name="Max"/>
														<Enumeration index="1" name="Min"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="251"/>
													<Property name="Top" type="int" value="53"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="52"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="11" flip="true" index="8" name="One step delay 1" type="one_step_del">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Init value" type="real" value="0"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="249"/>
													<Property name="Top" type="int" value="248"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="10" flip="true" index="9" name="Logic operator 1" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="1"/>
													<Property name="Type" type="enum" value="3">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="563"/>
													<Property name="Top" type="int" value="240"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="13" flip="false" index="10" name="Logic operator 2" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="2"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="657"/>
													<Property name="Top" type="int" value="344"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
									</Objects>
									<Connections>
										<Connection begin_id="4" begin_idx="0" end_id="3" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="64"/>
													<Segment index="1" value="77"/>
													<Segment index="2" value="119"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="0" begin_idx="0" end_id="8" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="69"/>
													<Segment index="1" value="100"/>
													<Segment index="2" value="188"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="9" begin_idx="0" end_id="8" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="90"/>
													<Segment index="1" value="-41"/>
													<Segment index="2" value="73"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="0" begin_idx="0" end_id="7" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="100"/>
													<Segment index="1" value="-229"/>
													<Segment index="2" value="104"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="7" begin_idx="0" end_id="11" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="19"/>
													<Segment index="1" value="200"/>
													<Segment index="2" value="-21"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="11" begin_idx="0" end_id="7" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="-20"/>
													<Segment index="1" value="-183"/>
													<Segment index="2" value="22"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="7" begin_idx="0" end_id="4" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="121"/>
													<Segment index="1" value="-9"/>
													<Segment index="2" value="122"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="5" begin_idx="0" end_id="4" end_idx="1" type="1">
											<Display type="1">
												<Segments size="5">
													<Segment index="0" value="45"/>
													<Segment index="1" value="-48"/>
													<Segment index="2" value="13"/>
													<Segment index="3" value="-45"/>
													<Segment index="4" value="10"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="4" begin_idx="0" end_id="10" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="31"/>
													<Segment index="1" value="194"/>
													<Segment index="2" value="-17"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="8" begin_idx="0" end_id="13" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="107"/>
													<Segment index="1" value="-32"/>
													<Segment index="2" value="191"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="10" begin_idx="0" end_id="13" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="-24"/>
													<Segment index="1" value="97"/>
													<Segment index="2" value="118"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="13" begin_idx="0" end_id="6" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="38"/>
													<Segment index="1" value="0"/>
													<Segment index="2" value="40"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
									</Connections>
								</PATH>
							</Paths>
							<Objects>
								<Object ID="0" flip="false" index="0" name="In" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="50"/>
											<Property name="Top" type="int" value="597"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="Demux 1" type="Demux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Outputs" type="uint" value="10"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="138"/>
											<Property name="Top" type="int" value="88"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="500"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="dodatnia" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="245"/>
											<Property name="Top" type="int" value="95"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="ujemna" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="245"/>
											<Property name="Top" type="int" value="135"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="CVbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="249"/>
											<Property name="Top" type="int" value="182"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Cv>alfaplusbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="243"/>
											<Property name="Top" type="int" value="232"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="329"/>
											<Property name="Top" type="int" value="47"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="CVbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="241"/>
											<Property name="Top" type="int" value="280"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="CValfaplusbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="243"/>
											<Property name="Top" type="int" value="326"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="Ax+B1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="242"/>
											<Property name="Top" type="int" value="376"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="Ax+B2" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="240"/>
											<Property name="Top" type="int" value="429"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="Ax+B4" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="233"/>
											<Property name="Top" type="int" value="530"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="Ax+B3" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="237"/>
											<Property name="Top" type="int" value="480"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="27" flip="false" index="13" name="out" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1286"/>
											<Property name="Top" type="int" value="341"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="62" flip="false" index="14" name="CV_mniejsze_od_1" type="subpath">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="561"/>
											<Property name="Top" type="int" value="362"/>
											<Property name="Width" type="int" value="200"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="63" flip="false" index="15" name="Mux 1" type="Mux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs" type="uint" value="7"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="447"/>
											<Property name="Top" type="int" value="150"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="135"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="64" flip="false" index="16" name="Cv_wieksze_rowne_od_1" type="subpath">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="557"/>
											<Property name="Top" type="int" value="567"/>
											<Property name="Width" type="int" value="200"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="17" flip="false" index="17" name="Sterowanie_przeplywem" type="subpath">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="540"/>
											<Property name="Top" type="int" value="138"/>
											<Property name="Width" type="int" value="200"/>
											<Property name="Height" type="int" value="100"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="19" flip="false" index="18" name="Product 1" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="879"/>
											<Property name="Top" type="int" value="265"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="20" flip="false" index="19" name="Product 2" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="879"/>
											<Property name="Top" type="int" value="388"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="21" flip="false" index="20" name="XY Display 1" type="xy_display">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer length" type="uint" value="1000"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1245"/>
											<Property name="Top" type="int" value="99"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="66"/>
											<Property name="X axis min" type="real" value="-1.144"/>
											<Property name="X axis max" type="real" value="1.144"/>
											<Property name="Y axis min" type="real" value="-1.04"/>
											<Property name="Y axis max" type="real" value="1.04"/>
											<Property name="Chart bkg color" type="color" value="16777215"/>
											<Property name="Chart axes color" type="color" value="12632256"/>
											<Property name="Chart plot color" type="color" value="0"/>
											<Property name="Chart bad sample color" type="color" value="255"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="23" buffer_type="1" flip="false" index="21" name="Internal variable 2" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 3"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="936"/>
											<Property name="Top" type="int" value="151"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="22" buffer_type="1" flip="false" index="22" name="Internal variable 1" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 3 1"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="938"/>
											<Property name="Top" type="int" value="86"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="24" buffer_type="1" flip="false" index="23" name="Internal variable 3" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 3 2"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1092"/>
											<Property name="Top" type="int" value="443"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="25" buffer_type="1" flip="false" index="24" name="Internal variable 4" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 3 2 1"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1067"/>
											<Property name="Top" type="int" value="223"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="26" flip="false" index="25" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1079"/>
											<Property name="Top" type="int" value="323"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="1" begin_idx="0" end_id="2" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="32"/>
											<Segment index="1" value="-31"/>
											<Segment index="2" value="60"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="1" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="33"/>
											<Segment index="1" value="-36"/>
											<Segment index="2" value="59"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="2" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="36"/>
											<Segment index="1" value="-34"/>
											<Segment index="2" value="60"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="4" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="37"/>
											<Segment index="1" value="-26"/>
											<Segment index="2" value="51"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="5" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="40"/>
											<Segment index="1" value="-25"/>
											<Segment index="2" value="50"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="6" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="40"/>
											<Segment index="1" value="-20"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="7" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="39"/>
											<Segment index="1" value="-12"/>
											<Segment index="2" value="48"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="8" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="59"/>
											<Segment index="1" value="-6"/>
											<Segment index="2" value="25"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="9" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="42"/>
											<Segment index="1" value="-1"/>
											<Segment index="2" value="38"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="3" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="59"/>
											<Segment index="1" value="-29"/>
											<Segment index="2" value="31"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="1" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="47"/>
											<Segment index="1" value="-269"/>
											<Segment index="2" value="16"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="64" end_idx="1" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="281"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="201"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="63" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="46"/>
											<Segment index="1" value="109"/>
											<Segment index="2" value="47"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="63" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="78"/>
											<Segment index="1" value="80"/>
											<Segment index="2" value="69"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="63" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="67"/>
											<Segment index="1" value="56"/>
											<Segment index="2" value="80"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="63" end_idx="3" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="68"/>
											<Segment index="1" value="-25"/>
											<Segment index="2" value="81"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="63" end_idx="4" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="77"/>
											<Segment index="1" value="-103"/>
											<Segment index="2" value="72"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="63" end_idx="5" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="96"/>
											<Segment index="1" value="-137"/>
											<Segment index="2" value="54"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="63" end_idx="6" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="120"/>
											<Segment index="1" value="-225"/>
											<Segment index="2" value="35"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="63" begin_idx="0" end_id="62" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="50"/>
											<Segment index="1" value="167"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="64" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="58"/>
											<Segment index="1" value="530"/>
											<Segment index="2" value="145"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="17" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="168"/>
											<Segment index="1" value="131"/>
											<Segment index="2" value="18"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="62" begin_idx="0" end_id="19" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="75"/>
											<Segment index="1" value="-93"/>
											<Segment index="2" value="48"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="19" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="141"/>
											<Segment index="1" value="107"/>
											<Segment index="2" value="3"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="1" end_id="20" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="75"/>
											<Segment index="1" value="197"/>
											<Segment index="2" value="69"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="64" begin_idx="0" end_id="20" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="58"/>
											<Segment index="1" value="-183"/>
											<Segment index="2" value="69"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="21" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="825"/>
											<Segment index="1" value="64"/>
											<Segment index="2" value="66"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="1" end_id="23" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="171"/>
											<Segment index="1" value="-35"/>
											<Segment index="2" value="30"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="22" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="123"/>
											<Segment index="1" value="-67"/>
											<Segment index="2" value="80"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="19" begin_idx="0" end_id="25" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="36"/>
											<Segment index="1" value="-44"/>
											<Segment index="2" value="97"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="19" begin_idx="0" end_id="26" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="118"/>
											<Segment index="1" value="58"/>
											<Segment index="2" value="27"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="26" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="114"/>
											<Segment index="1" value="-45"/>
											<Segment index="2" value="31"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="26" begin_idx="0" end_id="21" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="49"/>
											<Segment index="1" value="-210"/>
											<Segment index="2" value="62"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="26" begin_idx="0" end_id="27" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="79"/>
											<Segment index="1" value="-2"/>
											<Segment index="2" value="73"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="24" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="79"/>
											<Segment index="1" value="53"/>
											<Segment index="2" value="79"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
					</Paths>
					<Objects>
						<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="19"/>
									<Property name="Top" type="int" value="50"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="22"/>
									<Property name="Top" type="int" value="149"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="3"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="26"/>
									<Property name="Top" type="int" value="241"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="56" flip="false" index="3" name="Krzywa" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="164"/>
									<Property name="Top" type="int" value="280"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="90"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="43" flip="false" index="4" name="Pochodna" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="163"/>
									<Property name="Top" type="int" value="71"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="60"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="44" flip="false" index="5" name="Przedzialy" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="162"/>
									<Property name="Top" type="int" value="164"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="90"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="36" flip="false" index="6" name="Mux 1" type="Mux">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs" type="uint" value="10"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="465"/>
									<Property name="Top" type="int" value="71"/>
									<Property name="Width" type="int" value="20"/>
									<Property name="Height" type="int" value="350"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="15" flip="false" index="7" name="out2" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="878"/>
									<Property name="Top" type="int" value="232"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="11" flip="false" index="8" name="Logika" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="591"/>
									<Property name="Top" type="int" value="209"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="60"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="0" begin_idx="0" end_id="43" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="89"/>
									<Segment index="1" value="41"/>
									<Segment index="2" value="30"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="44" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="101"/>
									<Segment index="1" value="126"/>
									<Segment index="2" value="17"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="56" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="59"/>
									<Segment index="1" value="242"/>
									<Segment index="2" value="61"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="44" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="75"/>
									<Segment index="1" value="49"/>
									<Segment index="2" value="40"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="56" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="41"/>
									<Segment index="1" value="165"/>
									<Segment index="2" value="76"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="44" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="71"/>
									<Segment index="1" value="-21"/>
									<Segment index="2" value="40"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="56" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="69"/>
									<Segment index="1" value="95"/>
									<Segment index="2" value="44"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="43" begin_idx="0" end_id="36" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="42"/>
									<Segment index="1" value="11"/>
									<Segment index="2" value="65"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="43" begin_idx="1" end_id="36" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="42"/>
									<Segment index="1" value="22"/>
									<Segment index="2" value="65"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="0" end_id="36" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="51"/>
									<Segment index="1" value="-18"/>
									<Segment index="2" value="57"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="1" end_id="36" end_idx="3" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="51"/>
									<Segment index="1" value="-5"/>
									<Segment index="2" value="57"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="2" end_id="36" end_idx="4" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="51"/>
									<Segment index="1" value="8"/>
									<Segment index="2" value="57"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="3" end_id="36" end_idx="5" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="51"/>
									<Segment index="1" value="21"/>
									<Segment index="2" value="57"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="0" end_id="36" end_idx="6" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="-10"/>
									<Segment index="2" value="50"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="1" end_id="36" end_idx="7" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="50"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="2" end_id="36" end_idx="8" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="16"/>
									<Segment index="2" value="50"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="3" end_id="36" end_idx="9" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="29"/>
									<Segment index="2" value="50"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="36" begin_idx="0" end_id="11" end_idx="1" type="20">
							<Display type="20">
								<Segments size="3">
									<Segment index="0" value="43"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="68"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="11" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="499"/>
									<Segment index="1" value="169"/>
									<Segment index="2" value="48"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="11" begin_idx="0" end_id="15" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="58"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="34"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
				<PATH ID="10" block_id="13" index="-1" name="madel zaklocenia  Gz">
					<Paths/>
					<Objects>
						<Object ID="0" flip="false" index="0" name="" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="34"/>
									<Property name="Top" type="int" value="191"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="637"/>
									<Property name="Top" type="int" value="175"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="3" flip="false" index="2" name="1st order inertia 1" type="first_order_inertia">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="K" type="real" value="1"/>
									<Property name="T" type="real" value="100"/>
									<Property name="Y0[k-1]" type="real" value="0"/>
									<Property name="U0[k-1]" type="real" value="0"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="342"/>
									<Property name="Top" type="int" value="182"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="5" flip="false" index="3" name="1st order integrator 1" type="first_order_integrator">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="K" type="real" value="0.01"/>
									<Property name="T" type="real" value="1"/>
									<Property name="Y0[k-1]" type="real" value="0"/>
									<Property name="U0[k-1]" type="real" value="0"/>
									<Property name="Low limit" type="real" value="0"/>
									<Property name="High limit" type="real" value="0"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="229"/>
									<Property name="Top" type="int" value="182"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="0" begin_idx="0" end_id="5" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="85"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="85"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="5" begin_idx="0" end_id="3" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="29"/>
									<Segment index="1" value="0"/>
									<Segment index="2" value="29"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="3" begin_idx="0" end_id="1" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="90"/>
									<Segment index="1" value="-19"/>
									<Segment index="2" value="90"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
				<PATH ID="6" block_id="15" index="-1" name="Model Procesu  Gp">
					<Paths/>
					<Objects>
						<Object ID="0" flip="false" index="0" name="" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="116"/>
									<Property name="Top" type="int" value="240"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="1141"/>
									<Property name="Top" type="int" value="232"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="2" flip="false" index="2" name="2nd order inertia 1" type="second_order_inertia">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="K1" type="real" value="2"/>
									<Property name="K2" type="real" value="10"/>
									<Property name="T1" type="real" value="2"/>
									<Property name="T2" type="real" value="10"/>
									<Property name="Y0[k-1]" type="real" value="0"/>
									<Property name="Y0[k-2]" type="real" value="0"/>
									<Property name="U0[k-1]" type="real" value="0"/>
									<Property name="U0[k-2]" type="real" value="0"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="494"/>
									<Property name="Top" type="int" value="222"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="4" flip="false" index="3" name="Transfer function (cont.) 1" type="cld_transfer_function">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Nominator order" type="uint" value="1"/>
									<Property name="Denominator order" type="uint" value="2"/>
									<Property name="Nominator" size="1" type="double_vect">
										<Row index="0" value="1"/>
									</Property>
									<Property name="Denominator" size="2" type="double_vect">
										<Row index="0" value="1"/>
										<Row index="1" value="-25"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="686"/>
									<Property name="Top" type="int" value="225"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="5" flip="false" index="4" name="N-step delay 1" type="integer_delay">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="316"/>
									<Property name="Top" type="int" value="230"/>
									<Property name="Width" type="int" value="100"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="6" flip="false" index="5" name="Constant 1" type="constant">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Value" type="real" value="40"/>
									<Property name="Min" type="real" value="0"/>
									<Property name="Max" type="real" value="1"/>
									<Property name="Limit output" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="148"/>
									<Property name="Top" type="int" value="375"/>
									<Property name="Width" type="int" value="100"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="2" begin_idx="0" end_id="4" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="60"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="17"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="4" begin_idx="0" end_id="1" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="193"/>
									<Segment index="1" value="-5"/>
									<Segment index="2" value="147"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="6" begin_idx="0" end_id="5" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="20"/>
									<Segment index="1" value="-130"/>
									<Segment index="2" value="53"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="5" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="71"/>
									<Segment index="1" value="-5"/>
									<Segment index="2" value="104"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="5" begin_idx="0" end_id="2" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="54"/>
									<Segment index="1" value="-8"/>
									<Segment index="2" value="29"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
			</Paths>
			<Objects>
				<Object ID="1" flip="false" index="0" name="alfa" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0.1"/>
							<Property name="Min" type="real" value="0"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="107"/>
							<Property name="Top" type="int" value="412"/>
							<Property name="Width" type="int" value="50"/>
							<Property name="Height" type="int" value="20"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="2" flip="false" index="1" name="beta" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0.05"/>
							<Property name="Min" type="real" value="0"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="107"/>
							<Property name="Top" type="int" value="458"/>
							<Property name="Width" type="int" value="50"/>
							<Property name="Height" type="int" value="20"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="7" flip="false" index="2" name="ElNL" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="65535"/>
							<Property name="Left" type="int" value="377"/>
							<Property name="Top" type="int" value="398"/>
							<Property name="Width" type="int" value="150"/>
							<Property name="Height" type="int" value="75"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="11" flip="false" index="3" name="PID 1" type="pid_controller">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="k" type="real" value="-2"/>
							<Property name="Ti" type="real" value="22"/>
							<Property name="Td" type="real" value="1.2"/>
							<Property name="Differentiate inertia" type="real" value="0.01"/>
							<Property name="Output low limit" type="real" value="-10"/>
							<Property name="Output high limit" type="real" value="10"/>
							<Property name="Initial output" type="real" value="0"/>
							<Property name="Bias" type="real" value="0"/>
							<Property name="Setpoint derivative" type="bool" value="true"/>
							<Property name="Setpoint proportional" type="bool" value="true"/>
							<Property name="Stop integrate on limits" type="bool" value="true"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="255"/>
							<Property name="Left" type="int" value="126"/>
							<Property name="Top" type="int" value="278"/>
							<Property name="Width" type="int" value="80"/>
							<Property name="Height" type="int" value="45"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="13" flip="false" index="4" name="madel zaklocenia  Gz" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="16776960"/>
							<Property name="Left" type="int" value="651"/>
							<Property name="Top" type="int" value="229"/>
							<Property name="Width" type="int" value="150"/>
							<Property name="Height" type="int" value="45"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="15" flip="false" index="5" name="Model Procesu  Gp" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="16776960"/>
							<Property name="Left" type="int" value="600"/>
							<Property name="Top" type="int" value="309"/>
							<Property name="Width" type="int" value="150"/>
							<Property name="Height" type="int" value="45"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="17" flip="false" index="6" name="Constant 1" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0"/>
							<Property name="Min" type="real" value="-1"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="556"/>
							<Property name="Top" type="int" value="138"/>
							<Property name="Width" type="int" value="50"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="18" flip="false" index="7" name="Display 2" type="display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="2500"/>
							<Property name="Inputs count" type="uint" value="2"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="666"/>
							<Property name="Top" type="int" value="55"/>
							<Property name="Width" type="int" value="170"/>
							<Property name="Height" type="int" value="60"/>
							<Property name="Chart min" type="real" value="-2"/>
							<Property name="Chart max" type="real" value="56115.4373185785"/>
							<Property name="Autoscale" type="bool" value="true"/>
							<Property name="Scale each sep." type="bool" value="false"/>
							<Property name="Precision" type="int" value="3"/>
							<Property name="Digits" type="int" value="3"/>
							<Property name="Type" type="enum" value="1">
								<Enumeration index="0" name="Fixed"/>
								<Enumeration index="1" name="General"/>
								<Enumeration index="2" name="Scientific"/>
							</Property>
							<Property name="Bkg color" type="color" value="-16777198"/>
							<Property name="Axes color" type="color" value="8421504"/>
							<Property name="Bad sample color" type="color" value="255"/>
							<Property name="Show sample nr" type="bool" value="false"/>
							<Property name="Color scheme" type="enum" value="0">
								<Enumeration index="0" name="dark"/>
								<Enumeration index="1" name="light"/>
							</Property>
						</Properties>
					</Display>
				</Object>
				<Object ID="22" flip="false" index="8" name="Sum 4" type="sum">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Signs" type="string" value="++"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="-16777214"/>
							<Property name="Left" type="int" value="905"/>
							<Property name="Top" type="int" value="274"/>
							<Property name="Width" type="int" value="60"/>
							<Property name="Height" type="int" value="60"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="23" flip="false" index="9" name="Display 1" type="display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="900"/>
							<Property name="Inputs count" type="uint" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="1008"/>
							<Property name="Top" type="int" value="315"/>
							<Property name="Width" type="int" value="170"/>
							<Property name="Height" type="int" value="45"/>
							<Property name="Chart min" type="real" value="-80199530857041.4"/>
							<Property name="Chart max" type="real" value="0"/>
							<Property name="Autoscale" type="bool" value="false"/>
							<Property name="Scale each sep." type="bool" value="false"/>
							<Property name="Precision" type="int" value="3"/>
							<Property name="Digits" type="int" value="3"/>
							<Property name="Type" type="enum" value="1">
								<Enumeration index="0" name="Fixed"/>
								<Enumeration index="1" name="General"/>
								<Enumeration index="2" name="Scientific"/>
							</Property>
							<Property name="Bkg color" type="color" value="0"/>
							<Property name="Axes color" type="color" value="8421504"/>
							<Property name="Bad sample color" type="color" value="255"/>
							<Property name="Show sample nr" type="bool" value="false"/>
							<Property name="Color scheme" type="enum" value="0">
								<Enumeration index="0" name="dark"/>
								<Enumeration index="1" name="light"/>
							</Property>
						</Properties>
					</Display>
				</Object>
				<Object ID="14" flip="false" index="10" name="Random 1" type="random">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Minimum" type="real" value="-0.5"/>
							<Property name="Maximum" type="real" value="0.5"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="462"/>
							<Property name="Top" type="int" value="231"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="16" flip="false" index="11" name="Periodical signal 1" type="src_periodic">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Frequency" type="real" value="0.001"/>
							<Property name="Phase" type="real" value="0"/>
							<Property name="Amplitude" type="real" value="1"/>
							<Property name="Bias" type="real" value="0"/>
							<Property name="Function" type="enum" value="1">
								<Enumeration index="0" name="sawtooth"/>
								<Enumeration index="1" name="sinus"/>
								<Enumeration index="2" name="square"/>
							</Property>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="1"/>
							<Property name="Top" type="int" value="69"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="20" flip="true" index="12" name="Time event 1" type="src_time_event">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Start time" type="real" value="1"/>
							<Property name="Stop time" type="real" value="100"/>
							<Property name="Amplitude" type="real" value="10"/>
							<Property name="Bias" type="real" value="0"/>
							<Property name="Kind" type="enum" value="2">
								<Enumeration index="0" name="impulse"/>
								<Enumeration index="1" name="ramp"/>
								<Enumeration index="2" name="step"/>
							</Property>
							<Property name="Reset time" type="real" value="1000000"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="280"/>
							<Property name="Top" type="int" value="162"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="21" flip="true" index="13" name="Gain 1" type="gain">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Gain" type="real" value="0.1"/>
							<Property name="Clear sig. status" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="123"/>
							<Property name="Top" type="int" value="166"/>
							<Property name="Width" type="int" value="60"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
			</Objects>
			<Connections>
				<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="135"/>
							<Segment index="1" value="12"/>
							<Segment index="2" value="90"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="2" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="149"/>
							<Segment index="1" value="-16"/>
							<Segment index="2" value="76"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="20" begin_idx="0" end_id="21" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="-77"/>
							<Segment index="1" value="4"/>
							<Segment index="2" value="-25"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="21" begin_idx="0" end_id="11" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="-56"/>
							<Segment index="1" value="112"/>
							<Segment index="2" value="59"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="11" begin_idx="0" end_id="15" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="201"/>
							<Segment index="1" value="31"/>
							<Segment index="2" value="198"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="14" begin_idx="0" end_id="13" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="77"/>
							<Segment index="1" value="5"/>
							<Segment index="2" value="17"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="15" begin_idx="0" end_id="22" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="107"/>
							<Segment index="1" value="-17"/>
							<Segment index="2" value="53"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="13" begin_idx="0" end_id="22" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="69"/>
							<Segment index="1" value="43"/>
							<Segment index="2" value="40"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="22" begin_idx="0" end_id="11" end_idx="1" type="1">
					<Display type="1">
						<Segments size="5">
							<Segment index="0" value="7"/>
							<Segment index="1" value="260"/>
							<Segment index="2" value="-916"/>
							<Segment index="3" value="-256"/>
							<Segment index="4" value="75"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="11" begin_idx="0" end_id="18" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="218"/>
							<Segment index="1" value="-225"/>
							<Segment index="2" value="247"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="15" begin_idx="0" end_id="18" end_idx="1" type="1">
					<Display type="1">
						<Segments size="5">
							<Segment index="0" value="85"/>
							<Segment index="1" value="-118"/>
							<Segment index="2" value="-189"/>
							<Segment index="3" value="-118"/>
							<Segment index="4" value="25"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="22" begin_idx="0" end_id="23" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="24"/>
							<Segment index="1" value="33"/>
							<Segment index="2" value="24"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
			</Connections>
		</PATH>
	</Paths>
</CalcPaths_Export_File>
