<CalcPaths_Export_File>
	<Simulator after_time="0" max_steps="10000" sample_time="0.01" simulation_rate="10" simulation_ratio="1" start_time="1984-01-01T00:00:00.000" stop_condition="3" stop_time="1984-01-01T00:00:00.000"/>
	<Paths>
		<PATH ID="2" block_id="-1" index="-1" name="Path 1">
			<Paths>
				<PATH ID="9" block_id="7" index="-1" name="Subpath 1">
					<Paths>
						<PATH ID="4" block_id="46" index="-1" name="przypadekXmniejszyOd1dlaPlus">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="65"/>
											<Property name="Top" type="int" value="140"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="659"/>
											<Property name="Top" type="int" value="165"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="57"/>
											<Property name="Top" type="int" value="216"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="+-"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="187"/>
											<Property name="Top" type="int" value="135"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="63"/>
											<Property name="Top" type="int" value="296"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="a" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="367"/>
											<Property name="Top" type="int" value="80"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="6" name="b2" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="-i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="369"/>
											<Property name="Top" type="int" value="204"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="7" name="Sum 2" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="551"/>
											<Property name="Top" type="int" value="155"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="50"/>
											<Segment index="1" value="5"/>
											<Segment index="2" value="47"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="3" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="48"/>
											<Segment index="1" value="-51"/>
											<Segment index="2" value="57"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="40"/>
											<Segment index="1" value="-63"/>
											<Segment index="2" value="85"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="211"/>
											<Segment index="1" value="-182"/>
											<Segment index="2" value="68"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="147"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="140"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="7" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="235"/>
											<Segment index="1" value="-58"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="23"/>
											<Segment index="1" value="55"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="8" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="21"/>
											<Segment index="1" value="-56"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="1" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="10"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="43"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="6" block_id="52" index="-1" name="przypadekXmniejszyOd1dlaMinus">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="65"/>
											<Property name="Top" type="int" value="140"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="659"/>
											<Property name="Top" type="int" value="165"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="70"/>
											<Property name="Top" type="int" value="216"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="176"/>
											<Property name="Top" type="int" value="144"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="71"/>
											<Property name="Top" type="int" value="296"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="a" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="367"/>
											<Property name="Top" type="int" value="80"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="6" name="Sum 2" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="551"/>
											<Property name="Top" type="int" value="155"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="7" name="Math expression 1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="374"/>
											<Property name="Top" type="int" value="179"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="8" begin_idx="0" end_id="1" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="10"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="43"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="43"/>
											<Segment index="1" value="14"/>
											<Segment index="2" value="43"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="3" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="40"/>
											<Segment index="1" value="-42"/>
											<Segment index="2" value="41"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="68"/>
											<Segment index="1" value="-72"/>
											<Segment index="2" value="68"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="228"/>
											<Segment index="1" value="-182"/>
											<Segment index="2" value="43"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="10" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="172"/>
											<Segment index="1" value="-83"/>
											<Segment index="2" value="106"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="8" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="29"/>
											<Segment index="1" value="-31"/>
											<Segment index="2" value="33"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="34"/>
											<Segment index="1" value="55"/>
											<Segment index="2" value="35"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="139"/>
											<Segment index="1" value="-25"/>
											<Segment index="2" value="140"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
					</Paths>
					<Objects>
						<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="33"/>
									<Property name="Top" type="int" value="74"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="48"/>
									<Property name="Top" type="int" value="165"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="3"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="48"/>
									<Property name="Top" type="int" value="212"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="3" flip="false" index="3" name="dCV" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="i1[0]-i1[1]"/>
									<Property name="Inputs" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="162"/>
									<Property name="Top" type="int" value="39"/>
									<Property name="Width" type="int" value="80"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="5" flip="false" index="4" name="a" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="1/(1-i1[0]-i2[0])"/>
									<Property name="Inputs" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="289"/>
									<Property name="Top" type="int" value="389"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="8" flip="false" index="5" name="b1" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="1-1/(1-i1[0]-i2[0])"/>
									<Property name="Inputs" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="265"/>
									<Property name="Top" type="int" value="479"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="9" flip="false" index="6" name="b2" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="-i2[0]/(1-i1[0]-i2[0])"/>
									<Property name="Inputs" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="265"/>
									<Property name="Top" type="int" value="537"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="10" flip="false" index="7" name="RosnacaPlus" type="logical">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs number" type="int" value="2"/>
									<Property name="Type" type="enum" value="0">
										<Enumeration index="0" name="AND"/>
										<Enumeration index="1" name="NAND"/>
										<Enumeration index="2" name="NOR"/>
										<Enumeration index="3" name="NOT"/>
										<Enumeration index="4" name="OR"/>
										<Enumeration index="5" name="XOR"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="846"/>
									<Property name="Top" type="int" value="60"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="11" flip="false" index="8" name="RosnacaMinus" type="logical">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs number" type="int" value="2"/>
									<Property name="Type" type="enum" value="0">
										<Enumeration index="0" name="AND"/>
										<Enumeration index="1" name="NAND"/>
										<Enumeration index="2" name="NOR"/>
										<Enumeration index="3" name="NOT"/>
										<Enumeration index="4" name="OR"/>
										<Enumeration index="5" name="XOR"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="788"/>
									<Property name="Top" type="int" value="302"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="14" flip="false" index="9" name="b3" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="i2[0]/(1-i1[0]-i2[0])"/>
									<Property name="Inputs" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="267"/>
									<Property name="Top" type="int" value="598"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="13" flip="false" index="10" name="b4" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="-1+1/(1-i1[0]-i2[0])"/>
									<Property name="Inputs" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="362"/>
									<Property name="Top" type="int" value="74"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="15" flip="false" index="11" name="Pochodna ujemna" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="4">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="350"/>
									<Property name="Top" type="int" value="91"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="12" flip="false" index="12" name="Pochodna  dodatnia" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="348"/>
									<Property name="Top" type="int" value="33"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="16" flip="false" index="13" name="" type="constant">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Value" type="real" value="0"/>
									<Property name="Min" type="real" value="0"/>
									<Property name="Max" type="real" value="1"/>
									<Property name="Limit output" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="247"/>
									<Property name="Top" type="int" value="127"/>
									<Property name="Width" type="int" value="40"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="17" flip="false" index="14" name="alfa+beta" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="1"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="347"/>
									<Property name="Top" type="int" value="181"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="18" flip="false" index="15" name="Sum 1" type="sum">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Signs" type="string" value="++"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="264"/>
									<Property name="Top" type="int" value="178"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="19" flip="false" index="16" name="rosnacaPlus" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="599"/>
									<Property name="Top" type="int" value="126"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="20" flip="false" index="17" name="CV" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="1"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="435"/>
									<Property name="Top" type="int" value="120"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="21" flip="false" index="18" name="CV" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="1"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="577"/>
									<Property name="Top" type="int" value="400"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="22" flip="false" index="19" name="A*x" type="product">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Actions string" type="string" value="**"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="666"/>
									<Property name="Top" type="int" value="429"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="23" flip="false" index="20" name="A*x+b1" type="sum">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Signs" type="string" value="++"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="763"/>
									<Property name="Top" type="int" value="464"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="24" flip="false" index="21" name="Sum 3" type="sum">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Signs" type="string" value="++"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="759"/>
									<Property name="Top" type="int" value="531"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="25" flip="false" index="22" name="Sum 4" type="sum">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Signs" type="string" value="++"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="758"/>
									<Property name="Top" type="int" value="590"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="26" flip="false" index="23" name="Sum 5" type="sum">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Signs" type="string" value="++"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="761"/>
									<Property name="Top" type="int" value="648"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="27" flip="false" index="24" name="Sum 6" type="sum">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Signs" type="string" value="+++++++"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1616"/>
									<Property name="Top" type="int" value="357"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="135"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="28" flip="false" index="25" name="Product 2" type="product">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Actions string" type="string" value="**"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1068"/>
									<Property name="Top" type="int" value="269"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="29" flip="false" index="26" name="Product 3" type="product">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Actions string" type="string" value="***"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1397"/>
									<Property name="Top" type="int" value="292"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="75"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="30" flip="false" index="27" name="Product 4" type="product">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Actions string" type="string" value="***"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1383"/>
									<Property name="Top" type="int" value="474"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="75"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="31" flip="false" index="28" name="Product 5" type="product">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Actions string" type="string" value="**"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1072"/>
									<Property name="Top" type="int" value="421"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="60"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="32" flip="false" index="29" name="Constant 1" type="constant">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Value" type="real" value="0"/>
									<Property name="Min" type="real" value="0"/>
									<Property name="Max" type="real" value="1"/>
									<Property name="Limit output" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="1252"/>
									<Property name="Top" type="int" value="572"/>
									<Property name="Width" type="int" value="40"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="33" flip="false" index="30" name="Signal limiter 1" type="signal_limiter">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Low limit" type="real" value="-1"/>
									<Property name="High limit" type="real" value="1"/>
									<Property name="Rising rate limit" type="real" value="0"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1718"/>
									<Property name="Top" type="int" value="396"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="34" flip="false" index="31" name="malejacaPlus" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="603"/>
									<Property name="Top" type="int" value="185"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="35" flip="false" index="32" name="MalejacaPlus" type="logical">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs number" type="int" value="2"/>
									<Property name="Type" type="enum" value="0">
										<Enumeration index="0" name="AND"/>
										<Enumeration index="1" name="NAND"/>
										<Enumeration index="2" name="NOR"/>
										<Enumeration index="3" name="NOT"/>
										<Enumeration index="4" name="OR"/>
										<Enumeration index="5" name="XOR"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="784"/>
									<Property name="Top" type="int" value="223"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="36" flip="false" index="33" name="rosnacaMinus" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="4">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="603"/>
									<Property name="Top" type="int" value="267"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="37" flip="false" index="34" name="malejacaMinus" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="4">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="599"/>
									<Property name="Top" type="int" value="332"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="38" flip="false" index="35" name="-beta" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="-1"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="313"/>
									<Property name="Top" type="int" value="285"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="39" flip="false" index="36" name="-(alfa+beta)" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="-1"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="445"/>
									<Property name="Top" type="int" value="347"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="40" flip="false" index="37" name="MalejacaMinus" type="logical">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs number" type="int" value="2"/>
									<Property name="Type" type="enum" value="0">
										<Enumeration index="0" name="AND"/>
										<Enumeration index="1" name="NAND"/>
										<Enumeration index="2" name="NOR"/>
										<Enumeration index="3" name="NOT"/>
										<Enumeration index="4" name="OR"/>
										<Enumeration index="5" name="XOR"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="774"/>
									<Property name="Top" type="int" value="389"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="41" flip="false" index="38" name="u" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="1825"/>
									<Property name="Top" type="int" value="392"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="42" flip="false" index="39" name="Logic operator 1" type="logical">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs number" type="int" value="3"/>
									<Property name="Type" type="enum" value="0">
										<Enumeration index="0" name="AND"/>
										<Enumeration index="1" name="NAND"/>
										<Enumeration index="2" name="NOR"/>
										<Enumeration index="3" name="NOT"/>
										<Enumeration index="4" name="OR"/>
										<Enumeration index="5" name="XOR"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1056"/>
									<Property name="Top" type="int" value="199"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="43" flip="false" index="40" name="Relational operator 1" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="3">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="274"/>
									<Property name="Top" type="int" value="778"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="44" flip="false" index="41" name="Constant 2" type="constant">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Value" type="real" value="1"/>
									<Property name="Min" type="real" value="0"/>
									<Property name="Max" type="real" value="1"/>
									<Property name="Limit output" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="68"/>
									<Property name="Top" type="int" value="793"/>
									<Property name="Width" type="int" value="40"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="46" flip="false" index="42" name="przypadekXmniejszyOd1dlaPlus" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1276"/>
									<Property name="Top" type="int" value="124"/>
									<Property name="Width" type="int" value="240"/>
									<Property name="Height" type="int" value="75"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="47" flip="false" index="43" name="Product 1" type="product">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Actions string" type="string" value="**"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1171"/>
									<Property name="Top" type="int" value="97"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="45" flip="false" index="44" name="Logic operator 2" type="logical">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs number" type="int" value="1"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="AND"/>
										<Enumeration index="1" name="NAND"/>
										<Enumeration index="2" name="NOR"/>
										<Enumeration index="3" name="NOT"/>
										<Enumeration index="4" name="OR"/>
										<Enumeration index="5" name="XOR"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1245"/>
									<Property name="Top" type="int" value="230"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="48" flip="false" index="45" name="Relational operator 2" type="relational">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="EQUAL"/>
										<Enumeration index="1" name="GREATER"/>
										<Enumeration index="2" name="GREATER or EQUAL"/>
										<Enumeration index="3" name="LESS"/>
										<Enumeration index="4" name="LESS or EQUAL"/>
										<Enumeration index="5" name="NOT EQUAL"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="275"/>
									<Property name="Top" type="int" value="854"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="49" flip="false" index="46" name="Constant 3" type="constant">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Value" type="real" value="-1"/>
									<Property name="Min" type="real" value="0"/>
									<Property name="Max" type="real" value="1"/>
									<Property name="Limit output" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="69"/>
									<Property name="Top" type="int" value="880"/>
									<Property name="Width" type="int" value="40"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="50" flip="false" index="47" name="Logic operator 3" type="logical">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs number" type="int" value="3"/>
									<Property name="Type" type="enum" value="0">
										<Enumeration index="0" name="AND"/>
										<Enumeration index="1" name="NAND"/>
										<Enumeration index="2" name="NOR"/>
										<Enumeration index="3" name="NOT"/>
										<Enumeration index="4" name="OR"/>
										<Enumeration index="5" name="XOR"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1071"/>
									<Property name="Top" type="int" value="566"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="51" flip="false" index="48" name="Logic operator 4" type="logical">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs number" type="int" value="1"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="AND"/>
										<Enumeration index="1" name="NAND"/>
										<Enumeration index="2" name="NOR"/>
										<Enumeration index="3" name="NOT"/>
										<Enumeration index="4" name="OR"/>
										<Enumeration index="5" name="XOR"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1173"/>
									<Property name="Top" type="int" value="529"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="52" flip="false" index="49" name="przypadekXmniejszyOd1dlaMinus" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1301"/>
									<Property name="Top" type="int" value="799"/>
									<Property name="Width" type="int" value="240"/>
									<Property name="Height" type="int" value="75"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="53" flip="false" index="50" name="Product 6" type="product">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Actions string" type="string" value="**"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1163"/>
									<Property name="Top" type="int" value="697"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="54" flip="false" index="51" name="Display 1" type="display">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Buffer length" type="uint" value="900"/>
									<Property name="Inputs count" type="uint" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="1753"/>
									<Property name="Top" type="int" value="690"/>
									<Property name="Width" type="int" value="170"/>
									<Property name="Height" type="int" value="60"/>
									<Property name="Chart min" type="real" value="-0.75"/>
									<Property name="Chart max" type="real" value="1.23529411764706"/>
									<Property name="Autoscale" type="bool" value="false"/>
									<Property name="Scale each sep." type="bool" value="false"/>
									<Property name="Precision" type="int" value="3"/>
									<Property name="Digits" type="int" value="3"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="Fixed"/>
										<Enumeration index="1" name="General"/>
										<Enumeration index="2" name="Scientific"/>
									</Property>
									<Property name="Bkg color" type="color" value="0"/>
									<Property name="Axes color" type="color" value="8421504"/>
									<Property name="Bad sample color" type="color" value="255"/>
									<Property name="Show sample nr" type="bool" value="false"/>
									<Property name="Color scheme" type="enum" value="0">
										<Enumeration index="0" name="dark"/>
										<Enumeration index="1" name="light"/>
									</Property>
								</Properties>
							</Display>
						</Object>
						<Object ID="55" flip="false" index="52" name="Display 2" type="display">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Buffer length" type="uint" value="900"/>
									<Property name="Inputs count" type="uint" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="1654"/>
									<Property name="Top" type="int" value="67"/>
									<Property name="Width" type="int" value="170"/>
									<Property name="Height" type="int" value="60"/>
									<Property name="Chart min" type="real" value="-0.75"/>
									<Property name="Chart max" type="real" value="1.23529411764706"/>
									<Property name="Autoscale" type="bool" value="false"/>
									<Property name="Scale each sep." type="bool" value="false"/>
									<Property name="Precision" type="int" value="3"/>
									<Property name="Digits" type="int" value="3"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="Fixed"/>
										<Enumeration index="1" name="General"/>
										<Enumeration index="2" name="Scientific"/>
									</Property>
									<Property name="Bkg color" type="color" value="0"/>
									<Property name="Axes color" type="color" value="8421504"/>
									<Property name="Bad sample color" type="color" value="255"/>
									<Property name="Show sample nr" type="bool" value="false"/>
									<Property name="Color scheme" type="enum" value="0">
										<Enumeration index="0" name="dark"/>
										<Enumeration index="1" name="light"/>
									</Property>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="49"/>
									<Segment index="1" value="-30"/>
									<Segment index="2" value="55"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="8" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="73"/>
									<Segment index="1" value="317"/>
									<Segment index="2" value="119"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="8" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="83"/>
									<Segment index="1" value="283"/>
									<Segment index="2" value="109"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="5" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="63"/>
									<Segment index="1" value="227"/>
									<Segment index="2" value="153"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="5" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="184"/>
									<Segment index="1" value="193"/>
									<Segment index="2" value="32"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="9" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="375"/>
									<Segment index="2" value="136"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="9" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="166"/>
									<Segment index="1" value="341"/>
									<Segment index="2" value="26"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="14" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="436"/>
									<Segment index="2" value="138"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="14" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="151"/>
									<Segment index="1" value="402"/>
									<Segment index="2" value="43"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="13" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="-88"/>
									<Segment index="2" value="233"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="13" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="151"/>
									<Segment index="1" value="-122"/>
									<Segment index="2" value="138"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="3" begin_idx="0" end_id="12" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="83"/>
									<Segment index="1" value="-6"/>
									<Segment index="2" value="28"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="16" begin_idx="0" end_id="12" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="46"/>
									<Segment index="1" value="-79"/>
									<Segment index="2" value="20"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="3" begin_idx="0" end_id="15" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="83"/>
									<Segment index="1" value="52"/>
									<Segment index="2" value="30"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="16" begin_idx="0" end_id="15" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="46"/>
									<Segment index="1" value="-21"/>
									<Segment index="2" value="22"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="18" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="176"/>
									<Segment index="1" value="16"/>
									<Segment index="2" value="15"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="18" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="151"/>
									<Segment index="1" value="-18"/>
									<Segment index="2" value="40"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="18" begin_idx="0" end_id="17" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="22"/>
									<Segment index="1" value="-2"/>
									<Segment index="2" value="6"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="20" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="93"/>
									<Segment index="1" value="51"/>
									<Segment index="2" value="284"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="20" begin_idx="0" end_id="19" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="84"/>
									<Segment index="1" value="6"/>
									<Segment index="2" value="25"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="17" begin_idx="0" end_id="19" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="148"/>
									<Segment index="1" value="-40"/>
									<Segment index="2" value="49"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="20" begin_idx="0" end_id="21" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="53"/>
									<Segment index="1" value="280"/>
									<Segment index="2" value="34"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="21" begin_idx="0" end_id="22" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="19"/>
									<Segment index="1" value="27"/>
									<Segment index="2" value="15"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="5" begin_idx="0" end_id="22" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="120"/>
									<Segment index="1" value="46"/>
									<Segment index="2" value="142"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="22" begin_idx="0" end_id="23" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="17"/>
									<Segment index="1" value="28"/>
									<Segment index="2" value="25"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="8" begin_idx="0" end_id="23" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="190"/>
									<Segment index="1" value="-9"/>
									<Segment index="2" value="193"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="13" begin_idx="0" end_id="26" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="90"/>
									<Segment index="1" value="580"/>
									<Segment index="2" value="194"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="14" begin_idx="0" end_id="25" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="183"/>
									<Segment index="1" value="-2"/>
									<Segment index="2" value="193"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="9" begin_idx="0" end_id="24" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="186"/>
									<Segment index="1" value="0"/>
									<Segment index="2" value="193"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="22" begin_idx="0" end_id="24" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="17"/>
									<Segment index="1" value="95"/>
									<Segment index="2" value="21"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="22" begin_idx="0" end_id="25" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="17"/>
									<Segment index="1" value="154"/>
									<Segment index="2" value="20"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="22" begin_idx="0" end_id="26" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="17"/>
									<Segment index="1" value="212"/>
									<Segment index="2" value="23"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="23" begin_idx="0" end_id="28" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="54"/>
									<Segment index="1" value="-189"/>
									<Segment index="2" value="196"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="24" begin_idx="0" end_id="29" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="124"/>
									<Segment index="1" value="-223"/>
									<Segment index="2" value="459"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="38" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="151"/>
									<Segment index="1" value="78"/>
									<Segment index="2" value="89"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="38" begin_idx="0" end_id="36" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="136"/>
									<Segment index="1" value="-3"/>
									<Segment index="2" value="99"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="20" begin_idx="0" end_id="36" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="40"/>
									<Segment index="1" value="147"/>
									<Segment index="2" value="73"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="17" begin_idx="0" end_id="39" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="25"/>
									<Segment index="1" value="166"/>
									<Segment index="2" value="18"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="39" begin_idx="0" end_id="37" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="47"/>
									<Segment index="1" value="0"/>
									<Segment index="2" value="52"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="12" begin_idx="0" end_id="11" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="323"/>
									<Segment index="1" value="262"/>
									<Segment index="2" value="62"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="15" begin_idx="0" end_id="40" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="295"/>
									<Segment index="1" value="291"/>
									<Segment index="2" value="74"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="36" begin_idx="0" end_id="11" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="21"/>
									<Segment index="1" value="43"/>
									<Segment index="2" value="109"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="37" begin_idx="0" end_id="40" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="89"/>
									<Segment index="1" value="65"/>
									<Segment index="2" value="31"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="11" begin_idx="0" end_id="30" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="49"/>
									<Segment index="1" value="168"/>
									<Segment index="2" value="491"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="40" begin_idx="0" end_id="31" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="30"/>
									<Segment index="2" value="187"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="25" begin_idx="0" end_id="30" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="101"/>
									<Segment index="1" value="-100"/>
									<Segment index="2" value="469"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="26" begin_idx="0" end_id="31" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="133"/>
									<Segment index="1" value="-207"/>
									<Segment index="2" value="123"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="20" begin_idx="0" end_id="37" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="27"/>
									<Segment index="1" value="212"/>
									<Segment index="2" value="82"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="10" begin_idx="0" end_id="28" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="29"/>
									<Segment index="1" value="200"/>
									<Segment index="2" value="138"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="19" begin_idx="0" end_id="10" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="65"/>
									<Segment index="1" value="-58"/>
									<Segment index="2" value="127"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="15" begin_idx="0" end_id="35" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="156"/>
									<Segment index="1" value="125"/>
									<Segment index="2" value="223"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="34" begin_idx="0" end_id="35" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="63"/>
									<Segment index="1" value="46"/>
									<Segment index="2" value="63"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="35" begin_idx="0" end_id="42" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="-12"/>
									<Segment index="1" value="-35"/>
									<Segment index="2" value="229"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="43" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="133"/>
									<Segment index="1" value="709"/>
									<Segment index="2" value="83"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="0" end_id="43" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="87"/>
									<Segment index="1" value="0"/>
									<Segment index="2" value="84"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="42" begin_idx="0" end_id="47" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="32"/>
									<Segment index="1" value="-98"/>
									<Segment index="2" value="28"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="47" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="565"/>
									<Segment index="1" value="26"/>
									<Segment index="2" value="548"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="47" begin_idx="0" end_id="46" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="30"/>
									<Segment index="1" value="25"/>
									<Segment index="2" value="20"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="46" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="607"/>
									<Segment index="1" value="-15"/>
									<Segment index="2" value="596"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="46" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="608"/>
									<Segment index="1" value="-44"/>
									<Segment index="2" value="595"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="32" begin_idx="0" end_id="27" end_idx="5" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="283"/>
									<Segment index="1" value="-134"/>
									<Segment index="2" value="46"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="31" begin_idx="0" end_id="27" end_idx="4" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="434"/>
									<Segment index="1" value="-14"/>
									<Segment index="2" value="55"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="29" begin_idx="0" end_id="27" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="35"/>
									<Segment index="1" value="76"/>
									<Segment index="2" value="129"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="28" begin_idx="0" end_id="27" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="100"/>
									<Segment index="1" value="100"/>
									<Segment index="2" value="393"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="46" begin_idx="0" end_id="27" end_idx="0" type="1">
							<Display type="1">
								<Segments size="5">
									<Segment index="0" value="33"/>
									<Segment index="1" value="116"/>
									<Segment index="2" value="28"/>
									<Segment index="3" value="96"/>
									<Segment index="4" value="44"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="27" begin_idx="0" end_id="33" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="17"/>
									<Segment index="1" value="-13"/>
									<Segment index="2" value="30"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="33" begin_idx="0" end_id="41" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="36"/>
									<Segment index="1" value="-9"/>
									<Segment index="2" value="16"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="42" begin_idx="0" end_id="45" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="74"/>
									<Segment index="1" value="31"/>
									<Segment index="2" value="60"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="12" begin_idx="0" end_id="10" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="221"/>
									<Segment index="1" value="20"/>
									<Segment index="2" value="222"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="19" begin_idx="0" end_id="42" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="222"/>
									<Segment index="1" value="73"/>
									<Segment index="2" value="180"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="43" begin_idx="0" end_id="42" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="688"/>
									<Segment index="1" value="-568"/>
									<Segment index="2" value="39"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="49" begin_idx="0" end_id="48" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="107"/>
									<Segment index="1" value="-11"/>
									<Segment index="2" value="64"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="48" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="156"/>
									<Segment index="1" value="785"/>
									<Segment index="2" value="61"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="37" begin_idx="0" end_id="50" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="333"/>
									<Segment index="1" value="234"/>
									<Segment index="2" value="84"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="48" begin_idx="0" end_id="50" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="723"/>
									<Segment index="1" value="-277"/>
									<Segment index="2" value="18"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="50" begin_idx="0" end_id="51" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="21"/>
									<Segment index="1" value="-37"/>
									<Segment index="2" value="26"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="52" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="616"/>
									<Segment index="1" value="660"/>
									<Segment index="2" value="612"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="52" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="618"/>
									<Segment index="1" value="631"/>
									<Segment index="2" value="610"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="11" begin_idx="0" end_id="50" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="198"/>
									<Segment index="1" value="253"/>
									<Segment index="2" value="30"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="50" begin_idx="0" end_id="53" end_idx="0" type="1">
							<Display type="1">
								<Segments size="5">
									<Segment index="0" value="27"/>
									<Segment index="1" value="79"/>
									<Segment index="2" value="1"/>
									<Segment index="3" value="43"/>
									<Segment index="4" value="9"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="53" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="52"/>
									<Segment index="1" value="639"/>
									<Segment index="2" value="1053"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="53" begin_idx="0" end_id="52" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="16"/>
									<Segment index="1" value="100"/>
									<Segment index="2" value="67"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="30" begin_idx="0" end_id="27" end_idx="3" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="78"/>
									<Segment index="1" value="-90"/>
									<Segment index="2" value="100"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="51" begin_idx="0" end_id="30" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="70"/>
									<Segment index="1" value="-23"/>
									<Segment index="2" value="85"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="35" begin_idx="0" end_id="29" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="55"/>
									<Segment index="1" value="65"/>
									<Segment index="2" value="503"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="45" begin_idx="0" end_id="29" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="48"/>
									<Segment index="1" value="94"/>
									<Segment index="2" value="49"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="20" begin_idx="0" end_id="34" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="48"/>
									<Segment index="1" value="65"/>
									<Segment index="2" value="65"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="34" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="267"/>
									<Segment index="1" value="-7"/>
									<Segment index="2" value="263"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="52" begin_idx="0" end_id="27" end_idx="6" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="53"/>
									<Segment index="1" value="-367"/>
									<Segment index="2" value="27"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="53" begin_idx="0" end_id="54" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="68"/>
									<Segment index="1" value="-7"/>
									<Segment index="2" value="467"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="47" begin_idx="0" end_id="55" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="189"/>
									<Segment index="1" value="-30"/>
									<Segment index="2" value="239"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="46" begin_idx="0" end_id="55" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="71"/>
									<Segment index="1" value="-54"/>
									<Segment index="2" value="72"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="52" begin_idx="0" end_id="54" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="109"/>
									<Segment index="1" value="-106"/>
									<Segment index="2" value="108"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="46" begin_idx="0" end_id="50" end_idx="0" type="1">
							<Display type="1">
								<Segments size="5">
									<Segment index="0" value="10"/>
									<Segment index="1" value="208"/>
									<Segment index="2" value="-460"/>
									<Segment index="3" value="208"/>
									<Segment index="4" value="10"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
			</Paths>
			<Objects>
				<Object ID="1" flip="false" index="0" name="Constant 1" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0.1"/>
							<Property name="Min" type="real" value="0"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="169"/>
							<Property name="Top" type="int" value="157"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="2" flip="false" index="1" name="Constant 2" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0.05"/>
							<Property name="Min" type="real" value="0"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="171"/>
							<Property name="Top" type="int" value="205"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="3" flip="false" index="2" name="Time event 1" type="src_time_event">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Start time" type="real" value="1"/>
							<Property name="Stop time" type="real" value="6"/>
							<Property name="Amplitude" type="real" value="3"/>
							<Property name="Bias" type="real" value="-1.5"/>
							<Property name="Kind" type="enum" value="1">
								<Enumeration index="0" name="impulse"/>
								<Enumeration index="1" name="ramp"/>
								<Enumeration index="2" name="step"/>
							</Property>
							<Property name="Reset time" type="real" value="12"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="129"/>
							<Property name="Top" type="int" value="45"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="4" flip="false" index="3" name="Time event 2" type="src_time_event">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Start time" type="real" value="6"/>
							<Property name="Stop time" type="real" value="11"/>
							<Property name="Amplitude" type="real" value="-3"/>
							<Property name="Bias" type="real" value="0"/>
							<Property name="Kind" type="enum" value="1">
								<Enumeration index="0" name="impulse"/>
								<Enumeration index="1" name="ramp"/>
								<Enumeration index="2" name="step"/>
							</Property>
							<Property name="Reset time" type="real" value="12"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="127"/>
							<Property name="Top" type="int" value="96"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="5" flip="false" index="4" name="Sum 1" type="sum">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Signs" type="string" value="++"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="299"/>
							<Property name="Top" type="int" value="40"/>
							<Property name="Width" type="int" value="60"/>
							<Property name="Height" type="int" value="60"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="6" flip="false" index="5" name="Display 1" type="display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="120"/>
							<Property name="Inputs count" type="uint" value="2"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="730"/>
							<Property name="Top" type="int" value="77"/>
							<Property name="Width" type="int" value="170"/>
							<Property name="Height" type="int" value="60"/>
							<Property name="Chart min" type="real" value="-1.5"/>
							<Property name="Chart max" type="real" value="1.5"/>
							<Property name="Autoscale" type="bool" value="false"/>
							<Property name="Scale each sep." type="bool" value="false"/>
							<Property name="Precision" type="int" value="3"/>
							<Property name="Digits" type="int" value="3"/>
							<Property name="Type" type="enum" value="1">
								<Enumeration index="0" name="Fixed"/>
								<Enumeration index="1" name="General"/>
								<Enumeration index="2" name="Scientific"/>
							</Property>
							<Property name="Bkg color" type="color" value="0"/>
							<Property name="Axes color" type="color" value="8421504"/>
							<Property name="Bad sample color" type="color" value="255"/>
							<Property name="Show sample nr" type="bool" value="false"/>
							<Property name="Color scheme" type="enum" value="0">
								<Enumeration index="0" name="dark"/>
								<Enumeration index="1" name="light"/>
							</Property>
						</Properties>
					</Display>
				</Object>
				<Object ID="8" flip="false" index="6" name="XY Display 1" type="xy_display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="2000"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="786"/>
							<Property name="Top" type="int" value="260"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="66"/>
							<Property name="X axis min" type="real" value="-0.779999999999999"/>
							<Property name="X axis max" type="real" value="0.779999999999974"/>
							<Property name="Y axis min" type="real" value="-1"/>
							<Property name="Y axis max" type="real" value="1"/>
							<Property name="Chart bkg color" type="color" value="16777215"/>
							<Property name="Chart axes color" type="color" value="12632256"/>
							<Property name="Chart plot color" type="color" value="0"/>
							<Property name="Chart bad sample color" type="color" value="255"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="7" flip="false" index="7" name="Subpath 1" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="400"/>
							<Property name="Top" type="int" value="134"/>
							<Property name="Width" type="int" value="200"/>
							<Property name="Height" type="int" value="75"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="9" flip="false" index="8" name="Gain 1" type="gain">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Gain" type="real" value="0.5"/>
							<Property name="Clear sig. status" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="377"/>
							<Property name="Top" type="int" value="56"/>
							<Property name="Width" type="int" value="60"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
			</Objects>
			<Connections>
				<Connection begin_id="3" begin_idx="0" end_id="5" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="65"/>
							<Segment index="1" value="0"/>
							<Segment index="2" value="10"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="4" begin_idx="0" end_id="5" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="43"/>
							<Segment index="1" value="-31"/>
							<Segment index="2" value="34"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="69"/>
							<Segment index="1" value="-2"/>
							<Segment index="2" value="67"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="2" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="68"/>
							<Segment index="1" value="-32"/>
							<Segment index="2" value="66"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="7" begin_idx="0" end_id="6" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="68"/>
							<Segment index="1" value="-74"/>
							<Segment index="2" value="67"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="7" begin_idx="0" end_id="8" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="96"/>
							<Segment index="1" value="133"/>
							<Segment index="2" value="95"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="5" begin_idx="0" end_id="9" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="11"/>
							<Segment index="1" value="1"/>
							<Segment index="2" value="12"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="7" end_idx="0" type="1">
					<Display type="1">
						<Segments size="5">
							<Segment index="0" value="10"/>
							<Segment index="1" value="40"/>
							<Segment index="2" value="-50"/>
							<Segment index="3" value="41"/>
							<Segment index="4" value="8"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="6" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="149"/>
							<Segment index="1" value="46"/>
							<Segment index="2" value="149"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="8" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="176"/>
							<Segment index="1" value="211"/>
							<Segment index="2" value="178"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
			</Connections>
		</PATH>
	</Paths>
</CalcPaths_Export_File>
