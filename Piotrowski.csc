<CalcPaths_Export_File>
	<Simulator after_time="0" max_steps="0" sample_time="0.1" simulation_rate="1" simulation_ratio="0.011" start_time="1984-01-01T00:00:00.000" stop_condition="0" stop_time="1984-01-01T00:00:00.000"/>
	<Paths>
		<PATH ID="2" block_id="-1" index="-1" name="Path 1">
			<Paths>
				<PATH ID="5" block_id="2" index="-1" name="EL. NL.">
					<Paths>
						<PATH ID="9" block_id="5" index="-1" name="Subpath 1">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="51"/>
											<Property name="Top" type="int" value="73"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="48"/>
											<Property name="Top" type="int" value="165"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="51"/>
											<Property name="Top" type="int" value="212"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="dCV" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i1[0]-i1[1]"/>
											<Property name="Inputs" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="175"/>
											<Property name="Top" type="int" value="68"/>
											<Property name="Width" type="int" value="80"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="4" name="a" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="265"/>
											<Property name="Top" type="int" value="419"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="5" name="b1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1-1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="264"/>
											<Property name="Top" type="int" value="479"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="6" name="b2" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="-i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="263"/>
											<Property name="Top" type="int" value="537"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="7" name="" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="747"/>
											<Property name="Top" type="int" value="140"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="8" name="" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="751"/>
											<Property name="Top" type="int" value="291"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="14" flip="false" index="9" name="b3" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="263"/>
											<Property name="Top" type="int" value="596"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="10" name="b4" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="-1+1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="266"/>
											<Property name="Top" type="int" value="654"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="15" flip="false" index="11" name="Pochodna ujemna" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="349"/>
											<Property name="Top" type="int" value="113"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="Pochodna  dodatnia" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="347"/>
											<Property name="Top" type="int" value="47"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="16" flip="false" index="13" name="" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="252"/>
											<Property name="Top" type="int" value="94"/>
											<Property name="Width" type="int" value="40"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="17" flip="false" index="14" name="alfa+beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="357"/>
											<Property name="Top" type="int" value="201"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="18" flip="false" index="15" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="270"/>
											<Property name="Top" type="int" value="196"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="19" flip="false" index="16" name="" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="596"/>
											<Property name="Top" type="int" value="148"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="20" flip="false" index="17" name="CV" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="434"/>
											<Property name="Top" type="int" value="152"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="21" flip="false" index="18" name="CV" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="577"/>
											<Property name="Top" type="int" value="400"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="22" flip="false" index="19" name="Product 1" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="672"/>
											<Property name="Top" type="int" value="413"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="23" flip="false" index="20" name="Sum 2" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="758"/>
											<Property name="Top" type="int" value="473"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="24" flip="false" index="21" name="Sum 3" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="759"/>
											<Property name="Top" type="int" value="531"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="25" flip="false" index="22" name="Sum 4" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="758"/>
											<Property name="Top" type="int" value="590"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="26" flip="false" index="23" name="Sum 5" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="761"/>
											<Property name="Top" type="int" value="648"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="27" flip="false" index="24" name="Sum 6" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="+++++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1101"/>
											<Property name="Top" type="int" value="365"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="105"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="28" flip="false" index="25" name="Product 2" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="962"/>
											<Property name="Top" type="int" value="273"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="29" flip="false" index="26" name="Product 3" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="961"/>
											<Property name="Top" type="int" value="331"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="30" flip="false" index="27" name="Product 4" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="964"/>
											<Property name="Top" type="int" value="396"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="31" flip="false" index="28" name="Product 5" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="963"/>
											<Property name="Top" type="int" value="457"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="32" flip="false" index="29" name="Constant 1" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="965"/>
											<Property name="Top" type="int" value="517"/>
											<Property name="Width" type="int" value="40"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="33" flip="false" index="30" name="Signal limiter 1" type="signal_limiter">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Low limit" type="real" value="-1"/>
											<Property name="High limit" type="real" value="1"/>
											<Property name="Rising rate limit" type="real" value="0"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1187"/>
											<Property name="Top" type="int" value="402"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="34" flip="false" index="31" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="598"/>
											<Property name="Top" type="int" value="211"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="35" flip="false" index="32" name="Logic operator 1" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="749"/>
											<Property name="Top" type="int" value="218"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="36" flip="false" index="33" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="599"/>
											<Property name="Top" type="int" value="271"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="37" flip="false" index="34" name="Relational operator 3" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="596"/>
											<Property name="Top" type="int" value="332"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="38" flip="false" index="35" name="-beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="-1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="313"/>
											<Property name="Top" type="int" value="285"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="39" flip="false" index="36" name="-(alfa+beta)" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="-1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="444"/>
											<Property name="Top" type="int" value="347"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="40" flip="false" index="37" name="" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="753"/>
											<Property name="Top" type="int" value="349"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="41" flip="false" index="38" name="u" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1299"/>
											<Property name="Top" type="int" value="407"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="31"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="68"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="56"/>
											<Segment index="1" value="317"/>
											<Segment index="2" value="135"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="8" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="148"/>
											<Segment index="1" value="283"/>
											<Segment index="2" value="40"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="56"/>
											<Segment index="1" value="257"/>
											<Segment index="2" value="136"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="148"/>
											<Segment index="1" value="223"/>
											<Segment index="2" value="41"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="56"/>
											<Segment index="1" value="375"/>
											<Segment index="2" value="134"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="9" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="148"/>
											<Segment index="1" value="341"/>
											<Segment index="2" value="39"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="14" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="56"/>
											<Segment index="1" value="434"/>
											<Segment index="2" value="134"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="14" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="148"/>
											<Segment index="1" value="400"/>
											<Segment index="2" value="39"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="56"/>
											<Segment index="1" value="492"/>
											<Segment index="2" value="137"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="13" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="148"/>
											<Segment index="1" value="458"/>
											<Segment index="2" value="42"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="70"/>
											<Segment index="1" value="-21"/>
											<Segment index="2" value="27"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="16" begin_idx="0" end_id="12" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="41"/>
											<Segment index="1" value="-32"/>
											<Segment index="2" value="19"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="15" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="70"/>
											<Segment index="1" value="45"/>
											<Segment index="2" value="29"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="16" begin_idx="0" end_id="15" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="41"/>
											<Segment index="1" value="34"/>
											<Segment index="2" value="21"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="18" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="176"/>
											<Segment index="1" value="34"/>
											<Segment index="2" value="21"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="18" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="148"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="17" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="16"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="16"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="20" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="56"/>
											<Segment index="1" value="84"/>
											<Segment index="2" value="302"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="19" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="85"/>
											<Segment index="1" value="-4"/>
											<Segment index="2" value="22"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="19" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="138"/>
											<Segment index="1" value="-38"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="19" begin_idx="0" end_id="10" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="20"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="76"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="323"/>
											<Segment index="1" value="86"/>
											<Segment index="2" value="22"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="21" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="56"/>
											<Segment index="1" value="248"/>
											<Segment index="2" value="32"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="21" begin_idx="0" end_id="22" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="19"/>
											<Segment index="1" value="11"/>
											<Segment index="2" value="21"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="22" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="144"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="148"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="22" begin_idx="0" end_id="23" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="11"/>
											<Segment index="1" value="53"/>
											<Segment index="2" value="20"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="23" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="191"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="188"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="13" begin_idx="0" end_id="26" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="186"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="194"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="14" begin_idx="0" end_id="25" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="187"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="193"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="24" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="188"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="193"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="22" begin_idx="0" end_id="24" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="11"/>
											<Segment index="1" value="111"/>
											<Segment index="2" value="21"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="22" begin_idx="0" end_id="25" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="11"/>
											<Segment index="1" value="170"/>
											<Segment index="2" value="20"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="22" begin_idx="0" end_id="26" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="11"/>
											<Segment index="1" value="228"/>
											<Segment index="2" value="23"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="32" begin_idx="0" end_id="27" end_idx="4" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="62"/>
											<Segment index="1" value="-82"/>
											<Segment index="2" value="39"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="27" begin_idx="0" end_id="33" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="10"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="21"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="28" begin_idx="0" end_id="27" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="73"/>
											<Segment index="1" value="89"/>
											<Segment index="2" value="11"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="29" begin_idx="0" end_id="27" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="68"/>
											<Segment index="1" value="48"/>
											<Segment index="2" value="17"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="30" begin_idx="0" end_id="27" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="15"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="67"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="31" begin_idx="0" end_id="27" end_idx="3" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="15"/>
											<Segment index="1" value="-44"/>
											<Segment index="2" value="68"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="28" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="119"/>
											<Segment index="1" value="124"/>
											<Segment index="2" value="41"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="23" begin_idx="0" end_id="28" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="59"/>
											<Segment index="1" value="-194"/>
											<Segment index="2" value="90"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="34" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="41"/>
											<Segment index="1" value="59"/>
											<Segment index="2" value="68"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="34" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="148"/>
											<Segment index="1" value="19"/>
											<Segment index="2" value="374"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="15" begin_idx="0" end_id="35" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="300"/>
											<Segment index="1" value="98"/>
											<Segment index="2" value="45"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="34" begin_idx="0" end_id="35" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="16"/>
											<Segment index="1" value="15"/>
											<Segment index="2" value="80"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="35" begin_idx="0" end_id="29" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="105"/>
											<Segment index="1" value="104"/>
											<Segment index="2" value="52"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="24" begin_idx="0" end_id="29" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="124"/>
											<Segment index="1" value="-194"/>
											<Segment index="2" value="23"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="38" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="148"/>
											<Segment index="1" value="78"/>
											<Segment index="2" value="89"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="38" begin_idx="0" end_id="36" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="136"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="95"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="36" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="41"/>
											<Segment index="1" value="119"/>
											<Segment index="2" value="69"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="39" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="15"/>
											<Segment index="1" value="146"/>
											<Segment index="2" value="17"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="39" begin_idx="0" end_id="37" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="48"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="323"/>
											<Segment index="1" value="237"/>
											<Segment index="2" value="26"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="15" begin_idx="0" end_id="40" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="300"/>
											<Segment index="1" value="229"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="36" begin_idx="0" end_id="11" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="25"/>
											<Segment index="1" value="28"/>
											<Segment index="2" value="72"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="37" begin_idx="0" end_id="40" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="21"/>
											<Segment index="1" value="25"/>
											<Segment index="2" value="81"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="11" begin_idx="0" end_id="30" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="86"/>
											<Segment index="1" value="96"/>
											<Segment index="2" value="72"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="40" begin_idx="0" end_id="31" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="77"/>
											<Segment index="1" value="99"/>
											<Segment index="2" value="78"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="25" begin_idx="0" end_id="30" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="101"/>
											<Segment index="1" value="-188"/>
											<Segment index="2" value="50"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="26" begin_idx="0" end_id="31" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="133"/>
											<Segment index="1" value="-185"/>
											<Segment index="2" value="14"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="33" begin_idx="0" end_id="41" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="30"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="27"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="37" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="28"/>
											<Segment index="1" value="180"/>
											<Segment index="2" value="79"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
					</Paths>
					<Objects>
						<Object ID="0" flip="false" index="0" name="PID in " type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="70"/>
									<Property name="Top" type="int" value="360"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="transOut" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="1315"/>
									<Property name="Top" type="int" value="401"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="5" flip="false" index="2" name="Subpath 1" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="497"/>
									<Property name="Top" type="int" value="338"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="75"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="6" flip="false" index="3" name="Constant 1" type="constant">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Value" type="real" value="0.1"/>
									<Property name="Min" type="real" value="0"/>
									<Property name="Max" type="real" value="1"/>
									<Property name="Limit output" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="308"/>
									<Property name="Top" type="int" value="414"/>
									<Property name="Width" type="int" value="100"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="7" flip="false" index="4" name="Constant 2" type="constant">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Value" type="real" value="0.05"/>
									<Property name="Min" type="real" value="0"/>
									<Property name="Max" type="real" value="1"/>
									<Property name="Limit output" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="307"/>
									<Property name="Top" type="int" value="465"/>
									<Property name="Width" type="int" value="100"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="9" flip="false" index="5" name="Gain 1" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="0.1"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="267"/>
									<Property name="Top" type="int" value="353"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="8" flip="false" index="6" name="Gain 2" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="10"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="856"/>
									<Property name="Top" type="int" value="379"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="6" begin_idx="0" end_id="5" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="40"/>
									<Segment index="1" value="-55"/>
									<Segment index="2" value="54"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="7" begin_idx="0" end_id="5" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="52"/>
									<Segment index="1" value="-88"/>
									<Segment index="2" value="43"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="9" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="81"/>
									<Segment index="1" value="-2"/>
									<Segment index="2" value="91"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="9" begin_idx="0" end_id="5" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="75"/>
									<Segment index="1" value="-12"/>
									<Segment index="2" value="100"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="5" begin_idx="0" end_id="8" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="75"/>
									<Segment index="1" value="19"/>
									<Segment index="2" value="89"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="8" begin_idx="0" end_id="1" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="202"/>
									<Segment index="1" value="17"/>
									<Segment index="2" value="202"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
				<PATH ID="6" block_id="3" index="-1" name="Model Procesu  Gp">
					<Paths/>
					<Objects>
						<Object ID="0" flip="false" index="0" name="" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="116"/>
									<Property name="Top" type="int" value="240"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="1141"/>
									<Property name="Top" type="int" value="232"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="2" flip="false" index="2" name="2nd order inertia 1" type="second_order_inertia">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="K1" type="real" value="2"/>
									<Property name="K2" type="real" value="10"/>
									<Property name="T1" type="real" value="2"/>
									<Property name="T2" type="real" value="10"/>
									<Property name="Y0[k-1]" type="real" value="0"/>
									<Property name="Y0[k-2]" type="real" value="0"/>
									<Property name="U0[k-1]" type="real" value="0"/>
									<Property name="U0[k-2]" type="real" value="0"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="491"/>
									<Property name="Top" type="int" value="220"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="5" flip="false" index="3" name="Math expression 1" type="math_expression">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Equation" type="string" value="i1[40]"/>
									<Property name="Inputs" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="276"/>
									<Property name="Top" type="int" value="215"/>
									<Property name="Width" type="int" value="130"/>
									<Property name="Height" type="int" value="52"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="4" flip="false" index="4" name="Transfer function (cont.) 1" type="cld_transfer_function">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Nominator order" type="uint" value="1"/>
									<Property name="Denominator order" type="uint" value="2"/>
									<Property name="Nominator" size="1" type="double_vect">
										<Row index="0" value="1"/>
									</Property>
									<Property name="Denominator" size="2" type="double_vect">
										<Row index="0" value="1"/>
										<Row index="1" value="-25"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="730"/>
									<Property name="Top" type="int" value="221"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="0" begin_idx="0" end_id="5" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="48"/>
									<Segment index="1" value="-9"/>
									<Segment index="2" value="87"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="5" begin_idx="0" end_id="2" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="38"/>
									<Segment index="1" value="1"/>
									<Segment index="2" value="52"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="4" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="63"/>
									<Segment index="1" value="1"/>
									<Segment index="2" value="61"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="4" begin_idx="0" end_id="1" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="149"/>
									<Segment index="1" value="-1"/>
									<Segment index="2" value="147"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
				<PATH ID="8" block_id="4" index="-1" name="madel zaklocenia  Gz">
					<Paths/>
					<Objects>
						<Object ID="0" flip="false" index="0" name="" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="34"/>
									<Property name="Top" type="int" value="190"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="637"/>
									<Property name="Top" type="int" value="175"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="2" flip="false" index="2" name="1st order inertia 1" type="first_order_inertia">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="K" type="real" value="0.1"/>
									<Property name="T" type="real" value="6"/>
									<Property name="Y0[k-1]" type="real" value="0"/>
									<Property name="U0[k-1]" type="real" value="0"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="237"/>
									<Property name="Top" type="int" value="147"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="3" flip="false" index="3" name="1st order integrator 1" type="first_order_integrator">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="K" type="real" value="1"/>
									<Property name="T" type="real" value="1"/>
									<Property name="Y0[k-1]" type="real" value="0"/>
									<Property name="U0[k-1]" type="real" value="0"/>
									<Property name="Low limit" type="real" value="0"/>
									<Property name="High limit" type="real" value="0"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="229"/>
									<Property name="Top" type="int" value="224"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="4" flip="false" index="4" name="Product 1" type="product">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Actions string" type="string" value="**"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="472"/>
									<Property name="Top" type="int" value="173"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="60"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="3" begin_idx="0" end_id="4" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="104"/>
									<Segment index="1" value="-33"/>
									<Segment index="2" value="84"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="4" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="38"/>
									<Segment index="1" value="24"/>
									<Segment index="2" value="82"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="2" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="87"/>
									<Segment index="1" value="-31"/>
									<Segment index="2" value="91"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="87"/>
									<Segment index="1" value="46"/>
									<Segment index="2" value="83"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="4" begin_idx="0" end_id="1" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="52"/>
									<Segment index="1" value="-18"/>
									<Segment index="2" value="58"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
				<PATH ID="10" block_id="6" index="-1" name="K">
					<Paths/>
					<Objects>
						<Object ID="0" flip="false" index="0" name="" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="540"/>
									<Property name="Top" type="int" value="292"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="782"/>
									<Property name="Top" type="int" value="288"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="2" flip="false" index="2" name="Gain 1" type="gain">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Gain" type="real" value="-1"/>
									<Property name="Clear sig. status" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="646"/>
									<Property name="Top" type="int" value="286"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="0" begin_idx="0" end_id="2" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="41"/>
									<Segment index="1" value="-1"/>
									<Segment index="2" value="40"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="1" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="41"/>
									<Segment index="1" value="-3"/>
									<Segment index="2" value="40"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
			</Paths>
			<Objects>
				<Object ID="2" flip="false" index="0" name="EL. NL." type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="390"/>
							<Property name="Top" type="int" value="501"/>
							<Property name="Width" type="int" value="160"/>
							<Property name="Height" type="int" value="45"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="3" flip="false" index="1" name="Model Procesu  Gp" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="766"/>
							<Property name="Top" type="int" value="498"/>
							<Property name="Width" type="int" value="120"/>
							<Property name="Height" type="int" value="45"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="4" flip="false" index="2" name="madel zaklocenia  Gz" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="687"/>
							<Property name="Top" type="int" value="347"/>
							<Property name="Width" type="int" value="120"/>
							<Property name="Height" type="int" value="45"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="5" flip="false" index="3" name="Sum 2" type="sum">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Signs" type="string" value="++"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="971"/>
							<Property name="Top" type="int" value="372"/>
							<Property name="Width" type="int" value="60"/>
							<Property name="Height" type="int" value="60"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="6" flip="false" index="4" name="K" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="640"/>
							<Property name="Top" type="int" value="501"/>
							<Property name="Width" type="int" value="80"/>
							<Property name="Height" type="int" value="45"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="7" flip="false" index="5" name="Constant 1" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0"/>
							<Property name="Min" type="real" value="0"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="405"/>
							<Property name="Top" type="int" value="200"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="9" flip="false" index="6" name="PID 1" type="pid_controller">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="k" type="real" value="1.1"/>
							<Property name="Ti" type="real" value="250"/>
							<Property name="Td" type="real" value="90"/>
							<Property name="Differentiate inertia" type="real" value="100"/>
							<Property name="Output low limit" type="real" value="-100"/>
							<Property name="Output high limit" type="real" value="100"/>
							<Property name="Initial output" type="real" value="0"/>
							<Property name="Bias" type="real" value="0"/>
							<Property name="Setpoint derivative" type="bool" value="true"/>
							<Property name="Setpoint proportional" type="bool" value="true"/>
							<Property name="Stop integrate on limits" type="bool" value="true"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="216"/>
							<Property name="Top" type="int" value="392"/>
							<Property name="Width" type="int" value="80"/>
							<Property name="Height" type="int" value="45"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="11" flip="false" index="7" name="Time event 1" type="src_time_event">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Start time" type="real" value="1"/>
							<Property name="Stop time" type="real" value="1000"/>
							<Property name="Amplitude" type="real" value="1"/>
							<Property name="Bias" type="real" value="0"/>
							<Property name="Kind" type="enum" value="2">
								<Enumeration index="0" name="impulse"/>
								<Enumeration index="1" name="ramp"/>
								<Enumeration index="2" name="step"/>
							</Property>
							<Property name="Reset time" type="real" value="1000000"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="49"/>
							<Property name="Top" type="int" value="354"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="10" flip="false" index="8" name="XY Display 1" type="xy_display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="200"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="1201"/>
							<Property name="Top" type="int" value="487"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="66"/>
							<Property name="X axis min" type="real" value="-0.02"/>
							<Property name="X axis max" type="real" value="1.02"/>
							<Property name="Y axis min" type="real" value="-2.23386107483037"/>
							<Property name="Y axis max" type="real" value="2"/>
							<Property name="Chart bkg color" type="color" value="16777215"/>
							<Property name="Chart axes color" type="color" value="12632256"/>
							<Property name="Chart plot color" type="color" value="0"/>
							<Property name="Chart bad sample color" type="color" value="255"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="13" flip="false" index="9" name="Display 2" type="display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="3000"/>
							<Property name="Inputs count" type="uint" value="2"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="871"/>
							<Property name="Top" type="int" value="243"/>
							<Property name="Width" type="int" value="170"/>
							<Property name="Height" type="int" value="60"/>
							<Property name="Chart min" type="real" value="-5.37725678671601"/>
							<Property name="Chart max" type="real" value="4.75993002658249"/>
							<Property name="Autoscale" type="bool" value="true"/>
							<Property name="Scale each sep." type="bool" value="false"/>
							<Property name="Precision" type="int" value="3"/>
							<Property name="Digits" type="int" value="3"/>
							<Property name="Type" type="enum" value="1">
								<Enumeration index="0" name="Fixed"/>
								<Enumeration index="1" name="General"/>
								<Enumeration index="2" name="Scientific"/>
							</Property>
							<Property name="Bkg color" type="color" value="0"/>
							<Property name="Axes color" type="color" value="8421504"/>
							<Property name="Bad sample color" type="color" value="255"/>
							<Property name="Show sample nr" type="bool" value="false"/>
							<Property name="Color scheme" type="enum" value="0">
								<Enumeration index="0" name="dark"/>
								<Enumeration index="1" name="light"/>
							</Property>
						</Properties>
					</Display>
				</Object>
				<Object ID="12" flip="false" index="10" name="Display 1" type="display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="36000"/>
							<Property name="Inputs count" type="uint" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="382"/>
							<Property name="Top" type="int" value="307"/>
							<Property name="Width" type="int" value="170"/>
							<Property name="Height" type="int" value="45"/>
							<Property name="Chart min" type="real" value="-5.37725678671601"/>
							<Property name="Chart max" type="real" value="4.75993002658249"/>
							<Property name="Autoscale" type="bool" value="true"/>
							<Property name="Scale each sep." type="bool" value="false"/>
							<Property name="Precision" type="int" value="3"/>
							<Property name="Digits" type="int" value="3"/>
							<Property name="Type" type="enum" value="1">
								<Enumeration index="0" name="Fixed"/>
								<Enumeration index="1" name="General"/>
								<Enumeration index="2" name="Scientific"/>
							</Property>
							<Property name="Bkg color" type="color" value="0"/>
							<Property name="Axes color" type="color" value="8421504"/>
							<Property name="Bad sample color" type="color" value="255"/>
							<Property name="Show sample nr" type="bool" value="false"/>
							<Property name="Color scheme" type="enum" value="0">
								<Enumeration index="0" name="dark"/>
								<Enumeration index="1" name="light"/>
							</Property>
						</Properties>
					</Display>
				</Object>
			</Objects>
			<Connections>
				<Connection begin_id="7" begin_idx="0" end_id="4" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="166"/>
							<Segment index="1" value="154"/>
							<Segment index="2" value="21"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="2" begin_idx="0" end_id="6" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="61"/>
							<Segment index="1" value="0"/>
							<Segment index="2" value="34"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="5" begin_idx="0" end_id="10" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="98"/>
							<Segment index="1" value="129"/>
							<Segment index="2" value="77"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="11" begin_idx="0" end_id="9" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="59"/>
							<Segment index="1" value="38"/>
							<Segment index="2" value="13"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="13" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="323"/>
							<Segment index="1" value="-151"/>
							<Segment index="2" value="257"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="2" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="36"/>
							<Segment index="1" value="109"/>
							<Segment index="2" value="63"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="6" begin_idx="0" end_id="3" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="26"/>
							<Segment index="1" value="-3"/>
							<Segment index="2" value="25"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="3" begin_idx="0" end_id="13" end_idx="1" type="1">
					<Display type="1">
						<Segments size="5">
							<Segment index="0" value="33"/>
							<Segment index="1" value="-115"/>
							<Segment index="2" value="-53"/>
							<Segment index="3" value="-122"/>
							<Segment index="4" value="10"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="12" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="45"/>
							<Segment index="1" value="-85"/>
							<Segment index="2" value="46"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="4" begin_idx="0" end_id="5" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="89"/>
							<Segment index="1" value="23"/>
							<Segment index="2" value="80"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="3" begin_idx="0" end_id="9" end_idx="1" type="1">
					<Display type="1">
						<Segments size="5">
							<Segment index="0" value="33"/>
							<Segment index="1" value="82"/>
							<Segment index="2" value="-708"/>
							<Segment index="3" value="-180"/>
							<Segment index="4" value="10"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
			</Connections>
		</PATH>
	</Paths>
</CalcPaths_Export_File>
