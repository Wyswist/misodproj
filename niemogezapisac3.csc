<CalcPaths_Export_File>
	<Simulator after_time="0" max_steps="10000" sample_time="0.01" simulation_rate="10" simulation_ratio="1" start_time="1984-01-01T00:00:00.000" stop_condition="3" stop_time="1984-01-01T00:00:00.000"/>
	<Paths>
		<PATH ID="2" block_id="-1" index="-1" name="Path 1">
			<Paths>
				<PATH ID="9" block_id="7" index="-1" name="Subpath 1">
					<Paths>
						<PATH ID="5" block_id="56" index="-1" name="Krzywa">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="156"/>
											<Property name="Top" type="int" value="642"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="146"/>
											<Property name="Top" type="int" value="824"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="145"/>
											<Property name="Top" type="int" value="1018"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="A" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="403"/>
											<Property name="Top" type="int" value="711"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="B1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1-1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="396"/>
											<Property name="Top" type="int" value="817"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="B2" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="-i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="399"/>
											<Property name="Top" type="int" value="969"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="B3" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i1[0]+i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="402"/>
											<Property name="Top" type="int" value="1088"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="B4" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="400"/>
											<Property name="Top" type="int" value="1214"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="Product 1" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="678"/>
											<Property name="Top" type="int" value="669"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="834"/>
											<Property name="Top" type="int" value="734"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="Sum 2" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="837"/>
											<Property name="Top" type="int" value="853"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="Sum 3" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="836"/>
											<Property name="Top" type="int" value="970"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="Sum 4" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="840"/>
											<Property name="Top" type="int" value="1108"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="13" name="AxplusB1" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="945"/>
											<Property name="Top" type="int" value="743"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="14" flip="false" index="14" name="AxplusB2" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="956"/>
											<Property name="Top" type="int" value="864"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="15" flip="false" index="15" name="AxplusB3" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="960"/>
											<Property name="Top" type="int" value="984"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="16" flip="false" index="16" name="AxplusB4" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="4"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="963"/>
											<Property name="Top" type="int" value="1116"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="3" begin_idx="0" end_id="8" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="80"/>
											<Segment index="1" value="-49"/>
											<Segment index="2" value="80"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="50"/>
											<Segment index="1" value="58"/>
											<Segment index="2" value="51"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="52"/>
											<Segment index="1" value="177"/>
											<Segment index="2" value="52"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="53"/>
											<Segment index="1" value="294"/>
											<Segment index="2" value="50"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="54"/>
											<Segment index="1" value="432"/>
											<Segment index="2" value="53"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="9" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="164"/>
											<Segment index="1" value="-90"/>
											<Segment index="2" value="159"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="10" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="161"/>
											<Segment index="1" value="-123"/>
											<Segment index="2" value="162"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="11" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="200"/>
											<Segment index="1" value="-125"/>
											<Segment index="2" value="119"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="12" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="235"/>
											<Segment index="1" value="-113"/>
											<Segment index="2" value="90"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="90"/>
											<Segment index="1" value="-101"/>
											<Segment index="2" value="142"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="248"/>
											<Segment index="1" value="30"/>
											<Segment index="2" value="249"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="116"/>
											<Segment index="1" value="5"/>
											<Segment index="2" value="109"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="114"/>
											<Segment index="1" value="157"/>
											<Segment index="2" value="114"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="6" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="115"/>
											<Segment index="1" value="276"/>
											<Segment index="2" value="116"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="114"/>
											<Segment index="1" value="402"/>
											<Segment index="2" value="115"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="4" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="113"/>
											<Segment index="1" value="-167"/>
											<Segment index="2" value="113"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="114"/>
											<Segment index="1" value="-15"/>
											<Segment index="2" value="115"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="6" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="116"/>
											<Segment index="1" value="104"/>
											<Segment index="2" value="116"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="115"/>
											<Segment index="1" value="230"/>
											<Segment index="2" value="115"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="28"/>
											<Segment index="1" value="-1"/>
											<Segment index="2" value="28"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="14" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="32"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="32"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="11" begin_idx="0" end_id="15" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="34"/>
											<Segment index="1" value="4"/>
											<Segment index="2" value="35"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="16" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="35"/>
											<Segment index="1" value="-2"/>
											<Segment index="2" value="33"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="3" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="116"/>
											<Segment index="1" value="-273"/>
											<Segment index="2" value="117"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="7" block_id="43" index="-1" name="Pochodna">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="155"/>
											<Property name="Top" type="int" value="153"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="dodatnia" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="679"/>
											<Property name="Top" type="int" value="119"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="ujemna" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="681"/>
											<Property name="Top" type="int" value="202"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="Math expression 1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i1[0]-i1[1]"/>
											<Property name="Inputs" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="348"/>
											<Property name="Top" type="int" value="148"/>
											<Property name="Width" type="int" value="80"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="507"/>
											<Property name="Top" type="int" value="101"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="509"/>
											<Property name="Top" type="int" value="190"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="Constant 1" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="322"/>
											<Property name="Top" type="int" value="262"/>
											<Property name="Width" type="int" value="40"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="149"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="19"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="4" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="103"/>
											<Segment index="1" value="-146"/>
											<Segment index="2" value="47"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="104"/>
											<Segment index="1" value="-57"/>
											<Segment index="2" value="48"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="21"/>
											<Segment index="1" value="-47"/>
											<Segment index="2" value="63"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="48"/>
											<Segment index="1" value="42"/>
											<Segment index="2" value="38"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="1" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="58"/>
											<Segment index="1" value="6"/>
											<Segment index="2" value="59"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="2" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="58"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="59"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="8" block_id="44" index="-1" name="Przedzialy">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="242"/>
											<Property name="Top" type="int" value="203"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="75"/>
											<Property name="Top" type="int" value="574"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="99"/>
											<Property name="Top" type="int" value="340"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="538"/>
											<Property name="Top" type="int" value="211"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="538"/>
											<Property name="Top" type="int" value="310"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Relational operator 3" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="520"/>
											<Property name="Top" type="int" value="456"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="Relational operator 4" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="516"/>
											<Property name="Top" type="int" value="578"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="alfa+beta" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="160"/>
											<Property name="Top" type="int" value="500"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="-(alfa+beta)" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="-1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="248"/>
											<Property name="Top" type="int" value="594"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="Gain 1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="-1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="206"/>
											<Property name="Top" type="int" value="406"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="CVbeta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="791"/>
											<Property name="Top" type="int" value="243"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="CVwalfaplbeta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="829"/>
											<Property name="Top" type="int" value="326"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="CVmminusbeta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="831"/>
											<Property name="Top" type="int" value="457"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="13" name="CVmminusmalfambeta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="4"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="829"/>
											<Property name="Top" type="int" value="591"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="7" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="19"/>
											<Segment index="1" value="89"/>
											<Segment index="2" value="14"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="124"/>
											<Segment index="1" value="13"/>
											<Segment index="2" value="147"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="123"/>
											<Segment index="1" value="112"/>
											<Segment index="2" value="148"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="148"/>
											<Segment index="1" value="258"/>
											<Segment index="2" value="105"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="6" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="134"/>
											<Segment index="1" value="380"/>
											<Segment index="2" value="115"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="38"/>
											<Segment index="1" value="-58"/>
											<Segment index="2" value="22"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="18"/>
											<Segment index="1" value="163"/>
											<Segment index="2" value="18"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="6" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="105"/>
											<Segment index="1" value="-1"/>
											<Segment index="2" value="108"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="3" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="208"/>
											<Segment index="1" value="-109"/>
											<Segment index="2" value="206"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="4" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="157"/>
											<Segment index="1" value="-180"/>
											<Segment index="2" value="166"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="41"/>
											<Segment index="1" value="71"/>
											<Segment index="2" value="41"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="129"/>
											<Segment index="1" value="65"/>
											<Segment index="2" value="130"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="99"/>
											<Segment index="1" value="20"/>
											<Segment index="2" value="99"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="113"/>
											<Segment index="1" value="4"/>
											<Segment index="2" value="123"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="129"/>
											<Segment index="1" value="-11"/>
											<Segment index="2" value="127"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="129"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="129"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="10" block_id="34" index="-1" name="LogikaDlaCV1">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="In" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="27"/>
											<Property name="Top" type="int" value="226"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="Demux 1" type="Demux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Outputs" type="uint" value="10"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="124"/>
											<Property name="Top" type="int" value="62"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="500"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="dodatnia" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="231"/>
											<Property name="Top" type="int" value="56"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="ujemna" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="233"/>
											<Property name="Top" type="int" value="125"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="CV>beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="239"/>
											<Property name="Top" type="int" value="179"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Cv>alfa+beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="225"/>
											<Property name="Top" type="int" value="244"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="CV<-beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="225"/>
											<Property name="Top" type="int" value="287"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="CV<-(alfa+beta)" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="235"/>
											<Property name="Top" type="int" value="334"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="Ax+B1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="237"/>
											<Property name="Top" type="int" value="380"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="Ax+B2" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="234"/>
											<Property name="Top" type="int" value="427"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="Ax+B3" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="236"/>
											<Property name="Top" type="int" value="483"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="Ax+B4" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="239"/>
											<Property name="Top" type="int" value="554"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="Lfun1" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="491"/>
											<Property name="Top" type="int" value="58"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="13" name="Lfun2" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="494"/>
											<Property name="Top" type="int" value="129"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="14" flip="false" index="14" name="Lfun3" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="491"/>
											<Property name="Top" type="int" value="205"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="15" flip="false" index="15" name="Lfun4" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="489"/>
											<Property name="Top" type="int" value="273"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="16" flip="false" index="16" name="Product 1" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="726"/>
											<Property name="Top" type="int" value="246"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="17" flip="false" index="17" name="Product 2" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="727"/>
											<Property name="Top" type="int" value="344"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="18" flip="false" index="18" name="Product 3" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="731"/>
											<Property name="Top" type="int" value="448"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="19" flip="false" index="19" name="Product 4" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="733"/>
											<Property name="Top" type="int" value="548"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="20" flip="false" index="20" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="+++++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="900"/>
											<Property name="Top" type="int" value="253"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="350"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="21" flip="false" index="21" name="Constant 1" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="745"/>
											<Property name="Top" type="int" value="635"/>
											<Property name="Width" type="int" value="40"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="22" flip="false" index="22" name="Signal limiter 1" type="signal_limiter">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Low limit" type="real" value="-1"/>
											<Property name="High limit" type="real" value="1"/>
											<Property name="Rising rate limit" type="real" value="0"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1143"/>
											<Property name="Top" type="int" value="418"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="23" flip="false" index="23" name="wykres" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1262"/>
											<Property name="Top" type="int" value="430"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="0" begin_idx="0" end_id="1" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="53"/>
											<Segment index="1" value="76"/>
											<Segment index="2" value="19"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="2" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="70"/>
											<Segment index="1" value="-44"/>
											<Segment index="2" value="22"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="1" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="68"/>
											<Segment index="1" value="-20"/>
											<Segment index="2" value="26"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="2" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="68"/>
											<Segment index="1" value="-11"/>
											<Segment index="2" value="32"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="3" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="70"/>
											<Segment index="1" value="9"/>
											<Segment index="2" value="16"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="5" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="65"/>
											<Segment index="1" value="9"/>
											<Segment index="2" value="31"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="6" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="49"/>
											<Segment index="1" value="10"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="7" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="47"/>
											<Segment index="1" value="12"/>
											<Segment index="2" value="48"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="8" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="48"/>
											<Segment index="1" value="23"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="9" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="46"/>
											<Segment index="1" value="49"/>
											<Segment index="2" value="54"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="102"/>
											<Segment index="1" value="10"/>
											<Segment index="2" value="103"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="12" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="77"/>
											<Segment index="1" value="-163"/>
											<Segment index="2" value="134"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="115"/>
											<Segment index="1" value="12"/>
											<Segment index="2" value="91"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="4" end_id="6" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="43"/>
											<Segment index="1" value="7"/>
											<Segment index="2" value="43"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="13" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="98"/>
											<Segment index="1" value="-27"/>
											<Segment index="2" value="102"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="14" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="149"/>
											<Segment index="1" value="88"/>
											<Segment index="2" value="54"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="14" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="102"/>
											<Segment index="1" value="-106"/>
											<Segment index="2" value="99"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="15" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="126"/>
											<Segment index="1" value="225"/>
											<Segment index="2" value="77"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="15" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="105"/>
											<Segment index="1" value="9"/>
											<Segment index="2" value="104"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="16" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="94"/>
											<Segment index="1" value="179"/>
											<Segment index="2" value="86"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="16" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="348"/>
											<Segment index="1" value="-115"/>
											<Segment index="2" value="86"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="13" begin_idx="0" end_id="17" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="27"/>
											<Segment index="1" value="206"/>
											<Segment index="2" value="151"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="17" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="285"/>
											<Segment index="1" value="-64"/>
											<Segment index="2" value="153"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="14" begin_idx="0" end_id="18" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="65"/>
											<Segment index="1" value="234"/>
											<Segment index="2" value="120"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="18" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="219"/>
											<Segment index="1" value="-16"/>
											<Segment index="2" value="221"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="15" begin_idx="0" end_id="19" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="94"/>
											<Segment index="1" value="266"/>
											<Segment index="2" value="95"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="11" begin_idx="0" end_id="19" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="219"/>
											<Segment index="1" value="13"/>
											<Segment index="2" value="220"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="16" begin_idx="0" end_id="20" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="47"/>
											<Segment index="1" value="45"/>
											<Segment index="2" value="72"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="20" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="47"/>
											<Segment index="1" value="5"/>
											<Segment index="2" value="71"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="20" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="44"/>
											<Segment index="1" value="-41"/>
											<Segment index="2" value="70"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="19" begin_idx="0" end_id="20" end_idx="3" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="52"/>
											<Segment index="1" value="-83"/>
											<Segment index="2" value="60"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="21" begin_idx="0" end_id="20" end_idx="4" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="74"/>
											<Segment index="1" value="-107"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="22" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="94"/>
											<Segment index="1" value="5"/>
											<Segment index="2" value="94"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="22" begin_idx="0" end_id="23" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="22"/>
											<Segment index="1" value="7"/>
											<Segment index="2" value="42"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="12" block_id="13" index="-1" name="Logika">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="In" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="21"/>
											<Property name="Top" type="int" value="302"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="Demux 1" type="Demux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Outputs" type="uint" value="10"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="124"/>
											<Property name="Top" type="int" value="62"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="500"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="dodatnia" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="245"/>
											<Property name="Top" type="int" value="95"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="ujemna" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="255"/>
											<Property name="Top" type="int" value="133"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="CV>beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="249"/>
											<Property name="Top" type="int" value="182"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Cv>alfa+beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="242"/>
											<Property name="Top" type="int" value="232"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="130"/>
											<Property name="Top" type="int" value="615"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="CV<-beta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="241"/>
											<Property name="Top" type="int" value="280"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="CV<-(alfa+beta)" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="243"/>
											<Property name="Top" type="int" value="326"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="Ax+B1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="242"/>
											<Property name="Top" type="int" value="384"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="Ax+B2" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="246"/>
											<Property name="Top" type="int" value="429"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="Ax+B4" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="232"/>
											<Property name="Top" type="int" value="576"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="Ax+B3" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="244"/>
											<Property name="Top" type="int" value="495"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="13" name="Logic operator 1" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="688"/>
											<Property name="Top" type="int" value="497"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="20" flip="false" index="14" name="Product 1" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1074"/>
											<Property name="Top" type="int" value="653"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="23" buffer_type="1" flip="false" index="15" name="Internal variable 1" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="720"/>
											<Property name="Top" type="int" value="628"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="24" buffer_type="1" flip="false" index="16" name="Internal variable 2" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 1"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1008"/>
											<Property name="Top" type="int" value="454"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="25" flip="false" index="17" name="XY Display 1" type="xy_display">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer length" type="uint" value="1000"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1388"/>
											<Property name="Top" type="int" value="355"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="66"/>
											<Property name="X axis min" type="real" value="-0.779999999999999"/>
											<Property name="X axis max" type="real" value="0.779999999999974"/>
											<Property name="Y axis min" type="real" value="-0.0141176470588229"/>
											<Property name="Y axis max" type="real" value="0.71999999999997"/>
											<Property name="Chart bkg color" type="color" value="16777215"/>
											<Property name="Chart axes color" type="color" value="12632256"/>
											<Property name="Chart plot color" type="color" value="0"/>
											<Property name="Chart bad sample color" type="color" value="255"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="27" flip="false" index="18" name="out" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1718"/>
											<Property name="Top" type="int" value="765"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="33" flip="false" index="19" name="MinMax Selector 1" type="min_max_select">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs count" type="uint" value="2"/>
											<Property name="Function" type="enum" value="0">
												<Enumeration index="0" name="Max"/>
												<Enumeration index="1" name="Min"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="523"/>
											<Property name="Top" type="int" value="664"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="52"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="34" flip="true" index="20" name="One step delay 1" type="one_step_del">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Init value" type="real" value="0"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="527"/>
											<Property name="Top" type="int" value="757"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="26" flip="false" index="21" name="dMAX" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i1[0]-i1[1]"/>
											<Property name="Inputs" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="709"/>
											<Property name="Top" type="int" value="734"/>
											<Property name="Width" type="int" value="80"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="28" flip="false" index="22" name="Relational operator 3" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="845"/>
											<Property name="Top" type="int" value="734"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="29" flip="false" index="23" name="Constant 2" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="670"/>
											<Property name="Top" type="int" value="801"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="30" flip="false" index="24" name="Constant 1" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="1"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="409"/>
											<Property name="Top" type="int" value="631"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="31" flip="false" index="25" name="Logic operator 2" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="960"/>
											<Property name="Top" type="int" value="599"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="35" buffer_type="1" flip="false" index="26" name="Internal variable 3" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 1 1"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1049"/>
											<Property name="Top" type="int" value="757"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="32" flip="false" index="27" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="3">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="512"/>
											<Property name="Top" type="int" value="571"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="37" buffer_type="1" flip="false" index="28" name="Internal variable 4" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 1 2"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="629"/>
											<Property name="Top" type="int" value="341"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="38" buffer_type="1" flip="false" index="29" name="Internal variable 5" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 1 2 1"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="628"/>
											<Property name="Top" type="int" value="415"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="40" buffer_type="1" flip="false" index="30" name="Internal variable 7" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 1 2 1 2"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="423"/>
											<Property name="Top" type="int" value="353"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="36" flip="false" index="31" name="Logic operator 3" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1329"/>
											<Property name="Top" type="int" value="821"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="42" flip="false" index="32" name="Product 2" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1423"/>
											<Property name="Top" type="int" value="862"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="43" flip="false" index="33" name="Display 1" type="display">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer length" type="uint" value="100"/>
											<Property name="Inputs count" type="uint" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1576"/>
											<Property name="Top" type="int" value="561"/>
											<Property name="Width" type="int" value="170"/>
											<Property name="Height" type="int" value="60"/>
											<Property name="Chart min" type="real" value="0"/>
											<Property name="Chart max" type="real" value="0.731764705882384"/>
											<Property name="Autoscale" type="bool" value="true"/>
											<Property name="Scale each sep." type="bool" value="false"/>
											<Property name="Precision" type="int" value="3"/>
											<Property name="Digits" type="int" value="3"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="Fixed"/>
												<Enumeration index="1" name="General"/>
												<Enumeration index="2" name="Scientific"/>
											</Property>
											<Property name="Bkg color" type="color" value="0"/>
											<Property name="Axes color" type="color" value="8421504"/>
											<Property name="Bad sample color" type="color" value="255"/>
											<Property name="Show sample nr" type="bool" value="false"/>
											<Property name="Color scheme" type="enum" value="0">
												<Enumeration index="0" name="dark"/>
												<Enumeration index="1" name="light"/>
											</Property>
										</Properties>
									</Display>
								</Object>
								<Object ID="39" flip="false" index="34" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="2">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="866"/>
											<Property name="Top" type="int" value="553"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="41" flip="false" index="35" name="Logic operator 4" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1066"/>
											<Property name="Top" type="int" value="917"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="44" flip="false" index="36" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1520"/>
											<Property name="Top" type="int" value="768"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="46" buffer_type="0" flip="false" index="37" name="Internal variable 6" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 2"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="234"/>
											<Property name="Top" type="int" value="645"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="45" buffer_type="1" flip="false" index="38" name="Internal variable 8" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 1 2 1 2 1"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="395"/>
											<Property name="Top" type="int" value="533"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="47" buffer_type="1" flip="false" index="39" name="Internal variable 9" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 1 2 1 2 1 1"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="396"/>
											<Property name="Top" type="int" value="463"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="48" buffer_type="1" flip="false" index="40" name="Internal variable 10" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 1 2 1 2 1 1 1"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="424"/>
											<Property name="Top" type="int" value="286"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="49" buffer_type="1" flip="false" index="41" name="Internal variable 11" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 1 2 1 2 1 1 1 1"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="403"/>
											<Property name="Top" type="int" value="219"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="50" buffer_type="1" flip="false" index="42" name="Internal variable 12" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 1 2 1 2 1 1 1 1 1"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="407"/>
											<Property name="Top" type="int" value="68"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="1" begin_idx="0" end_id="2" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="46"/>
											<Segment index="1" value="-5"/>
											<Segment index="2" value="60"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="1" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="47"/>
											<Segment index="1" value="-12"/>
											<Segment index="2" value="69"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="2" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="50"/>
											<Segment index="1" value="-8"/>
											<Segment index="2" value="60"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="3" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="51"/>
											<Segment index="1" value="-3"/>
											<Segment index="2" value="52"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="4" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="51"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="51"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="5" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="54"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="50"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="6" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="54"/>
											<Segment index="1" value="14"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="7" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="53"/>
											<Segment index="1" value="14"/>
											<Segment index="2" value="54"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="8" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="73"/>
											<Segment index="1" value="35"/>
											<Segment index="2" value="32"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="9" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="56"/>
											<Segment index="1" value="71"/>
											<Segment index="2" value="37"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="1" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="42"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="36"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="273"/>
											<Segment index="1" value="368"/>
											<Segment index="2" value="105"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="13" begin_idx="0" end_id="24" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="199"/>
											<Segment index="1" value="-47"/>
											<Segment index="2" value="66"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="33" begin_idx="0" end_id="34" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="14"/>
											<Segment index="1" value="98"/>
											<Segment index="2" value="-10"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="34" begin_idx="0" end_id="33" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="-14"/>
											<Segment index="1" value="-81"/>
											<Segment index="2" value="10"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="33" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="29"/>
											<Segment index="1" value="290"/>
											<Segment index="2" value="197"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="33" begin_idx="0" end_id="20" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="293"/>
											<Segment index="1" value="-2"/>
											<Segment index="2" value="203"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="33" begin_idx="0" end_id="26" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="89"/>
											<Segment index="1" value="68"/>
											<Segment index="2" value="42"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="26" begin_idx="0" end_id="28" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="47"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="14"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="29" begin_idx="0" end_id="28" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="133"/>
											<Segment index="1" value="-52"/>
											<Segment index="2" value="17"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="33" begin_idx="0" end_id="23" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="33"/>
											<Segment index="1" value="-35"/>
											<Segment index="2" value="109"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="28" begin_idx="0" end_id="31" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="14"/>
											<Segment index="1" value="-135"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="13" begin_idx="0" end_id="31" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="78"/>
											<Segment index="1" value="91"/>
											<Segment index="2" value="139"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="31" begin_idx="0" end_id="20" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="20"/>
											<Segment index="1" value="45"/>
											<Segment index="2" value="39"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="28" begin_idx="0" end_id="35" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="45"/>
											<Segment index="1" value="19"/>
											<Segment index="2" value="104"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="30" begin_idx="0" end_id="32" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="57"/>
											<Segment index="1" value="-45"/>
											<Segment index="2" value="21"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="32" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="197"/>
											<Segment index="1" value="-39"/>
											<Segment index="2" value="160"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="32" begin_idx="0" end_id="13" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="84"/>
											<Segment index="1" value="-63"/>
											<Segment index="2" value="37"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="32" begin_idx="0" end_id="38" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="43"/>
											<Segment index="1" value="-160"/>
											<Segment index="2" value="18"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="40" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="207"/>
											<Segment index="1" value="-254"/>
											<Segment index="2" value="61"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="13" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="230"/>
											<Segment index="1" value="330"/>
											<Segment index="2" value="154"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="37" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="255"/>
											<Segment index="1" value="170"/>
											<Segment index="2" value="70"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="25" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="129"/>
											<Segment index="1" value="-274"/>
											<Segment index="2" value="130"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="25" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="616"/>
											<Segment index="1" value="-248"/>
											<Segment index="2" value="617"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="36" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="70"/>
											<Segment index="1" value="692"/>
											<Segment index="2" value="949"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="36" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="47"/>
											<Segment index="1" value="654"/>
											<Segment index="2" value="978"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="36" begin_idx="0" end_id="42" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="17"/>
											<Segment index="1" value="32"/>
											<Segment index="2" value="22"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="42" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="930"/>
											<Segment index="1" value="452"/>
											<Segment index="2" value="192"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="43" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="368"/>
											<Segment index="1" value="-92"/>
											<Segment index="2" value="79"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="33" begin_idx="0" end_id="39" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="262"/>
											<Segment index="1" value="-98"/>
											<Segment index="2" value="26"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="39" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="369"/>
											<Segment index="1" value="132"/>
											<Segment index="2" value="196"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="39" begin_idx="0" end_id="31" end_idx="2" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="10"/>
											<Segment index="1" value="31"/>
											<Segment index="2" value="0"/>
											<Segment index="3" value="26"/>
											<Segment index="4" value="29"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="31" begin_idx="0" end_id="41" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="25"/>
											<Segment index="1" value="318"/>
											<Segment index="2" value="26"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="41" begin_idx="0" end_id="36" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="73"/>
											<Segment index="1" value="-85"/>
											<Segment index="2" value="135"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="42" begin_idx="0" end_id="44" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="27"/>
											<Segment index="1" value="-74"/>
											<Segment index="2" value="15"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="42" begin_idx="0" end_id="43" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="5"/>
											<Segment index="1" value="-281"/>
											<Segment index="2" value="93"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="44" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="194"/>
											<Segment index="1" value="115"/>
											<Segment index="2" value="197"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="44" begin_idx="0" end_id="27" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="71"/>
											<Segment index="1" value="-23"/>
											<Segment index="2" value="72"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="11" begin_idx="0" end_id="45" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="54"/>
											<Segment index="1" value="-32"/>
											<Segment index="2" value="54"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="47" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="48"/>
											<Segment index="1" value="-21"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="48" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="59"/>
											<Segment index="1" value="-29"/>
											<Segment index="2" value="67"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="49" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="53"/>
											<Segment index="1" value="-50"/>
											<Segment index="2" value="54"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="50" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="53"/>
											<Segment index="1" value="-16"/>
											<Segment index="2" value="54"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
					</Paths>
					<Objects>
						<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="29"/>
									<Property name="Top" type="int" value="30"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="22"/>
									<Property name="Top" type="int" value="149"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="3"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="32"/>
									<Property name="Top" type="int" value="215"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="53" flip="false" index="3" name="Product 6" type="product">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Actions string" type="string" value="**"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="1163"/>
									<Property name="Top" type="int" value="697"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="40"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="54" flip="false" index="4" name="Display 1" type="display">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Buffer length" type="uint" value="900"/>
									<Property name="Inputs count" type="uint" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="1753"/>
									<Property name="Top" type="int" value="690"/>
									<Property name="Width" type="int" value="170"/>
									<Property name="Height" type="int" value="60"/>
									<Property name="Chart min" type="real" value="-0.75"/>
									<Property name="Chart max" type="real" value="1.23529411764706"/>
									<Property name="Autoscale" type="bool" value="false"/>
									<Property name="Scale each sep." type="bool" value="false"/>
									<Property name="Precision" type="int" value="3"/>
									<Property name="Digits" type="int" value="3"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="Fixed"/>
										<Enumeration index="1" name="General"/>
										<Enumeration index="2" name="Scientific"/>
									</Property>
									<Property name="Bkg color" type="color" value="0"/>
									<Property name="Axes color" type="color" value="8421504"/>
									<Property name="Bad sample color" type="color" value="255"/>
									<Property name="Show sample nr" type="bool" value="false"/>
									<Property name="Color scheme" type="enum" value="0">
										<Enumeration index="0" name="dark"/>
										<Enumeration index="1" name="light"/>
									</Property>
								</Properties>
							</Display>
						</Object>
						<Object ID="56" flip="false" index="5" name="Krzywa" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="164"/>
									<Property name="Top" type="int" value="287"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="90"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="43" flip="false" index="6" name="Pochodna" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="176"/>
									<Property name="Top" type="int" value="64"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="60"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="44" flip="false" index="7" name="Przedzialy" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="171"/>
									<Property name="Top" type="int" value="161"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="90"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="34" flip="false" index="8" name="LogikaDlaCV1" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="567"/>
									<Property name="Top" type="int" value="77"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="36" flip="false" index="9" name="Mux 1" type="Mux">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs" type="uint" value="10"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="466"/>
									<Property name="Top" type="int" value="63"/>
									<Property name="Width" type="int" value="20"/>
									<Property name="Height" type="int" value="350"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="13" flip="false" index="10" name="Logika" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="567"/>
									<Property name="Top" type="int" value="297"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="60"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="14" flip="false" index="11" name="Out" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="855"/>
									<Property name="Top" type="int" value="89"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="15" flip="false" index="12" name="out2" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="871"/>
									<Property name="Top" type="int" value="316"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="53" begin_idx="0" end_id="54" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="68"/>
									<Segment index="1" value="-7"/>
									<Segment index="2" value="467"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="43" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="79"/>
									<Segment index="1" value="54"/>
									<Segment index="2" value="43"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="44" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="91"/>
									<Segment index="1" value="143"/>
									<Segment index="2" value="26"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="56" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="49"/>
									<Segment index="1" value="269"/>
									<Segment index="2" value="61"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="44" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="75"/>
									<Segment index="1" value="46"/>
									<Segment index="2" value="49"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="56" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="41"/>
									<Segment index="1" value="172"/>
									<Segment index="2" value="76"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="44" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="65"/>
									<Segment index="1" value="2"/>
									<Segment index="2" value="49"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="56" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="63"/>
									<Segment index="1" value="128"/>
									<Segment index="2" value="44"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="43" begin_idx="0" end_id="36" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="29"/>
									<Segment index="1" value="10"/>
									<Segment index="2" value="66"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="43" begin_idx="1" end_id="36" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="29"/>
									<Segment index="1" value="21"/>
									<Segment index="2" value="66"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="36" begin_idx="0" end_id="34" end_idx="0" type="20">
							<Display type="20">
								<Segments size="3">
									<Segment index="0" value="13"/>
									<Segment index="1" value="-139"/>
									<Segment index="2" value="73"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="0" end_id="36" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="42"/>
									<Segment index="1" value="-23"/>
									<Segment index="2" value="58"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="1" end_id="36" end_idx="3" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="42"/>
									<Segment index="1" value="-10"/>
									<Segment index="2" value="58"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="2" end_id="36" end_idx="4" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="42"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="58"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="3" end_id="36" end_idx="5" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="42"/>
									<Segment index="1" value="16"/>
									<Segment index="2" value="58"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="0" end_id="36" end_idx="6" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="-25"/>
									<Segment index="2" value="51"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="1" end_id="36" end_idx="7" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="-12"/>
									<Segment index="2" value="51"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="2" end_id="36" end_idx="8" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="1"/>
									<Segment index="2" value="51"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="3" end_id="36" end_idx="9" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="14"/>
									<Segment index="2" value="51"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="13" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="488"/>
									<Segment index="1" value="297"/>
									<Segment index="2" value="25"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="36" begin_idx="0" end_id="13" end_idx="0" type="20">
							<Display type="20">
								<Segments size="3">
									<Segment index="0" value="46"/>
									<Segment index="1" value="79"/>
									<Segment index="2" value="40"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="34" begin_idx="0" end_id="14" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="70"/>
									<Segment index="1" value="0"/>
									<Segment index="2" value="23"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="13" begin_idx="0" end_id="15" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="74"/>
									<Segment index="1" value="-1"/>
									<Segment index="2" value="35"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
			</Paths>
			<Objects>
				<Object ID="1" flip="false" index="0" name="Constant 1" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0.1"/>
							<Property name="Min" type="real" value="0"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="169"/>
							<Property name="Top" type="int" value="157"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="2" flip="false" index="1" name="Constant 2" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0.05"/>
							<Property name="Min" type="real" value="0"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="139"/>
							<Property name="Top" type="int" value="254"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="3" flip="false" index="2" name="Time event 1" type="src_time_event">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Start time" type="real" value="1"/>
							<Property name="Stop time" type="real" value="6"/>
							<Property name="Amplitude" type="real" value="3"/>
							<Property name="Bias" type="real" value="-1.5"/>
							<Property name="Kind" type="enum" value="1">
								<Enumeration index="0" name="impulse"/>
								<Enumeration index="1" name="ramp"/>
								<Enumeration index="2" name="step"/>
							</Property>
							<Property name="Reset time" type="real" value="12"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="129"/>
							<Property name="Top" type="int" value="45"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="4" flip="false" index="3" name="Time event 2" type="src_time_event">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Start time" type="real" value="6"/>
							<Property name="Stop time" type="real" value="11"/>
							<Property name="Amplitude" type="real" value="-3"/>
							<Property name="Bias" type="real" value="0"/>
							<Property name="Kind" type="enum" value="1">
								<Enumeration index="0" name="impulse"/>
								<Enumeration index="1" name="ramp"/>
								<Enumeration index="2" name="step"/>
							</Property>
							<Property name="Reset time" type="real" value="12"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="127"/>
							<Property name="Top" type="int" value="96"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="5" flip="false" index="4" name="Sum 1" type="sum">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Signs" type="string" value="++"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="299"/>
							<Property name="Top" type="int" value="40"/>
							<Property name="Width" type="int" value="60"/>
							<Property name="Height" type="int" value="60"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="6" flip="false" index="5" name="Display 1" type="display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="120"/>
							<Property name="Inputs count" type="uint" value="2"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="730"/>
							<Property name="Top" type="int" value="77"/>
							<Property name="Width" type="int" value="170"/>
							<Property name="Height" type="int" value="60"/>
							<Property name="Chart min" type="real" value="-1.5"/>
							<Property name="Chart max" type="real" value="1.5"/>
							<Property name="Autoscale" type="bool" value="false"/>
							<Property name="Scale each sep." type="bool" value="false"/>
							<Property name="Precision" type="int" value="3"/>
							<Property name="Digits" type="int" value="3"/>
							<Property name="Type" type="enum" value="1">
								<Enumeration index="0" name="Fixed"/>
								<Enumeration index="1" name="General"/>
								<Enumeration index="2" name="Scientific"/>
							</Property>
							<Property name="Bkg color" type="color" value="0"/>
							<Property name="Axes color" type="color" value="8421504"/>
							<Property name="Bad sample color" type="color" value="255"/>
							<Property name="Show sample nr" type="bool" value="false"/>
							<Property name="Color scheme" type="enum" value="0">
								<Enumeration index="0" name="dark"/>
								<Enumeration index="1" name="light"/>
							</Property>
						</Properties>
					</Display>
				</Object>
				<Object ID="8" flip="false" index="6" name="XY Display 1" type="xy_display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="2000"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="774"/>
							<Property name="Top" type="int" value="260"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="66"/>
							<Property name="X axis min" type="real" value="-0.779999999999999"/>
							<Property name="X axis max" type="real" value="0.779999999999974"/>
							<Property name="Y axis min" type="real" value="-0.8564"/>
							<Property name="Y axis max" type="real" value="0.852870588235324"/>
							<Property name="Chart bkg color" type="color" value="16777215"/>
							<Property name="Chart axes color" type="color" value="12632256"/>
							<Property name="Chart plot color" type="color" value="0"/>
							<Property name="Chart bad sample color" type="color" value="255"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="7" flip="false" index="7" name="Subpath 1" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="400"/>
							<Property name="Top" type="int" value="133"/>
							<Property name="Width" type="int" value="200"/>
							<Property name="Height" type="int" value="75"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="9" flip="false" index="8" name="Gain 1" type="gain">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Gain" type="real" value="0.5"/>
							<Property name="Clear sig. status" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12632256"/>
							<Property name="Left" type="int" value="377"/>
							<Property name="Top" type="int" value="56"/>
							<Property name="Width" type="int" value="60"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="10" flip="false" index="9" name="XY Display 2" type="xy_display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="2000"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="775"/>
							<Property name="Top" type="int" value="373"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="66"/>
							<Property name="X axis min" type="real" value="-0.779999999999999"/>
							<Property name="X axis max" type="real" value="0.779999999999974"/>
							<Property name="Y axis min" type="real" value="-0.0146352941176477"/>
							<Property name="Y axis max" type="real" value="0.746400000000032"/>
							<Property name="Chart bkg color" type="color" value="-16777208"/>
							<Property name="Chart axes color" type="color" value="12632256"/>
							<Property name="Chart plot color" type="color" value="15793151"/>
							<Property name="Chart bad sample color" type="color" value="255"/>
						</Properties>
					</Display>
				</Object>
			</Objects>
			<Connections>
				<Connection begin_id="3" begin_idx="0" end_id="5" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="65"/>
							<Segment index="1" value="0"/>
							<Segment index="2" value="10"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="4" begin_idx="0" end_id="5" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="43"/>
							<Segment index="1" value="-31"/>
							<Segment index="2" value="34"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="69"/>
							<Segment index="1" value="-3"/>
							<Segment index="2" value="67"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="2" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="100"/>
							<Segment index="1" value="-82"/>
							<Segment index="2" value="66"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="5" begin_idx="0" end_id="9" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="11"/>
							<Segment index="1" value="1"/>
							<Segment index="2" value="12"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="7" end_idx="0" type="1">
					<Display type="1">
						<Segments size="5">
							<Segment index="0" value="10"/>
							<Segment index="1" value="40"/>
							<Segment index="2" value="-50"/>
							<Segment index="3" value="40"/>
							<Segment index="4" value="8"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="6" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="149"/>
							<Segment index="1" value="46"/>
							<Segment index="2" value="149"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="8" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="200"/>
							<Segment index="1" value="211"/>
							<Segment index="2" value="142"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="7" begin_idx="0" end_id="6" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="67"/>
							<Segment index="1" value="-61"/>
							<Segment index="2" value="68"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="7" begin_idx="0" end_id="8" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="89"/>
							<Segment index="1" value="146"/>
							<Segment index="2" value="90"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="7" begin_idx="1" end_id="10" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="66"/>
							<Segment index="1" value="234"/>
							<Segment index="2" value="114"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="9" begin_idx="0" end_id="10" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="185"/>
							<Segment index="1" value="324"/>
							<Segment index="2" value="158"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
			</Connections>
		</PATH>
	</Paths>
</CalcPaths_Export_File>
