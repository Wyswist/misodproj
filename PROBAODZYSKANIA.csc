<CalcPaths_Export_File>
	<Simulator after_time="0" max_steps="0" sample_time="0.1" simulation_rate="100" simulation_ratio="1" start_time="1984-01-01T00:00:00.000" stop_condition="0" stop_time="1984-01-01T00:00:00.000"/>
	<Paths>
		<PATH ID="1" block_id="-1" index="-1" name="Path 1">
		<PATH ID="12" block_id="13" index="-1" name="Logika">
						<Paths>
							<PATH ID="13" block_id="45" index="-1" name="LogikaDlaCV>1">
								<Paths/>
								<Objects>
									<Object ID="0" flip="false" index="0" name="In" type="subpath_input">
										<Transform>
											<Properties>
												<Property name="Processing rate" type="int" value="1"/>
												<Property name="Index" type="int" value="1"/>
											</Properties>
										</Transform>
										<Display>
											<Properties>
												<Property name="Color" type="color" value="15780518"/>
												<Property name="Left" type="int" value="27"/>
												<Property name="Top" type="int" value="226"/>
												<Property name="Width" type="int" value="30"/>
												<Property name="Height" type="int" value="20"/>
											</Properties>
										</Display>
									</Object>
									<Object ID="1" flip="false" index="1" name="Demux 1" type="Demux">
										<Transform>
											<Properties>
												<Property name="Processing rate" type="int" value="1"/>
												<Property name="Outputs" type="uint" value="10"/>
											</Properties>
										</Transform>
										<Display>
											<Properties>
												<Property name="Color" type="color" value="12632256"/>
												<Property name="Left" type="int" value="124"/>
												<Property name="Top" type="int" value="62"/>
												<Property name="Width" type="int" value="20"/>
												<Property name="Height" type="int" value="500"/>
											</Properties>
										</Display>
									</Object>
									<Object ID="2" flip="false" index="2" name="dodatnia" type="gain">
										<Transform>
											<Properties>
												<Property name="Processing rate" type="int" value="1"/>
												<Property name="Gain" type="real" value="1"/>
												<Property name="Clear sig. status" type="bool" value="false"/>
											</Properties>
										</Transform>
										<Display>
											<Properties>
												<Property name="Color" type="color" value="12632256"/>
												<Property name="Left" type="int" value="202"/>
												<Property name="Top" type="int" value="88"/>
												<Property name="Width" type="int" value="60"/>
												<Property name="Height" type="int" value="15"/>
											</Properties>
										</Display>
									</Object>
									<Object ID="3" flip="false" index="3" name="ujemna" type="gain">
										<Transform>
											<Properties>
												<Property name="Processing rate" type="int" value="1"/>
												<Property name="Gain" type="real" value="1"/>
												<Property name="Clear sig. status" type="bool" value="false"/>
											</Properties>
										</Transform>
										<Display>
											<Properties>
												<Property name="Color" type="color" value="12632256"/>
												<Property name="Left" type="int" value="210"/>
												<Property name="Top" type="int" value="135"/>
												<Property name="Width" type="int" value="60"/>
												<Property name="Height" type="int" value="15"/>
											</Properties>
										</Display>
									</Object>
									<Object ID="4" flip="false" index="4" name="CV>beta" type="gain">
										<Transform>
											<Properties>
												<Property name="Processing rate" type="int" value="1"/>
												<Property name="Gain" type="real" value="1"/>
												<Property name="Clear sig. status" type="bool" value="false"/>
											</Properties>
										</Transform>
										<Display>
											<Properties>
												<Property name="Color" type="color" value="12632256"/>
												<Property name="Left" type="int" value="210"/>
												<Property name="Top" type="int" value="188"/>
												<Property name="Width" type="int" value="60"/>
												<Property name="Height" type="int" value="15"/>
											</Properties>
										</Display>
									</Object>
									<Object ID="5" flip="false" index="5" name="Cv>alfa+beta" type="gain">
										<Transform>
											<Properties>
												<Property name="Processing rate" type="int" value="1"/>
												<Property name="Gain" type="real" value="1"/>
												<Property name="Clear sig. status" type="bool" value="false"/>
											</Properties>
										</Transform>
										<Display>
											<Properties>
												<Property name="Color" type="color" value="12632256"/>
												<Property name="Left" type="int" value="213"/>
												<Property name="Top" type="int" value="244"/>
												<Property name="Width" type="int" value="60"/>
												<Property name="Height" type="int" value="15"/>
											</Properties>
										</Display>
									</Object>
								</Objects>
								<Connections/>
							</PATH>
						</Paths>
						<Objects>
							<Object ID="0" flip="false" index="0" name="In" type="subpath_input">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Index" type="int" value="1"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="15780518"/>
										<Property name="Left" type="int" value="21"/>
										<Property name="Top" type="int" value="302"/>
										<Property name="Width" type="int" value="30"/>
										<Property name="Height" type="int" value="20"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="1" flip="false" index="1" name="Demux 1" type="Demux">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Outputs" type="uint" value="10"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="124"/>
										<Property name="Top" type="int" value="62"/>
										<Property name="Width" type="int" value="20"/>
										<Property name="Height" type="int" value="500"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="2" flip="false" index="2" name="dodatnia" type="gain">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Gain" type="real" value="1"/>
										<Property name="Clear sig. status" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="245"/>
										<Property name="Top" type="int" value="95"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="15"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="3" flip="false" index="3" name="ujemna" type="gain">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Gain" type="real" value="1"/>
										<Property name="Clear sig. status" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="236"/>
										<Property name="Top" type="int" value="133"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="15"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="4" flip="false" index="4" name="CV>beta" type="gain">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Gain" type="real" value="1"/>
										<Property name="Clear sig. status" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="232"/>
										<Property name="Top" type="int" value="182"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="15"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="5" flip="false" index="5" name="Cv>alfa+beta" type="gain">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Gain" type="real" value="1"/>
										<Property name="Clear sig. status" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="242"/>
										<Property name="Top" type="int" value="232"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="15"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="6" flip="false" index="6" name="CV" type="subpath_input">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Index" type="int" value="2"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="15780518"/>
										<Property name="Left" type="int" value="130"/>
										<Property name="Top" type="int" value="615"/>
										<Property name="Width" type="int" value="30"/>
										<Property name="Height" type="int" value="20"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="7" flip="false" index="7" name="CV<-beta" type="gain">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Gain" type="real" value="1"/>
										<Property name="Clear sig. status" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="241"/>
										<Property name="Top" type="int" value="280"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="15"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="8" flip="false" index="8" name="CV<-(alfa+beta)" type="gain">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Gain" type="real" value="1"/>
										<Property name="Clear sig. status" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="243"/>
										<Property name="Top" type="int" value="326"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="15"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="9" flip="false" index="9" name="Ax+B1" type="gain">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Gain" type="real" value="1"/>
										<Property name="Clear sig. status" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="242"/>
										<Property name="Top" type="int" value="384"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="15"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="10" flip="false" index="10" name="Ax+B2" type="gain">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Gain" type="real" value="1"/>
										<Property name="Clear sig. status" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="246"/>
										<Property name="Top" type="int" value="429"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="15"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="11" flip="false" index="11" name="Ax+B4" type="gain">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Gain" type="real" value="1"/>
										<Property name="Clear sig. status" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="232"/>
										<Property name="Top" type="int" value="576"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="15"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="12" flip="false" index="12" name="Ax+B3" type="gain">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Gain" type="real" value="1"/>
										<Property name="Clear sig. status" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="244"/>
										<Property name="Top" type="int" value="495"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="15"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="13" flip="false" index="13" name="Logic operator 1" type="logical">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Inputs number" type="int" value="3"/>
										<Property name="Type" type="enum" value="0">
											<Enumeration index="0" name="AND"/>
											<Enumeration index="1" name="NAND"/>
											<Enumeration index="2" name="NOR"/>
											<Enumeration index="3" name="NOT"/>
											<Enumeration index="4" name="OR"/>
											<Enumeration index="5" name="XOR"/>
										</Property>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="688"/>
										<Property name="Top" type="int" value="497"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="45"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="20" flip="false" index="14" name="Product 1" type="product">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Actions string" type="string" value="**"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="1074"/>
										<Property name="Top" type="int" value="653"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="40"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="23" buffer_type="1" flip="false" index="15" name="Internal variable 1" type="internal_out_var">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Buffer size" type="int" value="1"/>
										<Property name="Buffer name" type="string" value="Buffer"/>
										<Property name="Auto commit" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12639424"/>
										<Property name="Left" type="int" value="720"/>
										<Property name="Top" type="int" value="628"/>
										<Property name="Width" type="int" value="100"/>
										<Property name="Height" type="int" value="37"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="24" buffer_type="1" flip="false" index="16" name="Internal variable 2" type="internal_out_var">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Buffer size" type="int" value="1"/>
										<Property name="Buffer name" type="string" value="Buffer 1"/>
										<Property name="Auto commit" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12639424"/>
										<Property name="Left" type="int" value="1008"/>
										<Property name="Top" type="int" value="454"/>
										<Property name="Width" type="int" value="100"/>
										<Property name="Height" type="int" value="37"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="25" flip="false" index="17" name="XY Display 1" type="xy_display">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Buffer length" type="uint" value="1000"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12639424"/>
										<Property name="Left" type="int" value="1388"/>
										<Property name="Top" type="int" value="355"/>
										<Property name="Width" type="int" value="100"/>
										<Property name="Height" type="int" value="66"/>
										<Property name="X axis min" type="real" value="-0.779999999999999"/>
										<Property name="X axis max" type="real" value="0.779999999999974"/>
										<Property name="Y axis min" type="real" value="-0.0141176470588229"/>
										<Property name="Y axis max" type="real" value="0.71999999999997"/>
										<Property name="Chart bkg color" type="color" value="16777215"/>
										<Property name="Chart axes color" type="color" value="12632256"/>
										<Property name="Chart plot color" type="color" value="0"/>
										<Property name="Chart bad sample color" type="color" value="255"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="27" flip="false" index="18" name="out" type="subpath_output">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Index" type="int" value="1"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12639424"/>
										<Property name="Left" type="int" value="1718"/>
										<Property name="Top" type="int" value="765"/>
										<Property name="Width" type="int" value="30"/>
										<Property name="Height" type="int" value="20"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="33" flip="false" index="19" name="MinMax Selector 1" type="min_max_select">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Inputs count" type="uint" value="2"/>
										<Property name="Function" type="enum" value="0">
											<Enumeration index="0" name="Max"/>
											<Enumeration index="1" name="Min"/>
										</Property>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="523"/>
										<Property name="Top" type="int" value="664"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="52"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="34" flip="true" index="20" name="One step delay 1" type="one_step_del">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Init value" type="real" value="0"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="527"/>
										<Property name="Top" type="int" value="757"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="45"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="26" flip="false" index="21" name="dMAX" type="math_expression">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Equation" type="string" value="i1[0]-i1[1]"/>
										<Property name="Inputs" type="int" value="1"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="709"/>
										<Property name="Top" type="int" value="734"/>
										<Property name="Width" type="int" value="80"/>
										<Property name="Height" type="int" value="30"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="28" flip="false" index="22" name="Relational operator 3" type="relational">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Type" type="enum" value="0">
											<Enumeration index="0" name="EQUAL"/>
											<Enumeration index="1" name="GREATER"/>
											<Enumeration index="2" name="GREATER or EQUAL"/>
											<Enumeration index="3" name="LESS"/>
											<Enumeration index="4" name="LESS or EQUAL"/>
											<Enumeration index="5" name="NOT EQUAL"/>
										</Property>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="845"/>
										<Property name="Top" type="int" value="734"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="45"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="29" flip="false" index="23" name="Constant 2" type="constant">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Value" type="real" value="0"/>
										<Property name="Min" type="real" value="0"/>
										<Property name="Max" type="real" value="1"/>
										<Property name="Limit output" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="15780518"/>
										<Property name="Left" type="int" value="670"/>
										<Property name="Top" type="int" value="801"/>
										<Property name="Width" type="int" value="30"/>
										<Property name="Height" type="int" value="30"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="30" flip="false" index="24" name="Constant 1" type="constant">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Value" type="real" value="1"/>
										<Property name="Min" type="real" value="0"/>
										<Property name="Max" type="real" value="1"/>
										<Property name="Limit output" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="15780518"/>
										<Property name="Left" type="int" value="409"/>
										<Property name="Top" type="int" value="631"/>
										<Property name="Width" type="int" value="30"/>
										<Property name="Height" type="int" value="30"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="31" flip="false" index="25" name="Logic operator 2" type="logical">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Inputs number" type="int" value="3"/>
										<Property name="Type" type="enum" value="0">
											<Enumeration index="0" name="AND"/>
											<Enumeration index="1" name="NAND"/>
											<Enumeration index="2" name="NOR"/>
											<Enumeration index="3" name="NOT"/>
											<Enumeration index="4" name="OR"/>
											<Enumeration index="5" name="XOR"/>
										</Property>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="960"/>
										<Property name="Top" type="int" value="599"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="45"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="35" buffer_type="1" flip="false" index="26" name="Internal variable 3" type="internal_out_var">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Buffer size" type="int" value="1"/>
										<Property name="Buffer name" type="string" value="Buffer 1 1"/>
										<Property name="Auto commit" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12639424"/>
										<Property name="Left" type="int" value="1049"/>
										<Property name="Top" type="int" value="757"/>
										<Property name="Width" type="int" value="100"/>
										<Property name="Height" type="int" value="37"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="32" flip="false" index="27" name="Relational operator 1" type="relational">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Type" type="enum" value="3">
											<Enumeration index="0" name="EQUAL"/>
											<Enumeration index="1" name="GREATER"/>
											<Enumeration index="2" name="GREATER or EQUAL"/>
											<Enumeration index="3" name="LESS"/>
											<Enumeration index="4" name="LESS or EQUAL"/>
											<Enumeration index="5" name="NOT EQUAL"/>
										</Property>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="512"/>
										<Property name="Top" type="int" value="571"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="45"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="37" buffer_type="1" flip="false" index="28" name="Internal variable 4" type="internal_out_var">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Buffer size" type="int" value="1"/>
										<Property name="Buffer name" type="string" value="Buffer 1 2"/>
										<Property name="Auto commit" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12639424"/>
										<Property name="Left" type="int" value="629"/>
										<Property name="Top" type="int" value="341"/>
										<Property name="Width" type="int" value="100"/>
										<Property name="Height" type="int" value="37"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="38" buffer_type="1" flip="false" index="29" name="Internal variable 5" type="internal_out_var">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Buffer size" type="int" value="1"/>
										<Property name="Buffer name" type="string" value="Buffer 1 2 1"/>
										<Property name="Auto commit" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12639424"/>
										<Property name="Left" type="int" value="628"/>
										<Property name="Top" type="int" value="415"/>
										<Property name="Width" type="int" value="100"/>
										<Property name="Height" type="int" value="37"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="40" buffer_type="1" flip="false" index="30" name="Internal variable 7" type="internal_out_var">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Buffer size" type="int" value="1"/>
										<Property name="Buffer name" type="string" value="Buffer 1 2 1 2"/>
										<Property name="Auto commit" type="bool" value="false"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12639424"/>
										<Property name="Left" type="int" value="424"/>
										<Property name="Top" type="int" value="353"/>
										<Property name="Width" type="int" value="100"/>
										<Property name="Height" type="int" value="37"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="36" flip="false" index="31" name="Logic operator 3" type="logical">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Inputs number" type="int" value="3"/>
										<Property name="Type" type="enum" value="0">
											<Enumeration index="0" name="AND"/>
											<Enumeration index="1" name="NAND"/>
											<Enumeration index="2" name="NOR"/>
											<Enumeration index="3" name="NOT"/>
											<Enumeration index="4" name="OR"/>
											<Enumeration index="5" name="XOR"/>
										</Property>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="1329"/>
										<Property name="Top" type="int" value="821"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="45"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="42" flip="false" index="32" name="Product 2" type="product">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Actions string" type="string" value="**"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="1423"/>
										<Property name="Top" type="int" value="862"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="40"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="43" flip="false" index="33" name="Display 1" type="display">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Buffer length" type="uint" value="100"/>
										<Property name="Inputs count" type="uint" value="2"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12639424"/>
										<Property name="Left" type="int" value="1576"/>
										<Property name="Top" type="int" value="561"/>
										<Property name="Width" type="int" value="170"/>
										<Property name="Height" type="int" value="60"/>
										<Property name="Chart min" type="real" value="0"/>
										<Property name="Chart max" type="real" value="0.731764705882384"/>
										<Property name="Autoscale" type="bool" value="true"/>
										<Property name="Scale each sep." type="bool" value="false"/>
										<Property name="Precision" type="int" value="3"/>
										<Property name="Digits" type="int" value="3"/>
										<Property name="Type" type="enum" value="1">
											<Enumeration index="0" name="Fixed"/>
											<Enumeration index="1" name="General"/>
											<Enumeration index="2" name="Scientific"/>
										</Property>
										<Property name="Bkg color" type="color" value="0"/>
										<Property name="Axes color" type="color" value="8421504"/>
										<Property name="Bad sample color" type="color" value="255"/>
										<Property name="Show sample nr" type="bool" value="false"/>
										<Property name="Color scheme" type="enum" value="0">
											<Enumeration index="0" name="dark"/>
											<Enumeration index="1" name="light"/>
										</Property>
									</Properties>
								</Display>
							</Object>
							<Object ID="39" flip="false" index="34" name="Relational operator 2" type="relational">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Type" type="enum" value="2">
											<Enumeration index="0" name="EQUAL"/>
											<Enumeration index="1" name="GREATER"/>
											<Enumeration index="2" name="GREATER or EQUAL"/>
											<Enumeration index="3" name="LESS"/>
											<Enumeration index="4" name="LESS or EQUAL"/>
											<Enumeration index="5" name="NOT EQUAL"/>
										</Property>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="866"/>
										<Property name="Top" type="int" value="553"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="45"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="41" flip="false" index="35" name="Logic operator 4" type="logical">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Inputs number" type="int" value="1"/>
										<Property name="Type" type="enum" value="1">
											<Enumeration index="0" name="AND"/>
											<Enumeration index="1" name="NAND"/>
											<Enumeration index="2" name="NOR"/>
											<Enumeration index="3" name="NOT"/>
											<Enumeration index="4" name="OR"/>
											<Enumeration index="5" name="XOR"/>
										</Property>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="1066"/>
										<Property name="Top" type="int" value="917"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="45"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="44" flip="false" index="36" name="Sum 1" type="sum">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
										<Property name="Signs" type="string" value="++"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="1520"/>
										<Property name="Top" type="int" value="768"/>
										<Property name="Width" type="int" value="60"/>
										<Property name="Height" type="int" value="60"/>
									</Properties>
								</Display>
							</Object>
							<Object ID="45" flip="false" index="37" name="LogikaDlaCV>1" type="subpath">
								<Transform>
									<Properties>
										<Property name="Processing rate" type="int" value="1"/>
									</Properties>
								</Transform>
								<Display>
									<Properties>
										<Property name="Color" type="color" value="12632256"/>
										<Property name="Left" type="int" value="471"/>
										<Property name="Top" type="int" value="1160"/>
										<Property name="Width" type="int" value="200"/>
										<Property name="Height" type="int" value="45"/>
									</Properties>
								</Display>
							</Object>
						</Objects>
						<Connections>
							<Connection begin_id="1" begin_idx="0" end_id="2" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="46"/>
										<Segment index="1" value="-5"/>
										<Segment index="2" value="60"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="1" begin_idx="1" end_id="3" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="47"/>
										<Segment index="1" value="-12"/>
										<Segment index="2" value="50"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="1" begin_idx="2" end_id="4" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="50"/>
										<Segment index="1" value="-8"/>
										<Segment index="2" value="43"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="1" begin_idx="3" end_id="5" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="51"/>
										<Segment index="1" value="-3"/>
										<Segment index="2" value="52"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="1" begin_idx="4" end_id="7" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="51"/>
										<Segment index="1" value="0"/>
										<Segment index="2" value="51"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="1" begin_idx="5" end_id="8" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="54"/>
										<Segment index="1" value="1"/>
										<Segment index="2" value="50"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="1" begin_idx="6" end_id="9" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="54"/>
										<Segment index="1" value="14"/>
										<Segment index="2" value="49"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="1" begin_idx="7" end_id="10" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="53"/>
										<Segment index="1" value="14"/>
										<Segment index="2" value="54"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="1" begin_idx="8" end_id="12" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="73"/>
										<Segment index="1" value="35"/>
										<Segment index="2" value="32"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="1" begin_idx="9" end_id="11" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="56"/>
										<Segment index="1" value="71"/>
										<Segment index="2" value="37"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="0" begin_idx="0" end_id="1" end_idx="0" type="20">
								<Display type="20">
									<Segments size="3">
										<Segment index="0" value="42"/>
										<Segment index="1" value="0"/>
										<Segment index="2" value="36"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="3" begin_idx="0" end_id="13" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="292"/>
										<Segment index="1" value="368"/>
										<Segment index="2" value="105"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="13" begin_idx="0" end_id="24" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="199"/>
										<Segment index="1" value="-47"/>
										<Segment index="2" value="66"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="33" begin_idx="0" end_id="34" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="14"/>
										<Segment index="1" value="98"/>
										<Segment index="2" value="-10"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="34" begin_idx="0" end_id="33" end_idx="1" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="-14"/>
										<Segment index="1" value="-81"/>
										<Segment index="2" value="10"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="9" begin_idx="0" end_id="33" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="29"/>
										<Segment index="1" value="290"/>
										<Segment index="2" value="197"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="33" begin_idx="0" end_id="20" end_idx="1" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="293"/>
										<Segment index="1" value="-2"/>
										<Segment index="2" value="203"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="33" begin_idx="0" end_id="26" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="89"/>
										<Segment index="1" value="68"/>
										<Segment index="2" value="42"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="26" begin_idx="0" end_id="28" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="47"/>
										<Segment index="1" value="0"/>
										<Segment index="2" value="14"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="29" begin_idx="0" end_id="28" end_idx="1" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="133"/>
										<Segment index="1" value="-52"/>
										<Segment index="2" value="17"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="33" begin_idx="0" end_id="23" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="33"/>
										<Segment index="1" value="-35"/>
										<Segment index="2" value="109"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="28" begin_idx="0" end_id="31" end_idx="1" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="14"/>
										<Segment index="1" value="-135"/>
										<Segment index="2" value="46"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="13" begin_idx="0" end_id="31" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="78"/>
										<Segment index="1" value="91"/>
										<Segment index="2" value="139"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="31" begin_idx="0" end_id="20" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="20"/>
										<Segment index="1" value="45"/>
										<Segment index="2" value="39"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="28" begin_idx="0" end_id="35" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="45"/>
										<Segment index="1" value="19"/>
										<Segment index="2" value="104"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="30" begin_idx="0" end_id="32" end_idx="1" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="57"/>
										<Segment index="1" value="-45"/>
										<Segment index="2" value="21"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="6" begin_idx="0" end_id="32" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="197"/>
										<Segment index="1" value="-39"/>
										<Segment index="2" value="160"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="32" begin_idx="0" end_id="13" end_idx="2" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="84"/>
										<Segment index="1" value="-63"/>
										<Segment index="2" value="37"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="32" begin_idx="0" end_id="38" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="43"/>
										<Segment index="1" value="-160"/>
										<Segment index="2" value="18"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="6" begin_idx="0" end_id="40" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="207"/>
										<Segment index="1" value="-254"/>
										<Segment index="2" value="62"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="4" begin_idx="0" end_id="13" end_idx="1" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="247"/>
										<Segment index="1" value="330"/>
										<Segment index="2" value="154"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="4" begin_idx="0" end_id="37" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="272"/>
										<Segment index="1" value="170"/>
										<Segment index="2" value="70"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="20" begin_idx="0" end_id="25" end_idx="1" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="129"/>
										<Segment index="1" value="-274"/>
										<Segment index="2" value="130"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="6" begin_idx="0" end_id="25" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="616"/>
										<Segment index="1" value="-248"/>
										<Segment index="2" value="617"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="3" begin_idx="0" end_id="36" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="89"/>
										<Segment index="1" value="692"/>
										<Segment index="2" value="949"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="4" begin_idx="0" end_id="36" end_idx="1" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="64"/>
										<Segment index="1" value="654"/>
										<Segment index="2" value="978"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="36" begin_idx="0" end_id="42" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="17"/>
										<Segment index="1" value="32"/>
										<Segment index="2" value="22"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="10" begin_idx="0" end_id="42" end_idx="1" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="930"/>
										<Segment index="1" value="452"/>
										<Segment index="2" value="192"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="20" begin_idx="0" end_id="43" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="368"/>
										<Segment index="1" value="-92"/>
										<Segment index="2" value="79"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="33" begin_idx="0" end_id="39" end_idx="1" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="262"/>
										<Segment index="1" value="-98"/>
										<Segment index="2" value="26"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="10" begin_idx="0" end_id="39" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="369"/>
										<Segment index="1" value="132"/>
										<Segment index="2" value="196"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="39" begin_idx="0" end_id="31" end_idx="2" type="1">
								<Display type="1">
									<Segments size="5">
										<Segment index="0" value="10"/>
										<Segment index="1" value="31"/>
										<Segment index="2" value="0"/>
										<Segment index="3" value="26"/>
										<Segment index="4" value="29"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="31" begin_idx="0" end_id="41" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="25"/>
										<Segment index="1" value="318"/>
										<Segment index="2" value="26"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="41" begin_idx="0" end_id="36" end_idx="2" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="73"/>
										<Segment index="1" value="-85"/>
										<Segment index="2" value="135"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="42" begin_idx="0" end_id="44" end_idx="1" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="27"/>
										<Segment index="1" value="-74"/>
										<Segment index="2" value="15"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="42" begin_idx="0" end_id="43" end_idx="1" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="5"/>
										<Segment index="1" value="-281"/>
										<Segment index="2" value="93"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="20" begin_idx="0" end_id="44" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="194"/>
										<Segment index="1" value="115"/>
										<Segment index="2" value="197"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
							<Connection begin_id="44" begin_idx="0" end_id="27" end_idx="0" type="1">
								<Display type="1">
									<Segments size="3">
										<Segment index="0" value="71"/>
										<Segment index="1" value="-23"/>
										<Segment index="2" value="72"/>
									</Segments>
								</Display>
								<Properties/>
							</Connection>
						</Connections>
					</PATH>
		<Objects/>
		<Connections/>
		</PATH>
	</Paths>
</CalcPaths_Export_File>
