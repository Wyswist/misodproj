<CalcPaths_Export_File>
	<Simulator after_time="500" max_steps="0" sample_time="1" simulation_rate="10" simulation_ratio="0.01" start_time="1984-01-01T00:00:00.000" stop_condition="0" stop_time="1984-01-01T00:00:00.000"/>
	<Paths>
		<PATH ID="4" block_id="-1" index="-1" name="Path 1">
			<Paths>
				<PATH ID="17" block_id="7" index="-1" name="ElNL">
					<Paths>
						<PATH ID="15" block_id="56" index="-1" name="Krzywa">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="156"/>
											<Property name="Top" type="int" value="642"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="146"/>
											<Property name="Top" type="int" value="824"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="145"/>
											<Property name="Top" type="int" value="1018"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="A" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="403"/>
											<Property name="Top" type="int" value="711"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="B1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="1-1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="396"/>
											<Property name="Top" type="int" value="817"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="B2" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="-i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="399"/>
											<Property name="Top" type="int" value="969"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="B3" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="(i1[0]+i2[0])/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="567"/>
											<Property name="Top" type="int" value="1146"/>
											<Property name="Width" type="int" value="150"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="B4" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i2[0]/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="417"/>
											<Property name="Top" type="int" value="1214"/>
											<Property name="Width" type="int" value="120"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="Product 1" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="678"/>
											<Property name="Top" type="int" value="669"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="834"/>
											<Property name="Top" type="int" value="734"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="Sum 2" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="837"/>
											<Property name="Top" type="int" value="853"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="Sum 3" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="836"/>
											<Property name="Top" type="int" value="970"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="Sum 4" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="840"/>
											<Property name="Top" type="int" value="1108"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="13" name="Ax+B1" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="945"/>
											<Property name="Top" type="int" value="743"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="14" flip="false" index="14" name="Ax+B2" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="956"/>
											<Property name="Top" type="int" value="864"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="15" flip="false" index="15" name="Ax+B3" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="960"/>
											<Property name="Top" type="int" value="984"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="16" flip="false" index="16" name="Ax+B4" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="4"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="964"/>
											<Property name="Top" type="int" value="1116"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="17" flip="false" index="17" name="Math expression 1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="-1+1/(1-i1[0]-i2[0])"/>
											<Property name="Inputs" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="391"/>
											<Property name="Top" type="int" value="1076"/>
											<Property name="Width" type="int" value="150"/>
											<Property name="Height" type="int" value="66"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="3" begin_idx="0" end_id="8" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="80"/>
											<Segment index="1" value="-49"/>
											<Segment index="2" value="80"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="50"/>
											<Segment index="1" value="58"/>
											<Segment index="2" value="51"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="52"/>
											<Segment index="1" value="177"/>
											<Segment index="2" value="52"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="53"/>
											<Segment index="1" value="294"/>
											<Segment index="2" value="50"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="54"/>
											<Segment index="1" value="432"/>
											<Segment index="2" value="53"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="9" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="164"/>
											<Segment index="1" value="-90"/>
											<Segment index="2" value="159"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="10" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="161"/>
											<Segment index="1" value="-123"/>
											<Segment index="2" value="162"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="12" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="218"/>
											<Segment index="1" value="-113"/>
											<Segment index="2" value="90"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="90"/>
											<Segment index="1" value="-101"/>
											<Segment index="2" value="142"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="248"/>
											<Segment index="1" value="30"/>
											<Segment index="2" value="249"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="116"/>
											<Segment index="1" value="5"/>
											<Segment index="2" value="109"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="128"/>
											<Segment index="1" value="157"/>
											<Segment index="2" value="100"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="123"/>
											<Segment index="1" value="402"/>
											<Segment index="2" value="123"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="4" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="109"/>
											<Segment index="1" value="-167"/>
											<Segment index="2" value="117"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="114"/>
											<Segment index="1" value="-15"/>
											<Segment index="2" value="115"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="118"/>
											<Segment index="1" value="230"/>
											<Segment index="2" value="129"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="28"/>
											<Segment index="1" value="-1"/>
											<Segment index="2" value="28"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="14" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="32"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="32"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="11" begin_idx="0" end_id="15" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="34"/>
											<Segment index="1" value="4"/>
											<Segment index="2" value="35"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="16" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="35"/>
											<Segment index="1" value="-2"/>
											<Segment index="2" value="34"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="3" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="116"/>
											<Segment index="1" value="-273"/>
											<Segment index="2" value="117"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="11" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="165"/>
											<Segment index="1" value="-113"/>
											<Segment index="2" value="135"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="17" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="125"/>
											<Segment index="1" value="264"/>
											<Segment index="2" value="95"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="17" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="125"/>
											<Segment index="1" value="92"/>
											<Segment index="2" value="96"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="19" block_id="43" index="-1" name="Pochodna">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="155"/>
											<Property name="Top" type="int" value="153"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="dodatnia" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="679"/>
											<Property name="Top" type="int" value="119"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="ujemna" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="681"/>
											<Property name="Top" type="int" value="202"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="Math expression 1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i1[0]-i1[1]"/>
											<Property name="Inputs" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="348"/>
											<Property name="Top" type="int" value="148"/>
											<Property name="Width" type="int" value="80"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="507"/>
											<Property name="Top" type="int" value="101"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="509"/>
											<Property name="Top" type="int" value="190"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="Constant 1" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="322"/>
											<Property name="Top" type="int" value="262"/>
											<Property name="Width" type="int" value="40"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="149"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="19"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="4" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="103"/>
											<Segment index="1" value="-146"/>
											<Segment index="2" value="47"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="104"/>
											<Segment index="1" value="-57"/>
											<Segment index="2" value="48"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="21"/>
											<Segment index="1" value="-47"/>
											<Segment index="2" value="63"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="48"/>
											<Segment index="1" value="42"/>
											<Segment index="2" value="38"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="1" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="58"/>
											<Segment index="1" value="6"/>
											<Segment index="2" value="59"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="2" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="58"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="59"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="20" block_id="44" index="-1" name="Przedzialy">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="242"/>
											<Property name="Top" type="int" value="203"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="75"/>
											<Property name="Top" type="int" value="574"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="99"/>
											<Property name="Top" type="int" value="340"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="538"/>
											<Property name="Top" type="int" value="211"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="538"/>
											<Property name="Top" type="int" value="310"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Relational operator 3" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="520"/>
											<Property name="Top" type="int" value="456"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="Relational operator 4" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="516"/>
											<Property name="Top" type="int" value="578"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="alfaplusbeta" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="160"/>
											<Property name="Top" type="int" value="500"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="-(alfaplusbeta)" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="-1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="248"/>
											<Property name="Top" type="int" value="594"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="Gain 1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="-1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="206"/>
											<Property name="Top" type="int" value="406"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="CVbeta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="791"/>
											<Property name="Top" type="int" value="243"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="CValfaplusbeta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="829"/>
											<Property name="Top" type="int" value="326"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="CVbeta" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="3"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="831"/>
											<Property name="Top" type="int" value="457"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="13" name="CV" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="4"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="830"/>
											<Property name="Top" type="int" value="591"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="7" begin_idx="0" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="19"/>
											<Segment index="1" value="89"/>
											<Segment index="2" value="14"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="124"/>
											<Segment index="1" value="13"/>
											<Segment index="2" value="147"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="123"/>
											<Segment index="1" value="112"/>
											<Segment index="2" value="148"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="148"/>
											<Segment index="1" value="258"/>
											<Segment index="2" value="105"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="6" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="134"/>
											<Segment index="1" value="380"/>
											<Segment index="2" value="115"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="38"/>
											<Segment index="1" value="-58"/>
											<Segment index="2" value="22"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="18"/>
											<Segment index="1" value="163"/>
											<Segment index="2" value="18"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="6" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="105"/>
											<Segment index="1" value="-1"/>
											<Segment index="2" value="108"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="3" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="208"/>
											<Segment index="1" value="-109"/>
											<Segment index="2" value="206"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="4" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="157"/>
											<Segment index="1" value="-180"/>
											<Segment index="2" value="166"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="41"/>
											<Segment index="1" value="71"/>
											<Segment index="2" value="41"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="5" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="129"/>
											<Segment index="1" value="65"/>
											<Segment index="2" value="130"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="99"/>
											<Segment index="1" value="20"/>
											<Segment index="2" value="99"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="113"/>
											<Segment index="1" value="4"/>
											<Segment index="2" value="123"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="129"/>
											<Segment index="1" value="-11"/>
											<Segment index="2" value="127"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="129"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="130"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="18" block_id="34" index="-1" name="backupLogikaDlaCV1">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="In" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="27"/>
											<Property name="Top" type="int" value="226"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="Demux 1" type="Demux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Outputs" type="uint" value="10"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="124"/>
											<Property name="Top" type="int" value="62"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="500"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="dodatnia" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="231"/>
											<Property name="Top" type="int" value="56"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="ujemna" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="233"/>
											<Property name="Top" type="int" value="125"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="CVbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="239"/>
											<Property name="Top" type="int" value="179"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Cv>alfaplusbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="225"/>
											<Property name="Top" type="int" value="244"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="CVbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="225"/>
											<Property name="Top" type="int" value="287"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="CValfaplusbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="235"/>
											<Property name="Top" type="int" value="334"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="Ax+B1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="237"/>
											<Property name="Top" type="int" value="380"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="Ax+B2" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="234"/>
											<Property name="Top" type="int" value="427"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="Ax+B3" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="236"/>
											<Property name="Top" type="int" value="483"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="Ax+B4" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="239"/>
											<Property name="Top" type="int" value="554"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="17" flip="false" index="12" name="Product 2" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="727"/>
											<Property name="Top" type="int" value="344"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="18" flip="false" index="13" name="Product 3" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="731"/>
											<Property name="Top" type="int" value="448"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="19" flip="false" index="14" name="Product 4" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="733"/>
											<Property name="Top" type="int" value="548"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="20" flip="false" index="15" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="+++++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="900"/>
											<Property name="Top" type="int" value="253"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="350"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="21" flip="false" index="16" name="Constant 1" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="745"/>
											<Property name="Top" type="int" value="635"/>
											<Property name="Width" type="int" value="40"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="22" flip="false" index="17" name="Signal limiter 1" type="signal_limiter">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Low limit" type="real" value="-1"/>
											<Property name="High limit" type="real" value="1"/>
											<Property name="Rising rate limit" type="real" value="0"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1143"/>
											<Property name="Top" type="int" value="418"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="23" flip="false" index="18" name="wykres" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1264"/>
											<Property name="Top" type="int" value="430"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="24" flip="false" index="19" name="Logic operator 1" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="583"/>
											<Property name="Top" type="int" value="40"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="0" begin_idx="0" end_id="1" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="53"/>
											<Segment index="1" value="76"/>
											<Segment index="2" value="19"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="0" end_id="2" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="70"/>
											<Segment index="1" value="-44"/>
											<Segment index="2" value="22"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="1" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="68"/>
											<Segment index="1" value="-20"/>
											<Segment index="2" value="26"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="2" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="68"/>
											<Segment index="1" value="-11"/>
											<Segment index="2" value="32"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="3" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="70"/>
											<Segment index="1" value="9"/>
											<Segment index="2" value="16"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="5" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="65"/>
											<Segment index="1" value="9"/>
											<Segment index="2" value="31"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="6" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="49"/>
											<Segment index="1" value="10"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="7" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="47"/>
											<Segment index="1" value="12"/>
											<Segment index="2" value="48"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="8" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="48"/>
											<Segment index="1" value="23"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="9" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="46"/>
											<Segment index="1" value="49"/>
											<Segment index="2" value="54"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="4" end_id="6" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="43"/>
											<Segment index="1" value="7"/>
											<Segment index="2" value="43"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="17" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="285"/>
											<Segment index="1" value="-64"/>
											<Segment index="2" value="153"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="18" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="219"/>
											<Segment index="1" value="-16"/>
											<Segment index="2" value="221"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="11" begin_idx="0" end_id="19" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="219"/>
											<Segment index="1" value="13"/>
											<Segment index="2" value="220"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="20" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="47"/>
											<Segment index="1" value="5"/>
											<Segment index="2" value="71"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="20" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="44"/>
											<Segment index="1" value="-41"/>
											<Segment index="2" value="70"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="19" begin_idx="0" end_id="20" end_idx="3" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="52"/>
											<Segment index="1" value="-83"/>
											<Segment index="2" value="60"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="21" begin_idx="0" end_id="20" end_idx="4" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="74"/>
											<Segment index="1" value="-107"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="22" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="94"/>
											<Segment index="1" value="5"/>
											<Segment index="2" value="94"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="22" begin_idx="0" end_id="23" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="22"/>
											<Segment index="1" value="7"/>
											<Segment index="2" value="44"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="17" end_idx="1" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="10"/>
											<Segment index="1" value="3"/>
											<Segment index="2" value="-72"/>
											<Segment index="3" value="3"/>
											<Segment index="4" value="10"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="22" block_id="13" index="-1" name="Logika">
							<Paths/>
							<Objects>
								<Object ID="0" flip="false" index="0" name="In" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="111"/>
											<Property name="Top" type="int" value="305"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="Demux 1" type="Demux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Outputs" type="uint" value="10"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="168"/>
											<Property name="Top" type="int" value="59"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="500"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="dodatnia" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="245"/>
											<Property name="Top" type="int" value="95"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="ujemna" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="255"/>
											<Property name="Top" type="int" value="133"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="CVbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="249"/>
											<Property name="Top" type="int" value="182"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Cv>alfaplusbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="242"/>
											<Property name="Top" type="int" value="232"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="123"/>
											<Property name="Top" type="int" value="674"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="CVbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="241"/>
											<Property name="Top" type="int" value="280"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="CValfaplusbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="243"/>
											<Property name="Top" type="int" value="326"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="Ax+B1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="242"/>
											<Property name="Top" type="int" value="384"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="Ax+B2" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="246"/>
											<Property name="Top" type="int" value="429"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="Ax+B4" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="232"/>
											<Property name="Top" type="int" value="576"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="Ax+B3" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="244"/>
											<Property name="Top" type="int" value="495"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="13" flip="false" index="13" name="Logic operator 1" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="690"/>
											<Property name="Top" type="int" value="497"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="20" flip="false" index="14" name="Product 1" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1107"/>
											<Property name="Top" type="int" value="656"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="27" flip="false" index="15" name="out" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1871"/>
											<Property name="Top" type="int" value="769"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="33" flip="false" index="16" name="MinMax Selector 1" type="min_max_select">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs count" type="uint" value="2"/>
											<Property name="Function" type="enum" value="0">
												<Enumeration index="0" name="Max"/>
												<Enumeration index="1" name="Min"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="523"/>
											<Property name="Top" type="int" value="664"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="52"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="34" flip="true" index="17" name="One step delay 1" type="one_step_del">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Init value" type="real" value="0"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="525"/>
											<Property name="Top" type="int" value="765"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="26" flip="false" index="18" name="dMAX" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i1[0]-i1[1]"/>
											<Property name="Inputs" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="709"/>
											<Property name="Top" type="int" value="734"/>
											<Property name="Width" type="int" value="80"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="28" flip="false" index="19" name="Relational operator 3" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="853"/>
											<Property name="Top" type="int" value="735"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="29" flip="false" index="20" name="Constant 2" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="669"/>
											<Property name="Top" type="int" value="775"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="30" flip="false" index="21" name="Constant 1" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="1"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="401"/>
											<Property name="Top" type="int" value="646"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="31" flip="false" index="22" name="Logic operator 2" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="961"/>
											<Property name="Top" type="int" value="599"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="32" flip="false" index="23" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="3">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="526"/>
											<Property name="Top" type="int" value="579"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="36" flip="false" index="24" name="Logic operator 3" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1297"/>
											<Property name="Top" type="int" value="805"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="42" flip="false" index="25" name="Product 2" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1409"/>
											<Property name="Top" type="int" value="900"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="39" flip="false" index="26" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="2">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="866"/>
											<Property name="Top" type="int" value="553"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="44" flip="false" index="27" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="+++++++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1533"/>
											<Property name="Top" type="int" value="730"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="135"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="41" flip="false" index="28" name="Logic operator 4" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="1"/>
											<Property name="Type" type="enum" value="3">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1081"/>
											<Property name="Top" type="int" value="857"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="46" flip="false" index="29" name="Relational operator 4" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="373"/>
											<Property name="Top" type="int" value="1033"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="51" flip="false" index="30" name="minusbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="165"/>
											<Property name="Top" type="int" value="968"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="53" flip="false" index="31" name="Constant 3" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="-1"/>
											<Property name="Min" type="real" value="-1"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="146"/>
											<Property name="Top" type="int" value="1165"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="54" flip="false" index="32" name="Logic operator 5" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="779"/>
											<Property name="Top" type="int" value="1081"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="55" flip="false" index="33" name="MinMax Selector 2" type="min_max_select">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs count" type="uint" value="2"/>
											<Property name="Function" type="enum" value="1">
												<Enumeration index="0" name="Max"/>
												<Enumeration index="1" name="Min"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="647"/>
											<Property name="Top" type="int" value="894"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="52"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="56" flip="true" index="34" name="One step delay 2" type="one_step_del">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Init value" type="real" value="0"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="650"/>
											<Property name="Top" type="int" value="981"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="57" flip="false" index="35" name="Math expression 1" type="math_expression">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Equation" type="string" value="i1[0]-i1[1]"/>
											<Property name="Inputs" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="757"/>
											<Property name="Top" type="int" value="867"/>
											<Property name="Width" type="int" value="80"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="58" flip="false" index="36" name="Relational operator 6" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="888"/>
											<Property name="Top" type="int" value="917"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="59" flip="false" index="37" name="Constant 4" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="763"/>
											<Property name="Top" type="int" value="947"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="60" flip="false" index="38" name="Logic operator 6" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1030"/>
											<Property name="Top" type="int" value="958"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="61" flip="false" index="39" name="Relational operator 7" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="887"/>
											<Property name="Top" type="int" value="993"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="62" flip="false" index="40" name="Product 3" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1216"/>
											<Property name="Top" type="int" value="953"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="64" flip="false" index="41" name="Relational operator 8" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="2">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="366"/>
											<Property name="Top" type="int" value="1148"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="73" flip="false" index="42" name="Logic operator 7" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="1"/>
											<Property name="Type" type="enum" value="3">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1136"/>
											<Property name="Top" type="int" value="1189"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="74" flip="false" index="43" name="Logic operator 8" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1272"/>
											<Property name="Top" type="int" value="1027"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="63" flip="false" index="44" name="Product 4" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1386"/>
											<Property name="Top" type="int" value="992"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="47" flip="false" index="45" name="Logic operator 9" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="431"/>
											<Property name="Top" type="int" value="142"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="48" flip="false" index="46" name="Logic operator 10" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="2"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="467"/>
											<Property name="Top" type="int" value="310"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="49" flip="false" index="47" name="Product 5" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="648"/>
											<Property name="Top" type="int" value="212"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="50" flip="false" index="48" name="Product 6" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="649"/>
											<Property name="Top" type="int" value="385"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="52" flip="false" index="49" name="Signal limiter 1" type="signal_limiter">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Low limit" type="real" value="-1"/>
											<Property name="High limit" type="real" value="1"/>
											<Property name="Rising rate limit" type="real" value="0"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1642"/>
											<Property name="Top" type="int" value="784"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="65" flip="false" index="50" name="XY Display 1" type="xy_display">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer length" type="uint" value="1000"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1778"/>
											<Property name="Top" type="int" value="862"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="66"/>
											<Property name="X axis min" type="real" value="-1"/>
											<Property name="X axis max" type="real" value="1"/>
											<Property name="Y axis min" type="real" value="-1"/>
											<Property name="Y axis max" type="real" value="1"/>
											<Property name="Chart bkg color" type="color" value="-16777202"/>
											<Property name="Chart axes color" type="color" value="12632256"/>
											<Property name="Chart plot color" type="color" value="0"/>
											<Property name="Chart bad sample color" type="color" value="255"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="66" buffer_type="1" flip="false" index="51" name="Internal variable 1" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 2"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1791"/>
											<Property name="Top" type="int" value="708"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="67" buffer_type="1" flip="false" index="52" name="Internal variable 2" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 1 1"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1789"/>
											<Property name="Top" type="int" value="966"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="68" flip="false" index="53" name="Display 1" type="display">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer length" type="uint" value="900"/>
											<Property name="Inputs count" type="uint" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1906"/>
											<Property name="Top" type="int" value="817"/>
											<Property name="Width" type="int" value="170"/>
											<Property name="Height" type="int" value="45"/>
											<Property name="Chart min" type="real" value="-1.05"/>
											<Property name="Chart max" type="real" value="1.05"/>
											<Property name="Autoscale" type="bool" value="false"/>
											<Property name="Scale each sep." type="bool" value="false"/>
											<Property name="Precision" type="int" value="3"/>
											<Property name="Digits" type="int" value="3"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="Fixed"/>
												<Enumeration index="1" name="General"/>
												<Enumeration index="2" name="Scientific"/>
											</Property>
											<Property name="Bkg color" type="color" value="0"/>
											<Property name="Axes color" type="color" value="8421504"/>
											<Property name="Bad sample color" type="color" value="255"/>
											<Property name="Show sample nr" type="bool" value="false"/>
											<Property name="Color scheme" type="enum" value="0">
												<Enumeration index="0" name="dark"/>
												<Enumeration index="1" name="light"/>
											</Property>
										</Properties>
									</Display>
								</Object>
								<Object ID="69" flip="false" index="54" name="Constant 5" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="0"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="1497"/>
											<Property name="Top" type="int" value="950"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="1" begin_idx="0" end_id="2" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="2"/>
											<Segment index="1" value="-2"/>
											<Segment index="2" value="60"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="1" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="3"/>
											<Segment index="1" value="-9"/>
											<Segment index="2" value="69"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="2" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="6"/>
											<Segment index="1" value="-5"/>
											<Segment index="2" value="60"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="3" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="7"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="52"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="4" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="7"/>
											<Segment index="1" value="3"/>
											<Segment index="2" value="51"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="5" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="10"/>
											<Segment index="1" value="4"/>
											<Segment index="2" value="50"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="6" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="10"/>
											<Segment index="1" value="17"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="7" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="9"/>
											<Segment index="1" value="17"/>
											<Segment index="2" value="54"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="8" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="29"/>
											<Segment index="1" value="38"/>
											<Segment index="2" value="32"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="9" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="12"/>
											<Segment index="1" value="74"/>
											<Segment index="2" value="37"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="13" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="273"/>
											<Segment index="1" value="368"/>
											<Segment index="2" value="107"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="33" begin_idx="0" end_id="34" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="53"/>
											<Segment index="1" value="106"/>
											<Segment index="2" value="-51"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="34" begin_idx="0" end_id="33" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="-12"/>
											<Segment index="1" value="-89"/>
											<Segment index="2" value="10"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="33" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="29"/>
											<Segment index="1" value="290"/>
											<Segment index="2" value="197"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="33" begin_idx="0" end_id="20" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="293"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="236"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="33" begin_idx="0" end_id="26" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="89"/>
											<Segment index="1" value="68"/>
											<Segment index="2" value="42"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="26" begin_idx="0" end_id="28" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="47"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="22"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="29" begin_idx="0" end_id="28" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="134"/>
											<Segment index="1" value="-25"/>
											<Segment index="2" value="25"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="28" begin_idx="0" end_id="31" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="6"/>
											<Segment index="1" value="-136"/>
											<Segment index="2" value="47"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="13" begin_idx="0" end_id="31" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="76"/>
											<Segment index="1" value="91"/>
											<Segment index="2" value="140"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="31" begin_idx="0" end_id="20" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="19"/>
											<Segment index="1" value="48"/>
											<Segment index="2" value="72"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="30" begin_idx="0" end_id="32" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="49"/>
											<Segment index="1" value="-52"/>
											<Segment index="2" value="51"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="32" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="194"/>
											<Segment index="1" value="-90"/>
											<Segment index="2" value="184"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="32" begin_idx="0" end_id="13" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="70"/>
											<Segment index="1" value="-71"/>
											<Segment index="2" value="39"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="13" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="230"/>
											<Segment index="1" value="330"/>
											<Segment index="2" value="156"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="36" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="70"/>
											<Segment index="1" value="676"/>
											<Segment index="2" value="917"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="36" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="47"/>
											<Segment index="1" value="638"/>
											<Segment index="2" value="946"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="36" begin_idx="0" end_id="42" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="30"/>
											<Segment index="1" value="86"/>
											<Segment index="2" value="27"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="42" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="948"/>
											<Segment index="1" value="490"/>
											<Segment index="2" value="160"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="33" begin_idx="0" end_id="39" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="262"/>
											<Segment index="1" value="-98"/>
											<Segment index="2" value="26"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="39" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="369"/>
											<Segment index="1" value="132"/>
											<Segment index="2" value="196"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="39" begin_idx="0" end_id="31" end_idx="2" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="10"/>
											<Segment index="1" value="31"/>
											<Segment index="2" value="0"/>
											<Segment index="3" value="26"/>
											<Segment index="4" value="30"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="44" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="179"/>
											<Segment index="1" value="71"/>
											<Segment index="2" value="192"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="31" begin_idx="0" end_id="41" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="37"/>
											<Segment index="1" value="258"/>
											<Segment index="2" value="28"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="41" begin_idx="0" end_id="36" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="75"/>
											<Segment index="1" value="-41"/>
											<Segment index="2" value="86"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="46" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="89"/>
											<Segment index="1" value="364"/>
											<Segment index="2" value="136"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="51" end_idx="0" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="10"/>
											<Segment index="1" value="334"/>
											<Segment index="2" value="-151"/>
											<Segment index="3" value="362"/>
											<Segment index="4" value="10"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="51" begin_idx="0" end_id="46" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="83"/>
											<Segment index="1" value="80"/>
											<Segment index="2" value="70"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="46" begin_idx="0" end_id="54" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="126"/>
											<Segment index="1" value="48"/>
											<Segment index="2" value="225"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="54" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="25"/>
											<Segment index="1" value="990"/>
											<Segment index="2" value="454"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="55" begin_idx="0" end_id="56" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="17"/>
											<Segment index="1" value="92"/>
											<Segment index="2" value="-14"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="56" begin_idx="0" end_id="55" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="-29"/>
											<Segment index="1" value="-75"/>
											<Segment index="2" value="26"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="55" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="22"/>
											<Segment index="1" value="409"/>
											<Segment index="2" value="326"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="55" begin_idx="0" end_id="57" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="32"/>
											<Segment index="1" value="-29"/>
											<Segment index="2" value="23"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="57" begin_idx="0" end_id="58" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="32"/>
											<Segment index="1" value="50"/>
											<Segment index="2" value="24"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="59" begin_idx="0" end_id="58" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="53"/>
											<Segment index="1" value="-15"/>
											<Segment index="2" value="47"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="58" begin_idx="0" end_id="60" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="48"/>
											<Segment index="1" value="30"/>
											<Segment index="2" value="39"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="61" begin_idx="0" end_id="60" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="49"/>
											<Segment index="1" value="-35"/>
											<Segment index="2" value="39"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="54" begin_idx="0" end_id="60" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="171"/>
											<Segment index="1" value="-112"/>
											<Segment index="2" value="25"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="60" begin_idx="0" end_id="62" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="29"/>
											<Segment index="1" value="-1"/>
											<Segment index="2" value="102"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="55" begin_idx="0" end_id="62" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="451"/>
											<Segment index="1" value="55"/>
											<Segment index="2" value="63"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="53" begin_idx="0" end_id="64" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="90"/>
											<Segment index="1" value="-2"/>
											<Segment index="2" value="35"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="64" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="133"/>
											<Segment index="1" value="479"/>
											<Segment index="2" value="85"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="64" begin_idx="0" end_id="54" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="232"/>
											<Segment index="1" value="-56"/>
											<Segment index="2" value="126"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="55" begin_idx="0" end_id="61" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="45"/>
											<Segment index="1" value="112"/>
											<Segment index="2" value="140"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="62" begin_idx="0" end_id="44" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="125"/>
											<Segment index="1" value="-209"/>
											<Segment index="2" value="137"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="11" begin_idx="0" end_id="61" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="109"/>
											<Segment index="1" value="425"/>
											<Segment index="2" value="491"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="42" begin_idx="0" end_id="44" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="29"/>
											<Segment index="1" value="-139"/>
											<Segment index="2" value="40"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="60" begin_idx="0" end_id="73" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="29"/>
											<Segment index="1" value="231"/>
											<Segment index="2" value="22"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="73" begin_idx="0" end_id="74" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="49"/>
											<Segment index="1" value="-151"/>
											<Segment index="2" value="32"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="7" begin_idx="0" end_id="74" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="518"/>
											<Segment index="1" value="762"/>
											<Segment index="2" value="458"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="74" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="528"/>
											<Segment index="1" value="936"/>
											<Segment index="2" value="444"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="74" begin_idx="0" end_id="63" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="28"/>
											<Segment index="1" value="-31"/>
											<Segment index="2" value="31"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="11" begin_idx="0" end_id="63" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="559"/>
											<Segment index="1" value="422"/>
											<Segment index="2" value="540"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="63" begin_idx="0" end_id="44" end_idx="3" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="42"/>
											<Segment index="1" value="-214"/>
											<Segment index="2" value="50"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="47" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="95"/>
											<Segment index="1" value="55"/>
											<Segment index="2" value="36"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="47" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="69"/>
											<Segment index="1" value="-67"/>
											<Segment index="2" value="65"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="48" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="66"/>
											<Segment index="1" value="185"/>
											<Segment index="2" value="91"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="48" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="72"/>
											<Segment index="1" value="7"/>
											<Segment index="2" value="97"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="47" begin_idx="0" end_id="49" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="66"/>
											<Segment index="1" value="61"/>
											<Segment index="2" value="96"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="48" begin_idx="0" end_id="50" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="77"/>
											<Segment index="1" value="66"/>
											<Segment index="2" value="50"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="49" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="158"/>
											<Segment index="1" value="-153"/>
											<Segment index="2" value="193"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="50" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="201"/>
											<Segment index="1" value="-91"/>
											<Segment index="2" value="149"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="50" begin_idx="0" end_id="44" end_idx="4" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="729"/>
											<Segment index="1" value="410"/>
											<Segment index="2" value="100"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="49" begin_idx="0" end_id="44" end_idx="5" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="752"/>
											<Segment index="1" value="600"/>
											<Segment index="2" value="78"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="44" begin_idx="0" end_id="52" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="44"/>
											<Segment index="1" value="2"/>
											<Segment index="2" value="10"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="52" begin_idx="0" end_id="27" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="86"/>
											<Segment index="1" value="-20"/>
											<Segment index="2" value="88"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="52" begin_idx="0" end_id="65" end_idx="1" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="54"/>
											<Segment index="1" value="49"/>
											<Segment index="2" value="-22"/>
											<Segment index="3" value="58"/>
											<Segment index="4" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="65" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="815"/>
											<Segment index="1" value="200"/>
											<Segment index="2" value="815"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="52" begin_idx="0" end_id="66" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="69"/>
											<Segment index="1" value="-73"/>
											<Segment index="2" value="25"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="67" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="773"/>
											<Segment index="1" value="300"/>
											<Segment index="2" value="868"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="52" begin_idx="0" end_id="68" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="127"/>
											<Segment index="1" value="40"/>
											<Segment index="2" value="82"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="69" begin_idx="0" end_id="44" end_idx="6" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="19"/>
											<Segment index="1" value="-70"/>
											<Segment index="2" value="-18"/>
											<Segment index="3" value="-53"/>
											<Segment index="4" value="10"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="1" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="16"/>
											<Segment index="1" value="-6"/>
											<Segment index="2" value="16"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
						<PATH ID="23" block_id="11" index="-1" name="Logika">
							<Paths>
								<PATH ID="11" block_id="62" index="-1" name="CV_mniejsze_od1">
									<Paths/>
									<Objects>
										<Object ID="0" flip="false" index="0" name="In" type="subpath_input">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="1"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="-18"/>
													<Property name="Top" type="int" value="1094"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="1" flip="false" index="1" name="Demux 1" type="Demux">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Outputs" type="uint" value="7"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="44"/>
													<Property name="Top" type="int" value="877"/>
													<Property name="Width" type="int" value="20"/>
													<Property name="Height" type="int" value="450"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="2" flip="false" index="2" name="CV" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="239"/>
													<Property name="Top" type="int" value="891"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="3" flip="false" index="3" name="dodatnia" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="202"/>
													<Property name="Top" type="int" value="1061"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="4" flip="false" index="4" name="ujemna" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="199"/>
													<Property name="Top" type="int" value="1121"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="5" flip="false" index="5" name="CV_w_alfa_beta" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="192"/>
													<Property name="Top" type="int" value="1176"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="6" flip="false" index="6" name="CV_m_alfa_beta" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="186"/>
													<Property name="Top" type="int" value="1231"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="7" flip="false" index="7" name="Ax+B1" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="182"/>
													<Property name="Top" type="int" value="1279"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="8" flip="false" index="8" name="Ax+B3" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="175"/>
													<Property name="Top" type="int" value="1337"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="10" flip="false" index="9" name="Constant 1" type="constant">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Value" type="real" value="1"/>
													<Property name="Min" type="real" value="-1"/>
													<Property name="Max" type="real" value="1"/>
													<Property name="Limit output" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="373"/>
													<Property name="Top" type="int" value="1027"/>
													<Property name="Width" type="int" value="50"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="11" flip="false" index="10" name="Constant 2" type="constant">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Value" type="real" value="-1"/>
													<Property name="Min" type="real" value="-1"/>
													<Property name="Max" type="real" value="1"/>
													<Property name="Limit output" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="352"/>
													<Property name="Top" type="int" value="1224"/>
													<Property name="Width" type="int" value="50"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="12" flip="false" index="11" name="Relational operator 2" type="relational">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Type" type="enum" value="1">
														<Enumeration index="0" name="EQUAL"/>
														<Enumeration index="1" name="GREATER"/>
														<Enumeration index="2" name="GREATER or EQUAL"/>
														<Enumeration index="3" name="LESS"/>
														<Enumeration index="4" name="LESS or EQUAL"/>
														<Enumeration index="5" name="NOT EQUAL"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="453"/>
													<Property name="Top" type="int" value="1190"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="23" flip="false" index="12" name="Logic operator 3" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="3"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="726"/>
													<Property name="Top" type="int" value="965"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="26" flip="false" index="13" name="Logic operator 4" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="3"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="731"/>
													<Property name="Top" type="int" value="1038"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="27" flip="false" index="14" name="Product 2" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="847"/>
													<Property name="Top" type="int" value="968"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="28" flip="false" index="15" name="Product 3" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="854"/>
													<Property name="Top" type="int" value="1069"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="29" flip="false" index="16" name="Sum 2" type="sum">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Signs" type="string" value="++"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="946"/>
													<Property name="Top" type="int" value="1003"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="60"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="30" flip="false" index="17" name="XY Display 1" type="xy_display">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Buffer length" type="uint" value="1000"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12639424"/>
													<Property name="Left" type="int" value="1225"/>
													<Property name="Top" type="int" value="975"/>
													<Property name="Width" type="int" value="100"/>
													<Property name="Height" type="int" value="66"/>
													<Property name="X axis min" type="real" value="-1.13601428232733"/>
													<Property name="X axis max" type="real" value="1.14384341730054"/>
													<Property name="Y axis min" type="real" value="-1.03737454525005"/>
													<Property name="Y axis max" type="real" value="1.03737454525005"/>
													<Property name="Chart bkg color" type="color" value="16777215"/>
													<Property name="Chart axes color" type="color" value="12632256"/>
													<Property name="Chart plot color" type="color" value="0"/>
													<Property name="Chart bad sample color" type="color" value="255"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="31" flip="false" index="18" name="Relational operator 1" type="relational">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Type" type="enum" value="3">
														<Enumeration index="0" name="EQUAL"/>
														<Enumeration index="1" name="GREATER"/>
														<Enumeration index="2" name="GREATER or EQUAL"/>
														<Enumeration index="3" name="LESS"/>
														<Enumeration index="4" name="LESS or EQUAL"/>
														<Enumeration index="5" name="NOT EQUAL"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="526"/>
													<Property name="Top" type="int" value="985"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="32" flip="false" index="19" name="Logic operator 5" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="3"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="734"/>
													<Property name="Top" type="int" value="1155"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="33" flip="false" index="20" name="Logic operator 6" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="3"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="736"/>
													<Property name="Top" type="int" value="1224"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="34" flip="false" index="21" name="Product 4" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="860"/>
													<Property name="Top" type="int" value="1138"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="35" flip="false" index="22" name="Product 5" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="862"/>
													<Property name="Top" type="int" value="1217"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="36" flip="false" index="23" name="Sum 3" type="sum">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Signs" type="string" value="++"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="960"/>
													<Property name="Top" type="int" value="1153"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="60"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="39" flip="false" index="24" name="Sum 4" type="sum">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Signs" type="string" value="++"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="1138"/>
													<Property name="Top" type="int" value="1087"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="60"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="37" flip="false" index="25" name="Out" type="subpath_output">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="1"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12639424"/>
													<Property name="Left" type="int" value="1330"/>
													<Property name="Top" type="int" value="1104"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
									</Objects>
									<Connections>
										<Connection begin_id="1" begin_idx="0" end_id="2" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="113"/>
													<Segment index="1" value="-35"/>
													<Segment index="2" value="67"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="1" begin_idx="1" end_id="3" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="125"/>
													<Segment index="1" value="79"/>
													<Segment index="2" value="18"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="1" begin_idx="2" end_id="4" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="112"/>
													<Segment index="1" value="83"/>
													<Segment index="2" value="28"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="1" begin_idx="3" end_id="5" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="87"/>
													<Segment index="1" value="82"/>
													<Segment index="2" value="46"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="1" begin_idx="4" end_id="6" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="80"/>
													<Segment index="1" value="81"/>
													<Segment index="2" value="47"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="1" begin_idx="5" end_id="7" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="72"/>
													<Segment index="1" value="73"/>
													<Segment index="2" value="51"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="1" begin_idx="6" end_id="8" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="61"/>
													<Segment index="1" value="75"/>
													<Segment index="2" value="55"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="0" end_id="12" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="49"/>
													<Segment index="1" value="307"/>
													<Segment index="2" value="110"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="10" begin_idx="0" end_id="9" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="63"/>
													<Segment index="1" value="-33"/>
													<Segment index="2" value="23"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="0" begin_idx="0" end_id="1" end_idx="0" type="20">
											<Display type="20">
												<Segments size="3">
													<Segment index="0" value="18"/>
													<Segment index="1" value="-2"/>
													<Segment index="2" value="19"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="3" begin_idx="0" end_id="23" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="409"/>
													<Segment index="1" value="-81"/>
													<Segment index="2" value="60"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="5" begin_idx="0" end_id="23" end_idx="2" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="432"/>
													<Segment index="1" value="-185"/>
													<Segment index="2" value="47"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="4" begin_idx="0" end_id="26" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="437"/>
													<Segment index="1" value="-68"/>
													<Segment index="2" value="40"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="5" begin_idx="0" end_id="26" end_idx="2" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="455"/>
													<Segment index="1" value="-112"/>
													<Segment index="2" value="29"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="26" begin_idx="0" end_id="28" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="34"/>
													<Segment index="1" value="19"/>
													<Segment index="2" value="34"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="23" begin_idx="0" end_id="27" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="28"/>
													<Segment index="1" value="-9"/>
													<Segment index="2" value="38"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="7" begin_idx="0" end_id="27" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="580"/>
													<Segment index="1" value="-298"/>
													<Segment index="2" value="30"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="7" begin_idx="0" end_id="28" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="599"/>
													<Segment index="1" value="-197"/>
													<Segment index="2" value="18"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="27" begin_idx="0" end_id="29" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="22"/>
													<Segment index="1" value="40"/>
													<Segment index="2" value="22"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="28" begin_idx="0" end_id="29" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="18"/>
													<Segment index="1" value="-41"/>
													<Segment index="2" value="19"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="0" end_id="30" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="732"/>
													<Segment index="1" value="99"/>
													<Segment index="2" value="199"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="10" begin_idx="0" end_id="31" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="64"/>
													<Segment index="1" value="-27"/>
													<Segment index="2" value="44"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="2" begin_idx="0" end_id="31" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="88"/>
													<Segment index="1" value="102"/>
													<Segment index="2" value="144"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="31" begin_idx="0" end_id="23" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="72"/>
													<Segment index="1" value="-31"/>
													<Segment index="2" value="73"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="31" begin_idx="0" end_id="26" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="69"/>
													<Segment index="1" value="42"/>
													<Segment index="2" value="81"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="11" begin_idx="0" end_id="12" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="46"/>
													<Segment index="1" value="-19"/>
													<Segment index="2" value="10"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="12" begin_idx="0" end_id="32" end_idx="2" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="132"/>
													<Segment index="1" value="-24"/>
													<Segment index="2" value="94"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="12" begin_idx="0" end_id="33" end_idx="2" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="131"/>
													<Segment index="1" value="45"/>
													<Segment index="2" value="97"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="6" begin_idx="0" end_id="32" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="71"/>
													<Segment index="1" value="-61"/>
													<Segment index="2" value="422"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="6" begin_idx="0" end_id="33" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="72"/>
													<Segment index="1" value="8"/>
													<Segment index="2" value="423"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="4" begin_idx="0" end_id="32" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="234"/>
													<Segment index="1" value="38"/>
													<Segment index="2" value="246"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="3" begin_idx="0" end_id="33" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="346"/>
													<Segment index="1" value="167"/>
													<Segment index="2" value="133"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="33" begin_idx="0" end_id="35" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="37"/>
													<Segment index="1" value="-9"/>
													<Segment index="2" value="34"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="32" begin_idx="0" end_id="34" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="38"/>
													<Segment index="1" value="-19"/>
													<Segment index="2" value="33"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="8" begin_idx="0" end_id="35" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="626"/>
													<Segment index="1" value="-117"/>
													<Segment index="2" value="6"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="8" begin_idx="0" end_id="34" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="615"/>
													<Segment index="1" value="-196"/>
													<Segment index="2" value="15"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="34" begin_idx="0" end_id="36" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="22"/>
													<Segment index="1" value="20"/>
													<Segment index="2" value="23"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="35" begin_idx="0" end_id="36" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="21"/>
													<Segment index="1" value="-39"/>
													<Segment index="2" value="22"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="29" begin_idx="0" end_id="39" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="68"/>
													<Segment index="1" value="74"/>
													<Segment index="2" value="69"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="36" begin_idx="0" end_id="39" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="61"/>
													<Segment index="1" value="-56"/>
													<Segment index="2" value="62"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="39" begin_idx="0" end_id="30" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="16"/>
													<Segment index="1" value="-98"/>
													<Segment index="2" value="16"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="39" begin_idx="0" end_id="37" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="69"/>
													<Segment index="1" value="-3"/>
													<Segment index="2" value="68"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
									</Connections>
								</PATH>
								<PATH ID="12" block_id="64" index="-1" name="Cv_wieksze_rowne_od_1">
									<Paths/>
									<Objects>
										<Object ID="0" flip="false" index="0" name="In" type="subpath_input">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="1"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="7"/>
													<Property name="Top" type="int" value="440"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="1" flip="false" index="1" name="Out" type="subpath_output">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="1"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12639424"/>
													<Property name="Left" type="int" value="1053"/>
													<Property name="Top" type="int" value="578"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="2" flip="false" index="2" name="Demux 1" type="Demux">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Outputs" type="uint" value="10"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="119"/>
													<Property name="Top" type="int" value="105"/>
													<Property name="Width" type="int" value="20"/>
													<Property name="Height" type="int" value="700"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="4" flip="false" index="3" name="dodatnia" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="165"/>
													<Property name="Top" type="int" value="162"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="5" flip="false" index="4" name="ujemna" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="167"/>
													<Property name="Top" type="int" value="223"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="6" flip="false" index="5" name="w_beta" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="167"/>
													<Property name="Top" type="int" value="288"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="7" flip="false" index="6" name="w_alfa_beta" type="gain">
											<Description>w_alfa_beta</Description>
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="166"/>
													<Property name="Top" type="int" value="350"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="8" flip="false" index="7" name="m_m_beta" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="173"/>
													<Property name="Top" type="int" value="410"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="9" flip="false" index="8" name="m_m_alfa_m_beta" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="173"/>
													<Property name="Top" type="int" value="478"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="10" flip="false" index="9" name="Ax+B1" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="168"/>
													<Property name="Top" type="int" value="541"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="11" flip="false" index="10" name="Ax+B2" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="165"/>
													<Property name="Top" type="int" value="596"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="12" flip="false" index="11" name="Ax+B3" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="170"/>
													<Property name="Top" type="int" value="663"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="13" flip="false" index="12" name="Ax+B4" type="gain">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Gain" type="real" value="1"/>
													<Property name="Clear sig. status" type="bool" value="false"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="165"/>
													<Property name="Top" type="int" value="732"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="15"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="14" flip="false" index="13" name="Logic operator 1" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="2"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="438"/>
													<Property name="Top" type="int" value="149"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="15" flip="false" index="14" name="Logic operator 2" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="2"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="439"/>
													<Property name="Top" type="int" value="233"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="16" flip="false" index="15" name="Logic operator 3" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="2"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="439"/>
													<Property name="Top" type="int" value="323"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="17" flip="false" index="16" name="Logic operator 4" type="logical">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Inputs number" type="int" value="2"/>
													<Property name="Type" type="enum" value="0">
														<Enumeration index="0" name="AND"/>
														<Enumeration index="1" name="NAND"/>
														<Enumeration index="2" name="NOR"/>
														<Enumeration index="3" name="NOT"/>
														<Enumeration index="4" name="OR"/>
														<Enumeration index="5" name="XOR"/>
													</Property>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="437"/>
													<Property name="Top" type="int" value="424"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="45"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="18" flip="false" index="17" name="Product 1" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="695"/>
													<Property name="Top" type="int" value="426"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="40"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="19" flip="false" index="18" name="Product 2" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="691"/>
													<Property name="Top" type="int" value="520"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="40"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="20" flip="false" index="19" name="Product 3" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="688"/>
													<Property name="Top" type="int" value="622"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="40"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="21" flip="false" index="20" name="Product 4" type="product">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Actions string" type="string" value="**"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="692"/>
													<Property name="Top" type="int" value="720"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="40"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="23" flip="false" index="21" name="Sum 1" type="sum">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Signs" type="string" value="++++"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="804"/>
													<Property name="Top" type="int" value="387"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="400"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="24" flip="false" index="22" name="Signal limiter 1" type="signal_limiter">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Low limit" type="real" value="0"/>
													<Property name="High limit" type="real" value="0"/>
													<Property name="Rising rate limit" type="real" value="0"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="12632256"/>
													<Property name="Left" type="int" value="954"/>
													<Property name="Top" type="int" value="572"/>
													<Property name="Width" type="int" value="60"/>
													<Property name="Height" type="int" value="30"/>
												</Properties>
											</Display>
										</Object>
										<Object ID="25" flip="false" index="23" name="CV" type="subpath_input">
											<Transform>
												<Properties>
													<Property name="Processing rate" type="int" value="1"/>
													<Property name="Index" type="int" value="1"/>
												</Properties>
											</Transform>
											<Display>
												<Properties>
													<Property name="Color" type="color" value="15780518"/>
													<Property name="Left" type="int" value="24"/>
													<Property name="Top" type="int" value="89"/>
													<Property name="Width" type="int" value="30"/>
													<Property name="Height" type="int" value="20"/>
												</Properties>
											</Display>
										</Object>
									</Objects>
									<Connections>
										<Connection begin_id="0" begin_idx="0" end_id="2" end_idx="0" type="20">
											<Display type="20">
												<Segments size="3">
													<Segment index="0" value="65"/>
													<Segment index="1" value="5"/>
													<Segment index="2" value="22"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="4" begin_idx="0" end_id="14" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="109"/>
													<Segment index="1" value="-5"/>
													<Segment index="2" value="109"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="7" begin_idx="0" end_id="14" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="118"/>
													<Segment index="1" value="-178"/>
													<Segment index="2" value="99"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="14" begin_idx="0" end_id="18" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="117"/>
													<Segment index="1" value="268"/>
													<Segment index="2" value="85"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="10" begin_idx="0" end_id="18" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="388"/>
													<Segment index="1" value="-96"/>
													<Segment index="2" value="84"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="5" begin_idx="0" end_id="15" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="108"/>
													<Segment index="1" value="18"/>
													<Segment index="2" value="109"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="6" begin_idx="0" end_id="15" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="108"/>
													<Segment index="1" value="-32"/>
													<Segment index="2" value="109"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="15" begin_idx="0" end_id="19" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="102"/>
													<Segment index="1" value="278"/>
													<Segment index="2" value="95"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="11" begin_idx="0" end_id="19" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="239"/>
													<Segment index="1" value="-57"/>
													<Segment index="2" value="232"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="9" begin_idx="0" end_id="16" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="117"/>
													<Segment index="1" value="-147"/>
													<Segment index="2" value="94"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="5" begin_idx="0" end_id="16" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="108"/>
													<Segment index="1" value="123"/>
													<Segment index="2" value="109"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="16" begin_idx="0" end_id="20" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="101"/>
													<Segment index="1" value="290"/>
													<Segment index="2" value="93"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="12" begin_idx="0" end_id="20" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="231"/>
													<Segment index="1" value="-22"/>
													<Segment index="2" value="232"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="8" begin_idx="0" end_id="17" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="104"/>
													<Segment index="1" value="22"/>
													<Segment index="2" value="105"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="4" begin_idx="0" end_id="17" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="108"/>
													<Segment index="1" value="285"/>
													<Segment index="2" value="109"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="17" begin_idx="0" end_id="21" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="105"/>
													<Segment index="1" value="287"/>
													<Segment index="2" value="95"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="13" begin_idx="0" end_id="21" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="241"/>
													<Segment index="1" value="7"/>
													<Segment index="2" value="231"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="18" begin_idx="0" end_id="23" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="27"/>
													<Segment index="1" value="21"/>
													<Segment index="2" value="27"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="19" begin_idx="0" end_id="23" end_idx="1" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="29"/>
													<Segment index="1" value="7"/>
													<Segment index="2" value="29"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="20" begin_idx="0" end_id="23" end_idx="2" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="30"/>
													<Segment index="1" value="-15"/>
													<Segment index="2" value="31"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="21" begin_idx="0" end_id="23" end_idx="3" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="28"/>
													<Segment index="1" value="-33"/>
													<Segment index="2" value="29"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="23" begin_idx="0" end_id="24" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="47"/>
													<Segment index="1" value="0"/>
													<Segment index="2" value="48"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
										<Connection begin_id="24" begin_idx="0" end_id="1" end_idx="0" type="1">
											<Display type="1">
												<Segments size="3">
													<Segment index="0" value="22"/>
													<Segment index="1" value="1"/>
													<Segment index="2" value="22"/>
												</Segments>
											</Display>
											<Properties/>
										</Connection>
									</Connections>
								</PATH>
							</Paths>
							<Objects>
								<Object ID="0" flip="false" index="0" name="In" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="34"/>
											<Property name="Top" type="int" value="305"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="1" flip="false" index="1" name="Demux 1" type="Demux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Outputs" type="uint" value="10"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="176"/>
											<Property name="Top" type="int" value="66"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="500"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="2" flip="false" index="2" name="dodatnia" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="245"/>
											<Property name="Top" type="int" value="95"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="3" flip="false" index="3" name="ujemna" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="245"/>
											<Property name="Top" type="int" value="135"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="4" flip="false" index="4" name="CVbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="249"/>
											<Property name="Top" type="int" value="182"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="5" flip="false" index="5" name="Cv>alfaplusbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="243"/>
											<Property name="Top" type="int" value="232"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="6" flip="false" index="6" name="CV" type="subpath_input">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="2"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="97"/>
											<Property name="Top" type="int" value="773"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="7" flip="false" index="7" name="CVbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="241"/>
											<Property name="Top" type="int" value="280"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="8" flip="false" index="8" name="CValfaplusbeta" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="243"/>
											<Property name="Top" type="int" value="326"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="9" flip="false" index="9" name="Ax+B1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="242"/>
											<Property name="Top" type="int" value="376"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="10" flip="false" index="10" name="Ax+B2" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="240"/>
											<Property name="Top" type="int" value="429"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="11" flip="false" index="11" name="Ax+B4" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="233"/>
											<Property name="Top" type="int" value="530"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="12" flip="false" index="12" name="Ax+B3" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="237"/>
											<Property name="Top" type="int" value="480"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="15"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="27" flip="false" index="13" name="out" type="subpath_output">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Index" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1606"/>
											<Property name="Top" type="int" value="353"/>
											<Property name="Width" type="int" value="30"/>
											<Property name="Height" type="int" value="20"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="53" flip="false" index="14" name="Constant 3" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="1"/>
											<Property name="Min" type="real" value="-1"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="268"/>
											<Property name="Top" type="int" value="661"/>
											<Property name="Width" type="int" value="50"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="15" flip="false" index="15" name="Relational operator 1" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="3">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="345"/>
											<Property name="Top" type="int" value="610"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="16" flip="false" index="16" name="Relational operator 2" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="370"/>
											<Property name="Top" type="int" value="721"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="17" flip="false" index="17" name="Constant 1" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="-1"/>
											<Property name="Min" type="real" value="-1"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="263"/>
											<Property name="Top" type="int" value="746"/>
											<Property name="Width" type="int" value="50"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="18" flip="false" index="18" name="Logic operator 1" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="515"/>
											<Property name="Top" type="int" value="481"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="19" flip="false" index="19" name="Logic operator 2" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="516"/>
											<Property name="Top" type="int" value="549"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="20" flip="false" index="20" name="Product 1" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="635"/>
											<Property name="Top" type="int" value="402"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="21" flip="false" index="21" name="XY Display 1" type="xy_display">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer length" type="uint" value="1000"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1369"/>
											<Property name="Top" type="int" value="235"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="66"/>
											<Property name="X axis min" type="real" value="-1.144"/>
											<Property name="X axis max" type="real" value="1.144"/>
											<Property name="Y axis min" type="real" value="-1.0424412111933"/>
											<Property name="Y axis max" type="real" value="1.26660278891166"/>
											<Property name="Chart bkg color" type="color" value="16777215"/>
											<Property name="Chart axes color" type="color" value="12632256"/>
											<Property name="Chart plot color" type="color" value="0"/>
											<Property name="Chart bad sample color" type="color" value="255"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="25" flip="false" index="22" name="Product 2" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="641"/>
											<Property name="Top" type="int" value="460"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="26" flip="false" index="23" name="Sum 1" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="908"/>
											<Property name="Top" type="int" value="533"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="24" flip="false" index="24" name="Logic operator 3" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="522"/>
											<Property name="Top" type="int" value="733"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="28" flip="false" index="25" name="Logic operator 4" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="513"/>
											<Property name="Top" type="int" value="636"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="32" flip="false" index="26" name="Logic operator 5" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="723"/>
											<Property name="Top" type="int" value="73"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="33" flip="false" index="27" name="MinMax Selector 1" type="min_max_select">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs count" type="uint" value="2"/>
											<Property name="Function" type="enum" value="0">
												<Enumeration index="0" name="Max"/>
												<Enumeration index="1" name="Min"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="259"/>
											<Property name="Top" type="int" value="830"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="52"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="34" flip="true" index="28" name="One step delay 1" type="one_step_del">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Init value" type="real" value="0"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="257"/>
											<Property name="Top" type="int" value="927"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="35" flip="false" index="29" name="Relational operator 3" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="2">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="376"/>
											<Property name="Top" type="int" value="833"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="36" flip="false" index="30" name="Constant 4" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="1"/>
											<Property name="Min" type="real" value="-1"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="341"/>
											<Property name="Top" type="int" value="937"/>
											<Property name="Width" type="int" value="50"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="37" flip="false" index="31" name="max_wiekszy_od_1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="529"/>
											<Property name="Top" type="int" value="167"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="38" flip="false" index="32" name="MinMax Selector 2" type="min_max_select">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs count" type="uint" value="2"/>
											<Property name="Function" type="enum" value="1">
												<Enumeration index="0" name="Max"/>
												<Enumeration index="1" name="Min"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="256"/>
											<Property name="Top" type="int" value="1014"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="52"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="39" flip="true" index="33" name="One step delay 2" type="one_step_del">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Init value" type="real" value="0"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="253"/>
											<Property name="Top" type="int" value="1124"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="40" flip="false" index="34" name="Relational operator 4" type="relational">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Type" type="enum" value="4">
												<Enumeration index="0" name="EQUAL"/>
												<Enumeration index="1" name="GREATER"/>
												<Enumeration index="2" name="GREATER or EQUAL"/>
												<Enumeration index="3" name="LESS"/>
												<Enumeration index="4" name="LESS or EQUAL"/>
												<Enumeration index="5" name="NOT EQUAL"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="451"/>
											<Property name="Top" type="int" value="1043"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="41" flip="false" index="35" name="Constant 5" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="-1"/>
											<Property name="Min" type="real" value="-1"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="344"/>
											<Property name="Top" type="int" value="1204"/>
											<Property name="Width" type="int" value="50"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="42" flip="false" index="36" name="Logic operator 6" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="722"/>
											<Property name="Top" type="int" value="169"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="43" flip="false" index="37" name="min_wiekszy_od_minus1" type="gain">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Gain" type="real" value="1"/>
											<Property name="Clear sig. status" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="525"/>
											<Property name="Top" type="int" value="279"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="44" flip="false" index="38" name="Product 5" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="851"/>
											<Property name="Top" type="int" value="107"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="45" flip="false" index="39" name="Product 6" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="858"/>
											<Property name="Top" type="int" value="198"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="40"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="46" flip="false" index="40" name="Sum 2" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="945"/>
											<Property name="Top" type="int" value="144"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="49" buffer_type="1" flip="false" index="41" name="Internal variable 2" type="internal_out_var">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Buffer size" type="int" value="1"/>
											<Property name="Buffer name" type="string" value="Buffer 1 2"/>
											<Property name="Auto commit" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12639424"/>
											<Property name="Left" type="int" value="1421"/>
											<Property name="Top" type="int" value="433"/>
											<Property name="Width" type="int" value="100"/>
											<Property name="Height" type="int" value="37"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="50" flip="false" index="42" name="Logic operator 7" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="530"/>
											<Property name="Top" type="int" value="834"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="51" flip="false" index="43" name="Logic operator 8" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="1"/>
											<Property name="Type" type="enum" value="1">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="543"/>
											<Property name="Top" type="int" value="971"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="55" flip="false" index="44" name="Logic operator 9" type="logical">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs number" type="int" value="3"/>
											<Property name="Type" type="enum" value="0">
												<Enumeration index="0" name="AND"/>
												<Enumeration index="1" name="NAND"/>
												<Enumeration index="2" name="NOR"/>
												<Enumeration index="3" name="NOT"/>
												<Enumeration index="4" name="OR"/>
												<Enumeration index="5" name="XOR"/>
											</Property>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="673"/>
											<Property name="Top" type="int" value="289"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="57" flip="false" index="45" name="Constant 2" type="constant">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Value" type="real" value="0"/>
											<Property name="Min" type="real" value="-1"/>
											<Property name="Max" type="real" value="1"/>
											<Property name="Limit output" type="bool" value="false"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="15780518"/>
											<Property name="Left" type="int" value="918"/>
											<Property name="Top" type="int" value="333"/>
											<Property name="Width" type="int" value="50"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="60" flip="false" index="46" name="Sum 3" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="1195"/>
											<Property name="Top" type="int" value="331"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="48" flip="false" index="47" name="Product 3" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="609"/>
											<Property name="Top" type="int" value="589"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="54" flip="false" index="48" name="Product 4" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="623"/>
											<Property name="Top" type="int" value="691"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="58" flip="false" index="49" name="Sum 5" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="719"/>
											<Property name="Top" type="int" value="420"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="56" flip="false" index="50" name="Product 7" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="815"/>
											<Property name="Top" type="int" value="440"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="59" flip="false" index="51" name="Sum 4" type="sum">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Signs" type="string" value="++"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="707"/>
											<Property name="Top" type="int" value="626"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="60"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="61" flip="false" index="52" name="Product 8" type="product">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Actions string" type="string" value="**"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="829"/>
											<Property name="Top" type="int" value="646"/>
											<Property name="Width" type="int" value="60"/>
											<Property name="Height" type="int" value="30"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="62" flip="false" index="53" name="CV_mniejsze_od1" type="subpath">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="348"/>
											<Property name="Top" type="int" value="1325"/>
											<Property name="Width" type="int" value="200"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="63" flip="false" index="54" name="Mux 1" type="Mux">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
											<Property name="Inputs" type="uint" value="7"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="257"/>
											<Property name="Top" type="int" value="1282"/>
											<Property name="Width" type="int" value="20"/>
											<Property name="Height" type="int" value="135"/>
										</Properties>
									</Display>
								</Object>
								<Object ID="64" flip="false" index="55" name="Cv_wieksze_rowne_od_1" type="subpath">
									<Transform>
										<Properties>
											<Property name="Processing rate" type="int" value="1"/>
										</Properties>
									</Transform>
									<Display>
										<Properties>
											<Property name="Color" type="color" value="12632256"/>
											<Property name="Left" type="int" value="356"/>
											<Property name="Top" type="int" value="1451"/>
											<Property name="Width" type="int" value="200"/>
											<Property name="Height" type="int" value="45"/>
										</Properties>
									</Display>
								</Object>
							</Objects>
							<Connections>
								<Connection begin_id="1" begin_idx="0" end_id="2" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="-6"/>
											<Segment index="1" value="-9"/>
											<Segment index="2" value="60"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="1" end_id="3" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="-5"/>
											<Segment index="1" value="-14"/>
											<Segment index="2" value="59"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="2" end_id="4" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="-2"/>
											<Segment index="1" value="-12"/>
											<Segment index="2" value="60"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="4" end_id="7" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="-1"/>
											<Segment index="1" value="-4"/>
											<Segment index="2" value="51"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="5" end_id="8" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="2"/>
											<Segment index="1" value="-3"/>
											<Segment index="2" value="50"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="6" end_id="9" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="2"/>
											<Segment index="1" value="2"/>
											<Segment index="2" value="49"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="7" end_id="10" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="1"/>
											<Segment index="1" value="10"/>
											<Segment index="2" value="48"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="8" end_id="12" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="21"/>
											<Segment index="1" value="16"/>
											<Segment index="2" value="25"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="9" end_id="11" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="4"/>
											<Segment index="1" value="21"/>
											<Segment index="2" value="38"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="0" begin_idx="0" end_id="1" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="93"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="24"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="15" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="100"/>
											<Segment index="1" value="-158"/>
											<Segment index="2" value="123"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="16" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="115"/>
											<Segment index="1" value="-47"/>
											<Segment index="2" value="133"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="53" begin_idx="0" end_id="15" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="16"/>
											<Segment index="1" value="-36"/>
											<Segment index="2" value="16"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="17" begin_idx="0" end_id="16" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="49"/>
											<Segment index="1" value="-10"/>
											<Segment index="2" value="13"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="18" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="148"/>
											<Segment index="1" value="390"/>
											<Segment index="2" value="67"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="19" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="80"/>
											<Segment index="1" value="418"/>
											<Segment index="2" value="136"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="1" begin_idx="3" end_id="5" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="21"/>
											<Segment index="1" value="-7"/>
											<Segment index="2" value="31"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="19" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="110"/>
											<Segment index="1" value="332"/>
											<Segment index="2" value="108"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="15" begin_idx="0" end_id="19" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="42"/>
											<Segment index="1" value="-50"/>
											<Segment index="2" value="74"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="19" begin_idx="0" end_id="20" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="19"/>
											<Segment index="1" value="-149"/>
											<Segment index="2" value="45"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="20" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="174"/>
											<Segment index="1" value="29"/>
											<Segment index="2" value="164"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="18" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="142"/>
											<Segment index="1" value="264"/>
											<Segment index="2" value="75"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="21" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="659"/>
											<Segment index="1" value="-526"/>
											<Segment index="2" value="588"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="18" begin_idx="0" end_id="25" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="45"/>
											<Segment index="1" value="-23"/>
											<Segment index="2" value="26"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="25" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="281"/>
											<Segment index="1" value="87"/>
											<Segment index="2" value="63"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="28" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="134"/>
											<Segment index="1" value="314"/>
											<Segment index="2" value="81"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="24" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="185"/>
											<Segment index="1" value="411"/>
											<Segment index="2" value="39"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="16" begin_idx="0" end_id="28" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="48"/>
											<Segment index="1" value="-85"/>
											<Segment index="2" value="40"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="16" begin_idx="0" end_id="24" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="50"/>
											<Segment index="1" value="12"/>
											<Segment index="2" value="47"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="28" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="124"/>
											<Segment index="1" value="527"/>
											<Segment index="2" value="89"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="24" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="165"/>
											<Segment index="1" value="664"/>
											<Segment index="2" value="57"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="32" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="195"/>
											<Segment index="1" value="-18"/>
											<Segment index="2" value="228"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="32" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="156"/>
											<Segment index="1" value="-144"/>
											<Segment index="2" value="269"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="33" begin_idx="0" end_id="34" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="6"/>
											<Segment index="1" value="102"/>
											<Segment index="2" value="-8"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="34" begin_idx="0" end_id="33" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="-13"/>
											<Segment index="1" value="-85"/>
											<Segment index="2" value="15"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="33" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="81"/>
											<Segment index="1" value="64"/>
											<Segment index="2" value="56"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="33" begin_idx="0" end_id="35" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="31"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="31"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="36" begin_idx="0" end_id="35" end_idx="1" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="16"/>
											<Segment index="1" value="-51"/>
											<Segment index="2" value="-35"/>
											<Segment index="3" value="-38"/>
											<Segment index="4" value="9"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="35" begin_idx="0" end_id="37" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="50"/>
											<Segment index="1" value="-673"/>
											<Segment index="2" value="48"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="37" begin_idx="0" end_id="32" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="61"/>
											<Segment index="1" value="-76"/>
											<Segment index="2" value="78"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="38" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="63"/>
											<Segment index="1" value="248"/>
											<Segment index="2" value="71"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="38" begin_idx="0" end_id="39" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="10"/>
											<Segment index="1" value="115"/>
											<Segment index="2" value="-13"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="39" begin_idx="0" end_id="38" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="-10"/>
											<Segment index="1" value="-98"/>
											<Segment index="2" value="13"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="38" begin_idx="0" end_id="40" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="29"/>
											<Segment index="1" value="27"/>
											<Segment index="2" value="111"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="41" begin_idx="0" end_id="40" end_idx="1" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="33"/>
											<Segment index="1" value="-131"/>
											<Segment index="2" value="-11"/>
											<Segment index="3" value="-15"/>
											<Segment index="4" value="40"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="40" begin_idx="0" end_id="43" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="-9"/>
											<Segment index="1" value="-771"/>
											<Segment index="2" value="28"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="42" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="365"/>
											<Segment index="1" value="38"/>
											<Segment index="2" value="57"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="37" begin_idx="0" end_id="42" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="62"/>
											<Segment index="1" value="9"/>
											<Segment index="2" value="76"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="4" begin_idx="0" end_id="42" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="204"/>
											<Segment index="1" value="13"/>
											<Segment index="2" value="214"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="42" begin_idx="0" end_id="45" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="12"/>
											<Segment index="1" value="20"/>
											<Segment index="2" value="69"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="32" begin_idx="0" end_id="44" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="19"/>
											<Segment index="1" value="25"/>
											<Segment index="2" value="54"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="44" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="343"/>
											<Segment index="1" value="-250"/>
											<Segment index="2" value="211"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="10" begin_idx="0" end_id="45" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="326"/>
											<Segment index="1" value="-212"/>
											<Segment index="2" value="237"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="44" begin_idx="0" end_id="46" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="20"/>
											<Segment index="1" value="37"/>
											<Segment index="2" value="19"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="45" begin_idx="0" end_id="46" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="10"/>
											<Segment index="1" value="-34"/>
											<Segment index="2" value="22"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="35" begin_idx="0" end_id="50" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="55"/>
											<Segment index="1" value="1"/>
											<Segment index="2" value="44"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="40" begin_idx="0" end_id="51" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="2"/>
											<Segment index="1" value="-72"/>
											<Segment index="2" value="35"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="60" begin_idx="0" end_id="21" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="92"/>
											<Segment index="1" value="-82"/>
											<Segment index="2" value="27"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="60" begin_idx="0" end_id="49" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="89"/>
											<Segment index="1" value="90"/>
											<Segment index="2" value="82"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="15" begin_idx="0" end_id="18" end_idx="2" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="59"/>
											<Segment index="1" value="-118"/>
											<Segment index="2" value="56"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="28" begin_idx="0" end_id="48" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="25"/>
											<Segment index="1" value="-59"/>
											<Segment index="2" value="16"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="24" begin_idx="0" end_id="54" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="21"/>
											<Segment index="1" value="-54"/>
											<Segment index="2" value="25"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="48" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="167"/>
											<Segment index="1" value="122"/>
											<Segment index="2" value="150"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="20" begin_idx="0" end_id="58" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="17"/>
											<Segment index="1" value="23"/>
											<Segment index="2" value="12"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="25" begin_idx="0" end_id="58" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="17"/>
											<Segment index="1" value="-15"/>
											<Segment index="2" value="6"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="58" begin_idx="0" end_id="56" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="19"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="22"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="50" begin_idx="0" end_id="56" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="211"/>
											<Segment index="1" value="-396"/>
											<Segment index="2" value="19"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="56" begin_idx="0" end_id="26" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="22"/>
											<Segment index="1" value="98"/>
											<Segment index="2" value="16"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="48" begin_idx="0" end_id="59" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="30"/>
											<Segment index="1" value="42"/>
											<Segment index="2" value="13"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="54" begin_idx="0" end_id="59" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="17"/>
											<Segment index="1" value="-40"/>
											<Segment index="2" value="12"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="59" begin_idx="0" end_id="61" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="21"/>
											<Segment index="1" value="0"/>
											<Segment index="2" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="51" begin_idx="0" end_id="61" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="170"/>
											<Segment index="1" value="-327"/>
											<Segment index="2" value="61"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="61" begin_idx="0" end_id="26" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="5"/>
											<Segment index="1" value="-88"/>
											<Segment index="2" value="19"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="46" begin_idx="0" end_id="60" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="97"/>
											<Segment index="1" value="177"/>
											<Segment index="2" value="98"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="26" begin_idx="0" end_id="60" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="133"/>
											<Segment index="1" value="-192"/>
											<Segment index="2" value="99"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="54" end_idx="1" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="173"/>
											<Segment index="1" value="224"/>
											<Segment index="2" value="158"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="60" begin_idx="0" end_id="27" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="170"/>
											<Segment index="1" value="2"/>
											<Segment index="2" value="186"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="6" begin_idx="0" end_id="63" end_idx="0" type="1">
									<Display type="1">
										<Segments size="3">
											<Segment index="0" value="44"/>
											<Segment index="1" value="515"/>
											<Segment index="2" value="91"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="2" begin_idx="0" end_id="63" end_idx="1" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="23"/>
											<Segment index="1" value="598"/>
											<Segment index="2" value="-112"/>
											<Segment index="3" value="614"/>
											<Segment index="4" value="46"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="3" begin_idx="0" end_id="63" end_idx="2" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="10"/>
											<Segment index="1" value="586"/>
											<Segment index="2" value="-110"/>
											<Segment index="3" value="602"/>
											<Segment index="4" value="57"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="5" begin_idx="0" end_id="63" end_idx="3" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="19"/>
											<Segment index="1" value="546"/>
											<Segment index="2" value="-132"/>
											<Segment index="3" value="561"/>
											<Segment index="4" value="72"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="8" begin_idx="0" end_id="63" end_idx="4" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="10"/>
											<Segment index="1" value="512"/>
											<Segment index="2" value="-82"/>
											<Segment index="3" value="517"/>
											<Segment index="4" value="31"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="9" begin_idx="0" end_id="63" end_idx="5" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="10"/>
											<Segment index="1" value="493"/>
											<Segment index="2" value="-67"/>
											<Segment index="3" value="502"/>
											<Segment index="4" value="17"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="12" begin_idx="0" end_id="63" end_idx="6" type="1">
									<Display type="1">
										<Segments size="5">
											<Segment index="0" value="10"/>
											<Segment index="1" value="449"/>
											<Segment index="2" value="-69"/>
											<Segment index="3" value="458"/>
											<Segment index="4" value="24"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
								<Connection begin_id="63" begin_idx="0" end_id="62" end_idx="0" type="20">
									<Display type="20">
										<Segments size="3">
											<Segment index="0" value="37"/>
											<Segment index="1" value="-2"/>
											<Segment index="2" value="39"/>
										</Segments>
									</Display>
									<Properties/>
								</Connection>
							</Connections>
						</PATH>
					</Paths>
					<Objects>
						<Object ID="0" flip="false" index="0" name="CV" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="19"/>
									<Property name="Top" type="int" value="50"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="alfa" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="2"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="22"/>
									<Property name="Top" type="int" value="149"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="2" flip="false" index="2" name="beta" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="3"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="26"/>
									<Property name="Top" type="int" value="241"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="56" flip="false" index="3" name="Krzywa" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="164"/>
									<Property name="Top" type="int" value="280"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="90"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="43" flip="false" index="4" name="Pochodna" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="163"/>
									<Property name="Top" type="int" value="71"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="60"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="44" flip="false" index="5" name="Przedzialy" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="162"/>
									<Property name="Top" type="int" value="164"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="90"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="34" flip="false" index="6" name="backupLogikaDlaCV1" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="573"/>
									<Property name="Top" type="int" value="108"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="36" flip="false" index="7" name="Mux 1" type="Mux">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Inputs" type="uint" value="10"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="465"/>
									<Property name="Top" type="int" value="71"/>
									<Property name="Width" type="int" value="20"/>
									<Property name="Height" type="int" value="350"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="13" flip="false" index="8" name="Logika" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="584"/>
									<Property name="Top" type="int" value="305"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="60"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="15" flip="false" index="9" name="out2" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="910"/>
									<Property name="Top" type="int" value="293"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="10" flip="false" index="10" name="Display 1" type="display">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Buffer length" type="uint" value="1900"/>
									<Property name="Inputs count" type="uint" value="6"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="482"/>
									<Property name="Top" type="int" value="491"/>
									<Property name="Width" type="int" value="170"/>
									<Property name="Height" type="int" value="120"/>
									<Property name="Chart min" type="real" value="-2.2"/>
									<Property name="Chart max" type="real" value="2.2"/>
									<Property name="Autoscale" type="bool" value="false"/>
									<Property name="Scale each sep." type="bool" value="false"/>
									<Property name="Precision" type="int" value="3"/>
									<Property name="Digits" type="int" value="3"/>
									<Property name="Type" type="enum" value="1">
										<Enumeration index="0" name="Fixed"/>
										<Enumeration index="1" name="General"/>
										<Enumeration index="2" name="Scientific"/>
									</Property>
									<Property name="Bkg color" type="color" value="0"/>
									<Property name="Axes color" type="color" value="8421504"/>
									<Property name="Bad sample color" type="color" value="255"/>
									<Property name="Show sample nr" type="bool" value="false"/>
									<Property name="Color scheme" type="enum" value="0">
										<Enumeration index="0" name="dark"/>
										<Enumeration index="1" name="light"/>
									</Property>
								</Properties>
							</Display>
						</Object>
						<Object ID="11" flip="false" index="11" name="Logika" type="subpath">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="585"/>
									<Property name="Top" type="int" value="185"/>
									<Property name="Width" type="int" value="200"/>
									<Property name="Height" type="int" value="60"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="0" begin_idx="0" end_id="43" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="89"/>
									<Segment index="1" value="41"/>
									<Segment index="2" value="30"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="44" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="101"/>
									<Segment index="1" value="126"/>
									<Segment index="2" value="17"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="56" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="59"/>
									<Segment index="1" value="242"/>
									<Segment index="2" value="61"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="44" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="75"/>
									<Segment index="1" value="49"/>
									<Segment index="2" value="40"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="1" begin_idx="0" end_id="56" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="41"/>
									<Segment index="1" value="165"/>
									<Segment index="2" value="76"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="44" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="71"/>
									<Segment index="1" value="-21"/>
									<Segment index="2" value="40"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="2" begin_idx="0" end_id="56" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="69"/>
									<Segment index="1" value="95"/>
									<Segment index="2" value="44"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="43" begin_idx="0" end_id="36" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="42"/>
									<Segment index="1" value="11"/>
									<Segment index="2" value="65"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="43" begin_idx="1" end_id="36" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="42"/>
									<Segment index="1" value="22"/>
									<Segment index="2" value="65"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="0" end_id="36" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="51"/>
									<Segment index="1" value="-18"/>
									<Segment index="2" value="57"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="1" end_id="36" end_idx="3" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="51"/>
									<Segment index="1" value="-5"/>
									<Segment index="2" value="57"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="2" end_id="36" end_idx="4" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="51"/>
									<Segment index="1" value="8"/>
									<Segment index="2" value="57"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="44" begin_idx="3" end_id="36" end_idx="5" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="51"/>
									<Segment index="1" value="21"/>
									<Segment index="2" value="57"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="0" end_id="36" end_idx="6" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="-10"/>
									<Segment index="2" value="50"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="1" end_id="36" end_idx="7" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="50"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="2" end_id="36" end_idx="8" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="16"/>
									<Segment index="2" value="50"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="3" end_id="36" end_idx="9" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="56"/>
									<Segment index="1" value="29"/>
									<Segment index="2" value="50"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="13" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="498"/>
									<Segment index="1" value="285"/>
									<Segment index="2" value="42"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="36" begin_idx="0" end_id="13" end_idx="0" type="20">
							<Display type="20">
								<Segments size="3">
									<Segment index="0" value="47"/>
									<Segment index="1" value="79"/>
									<Segment index="2" value="57"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="13" begin_idx="0" end_id="10" end_idx="1" type="1">
							<Display type="1">
								<Segments size="5">
									<Segment index="0" value="24"/>
									<Segment index="1" value="112"/>
									<Segment index="2" value="-365"/>
									<Segment index="3" value="78"/>
									<Segment index="4" value="44"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="10" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="35"/>
									<Segment index="1" value="449"/>
									<Segment index="2" value="403"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="0" end_id="10" end_idx="2" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="26"/>
									<Segment index="1" value="244"/>
									<Segment index="2" value="97"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="1" end_id="10" end_idx="3" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="37"/>
									<Segment index="1" value="243"/>
									<Segment index="2" value="86"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="2" end_id="10" end_idx="4" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="59"/>
									<Segment index="1" value="242"/>
									<Segment index="2" value="64"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="56" begin_idx="3" end_id="10" end_idx="5" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="51"/>
									<Segment index="1" value="241"/>
									<Segment index="2" value="72"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="36" begin_idx="0" end_id="11" end_idx="0" type="20">
							<Display type="20">
								<Segments size="3">
									<Segment index="0" value="43"/>
									<Segment index="1" value="-41"/>
									<Segment index="2" value="62"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="11" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="499"/>
									<Segment index="1" value="165"/>
									<Segment index="2" value="42"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="11" begin_idx="0" end_id="15" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="64"/>
									<Segment index="1" value="88"/>
									<Segment index="2" value="66"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
				<PATH ID="10" block_id="13" index="-1" name="madel zaklocenia  Gz">
					<Paths/>
					<Objects>
						<Object ID="0" flip="false" index="0" name="" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="34"/>
									<Property name="Top" type="int" value="191"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="637"/>
									<Property name="Top" type="int" value="175"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="3" flip="false" index="2" name="1st order inertia 1" type="first_order_inertia">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="K" type="real" value="1"/>
									<Property name="T" type="real" value="100"/>
									<Property name="Y0[k-1]" type="real" value="0"/>
									<Property name="U0[k-1]" type="real" value="0"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="342"/>
									<Property name="Top" type="int" value="182"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="5" flip="false" index="3" name="1st order integrator 1" type="first_order_integrator">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="K" type="real" value="0.01"/>
									<Property name="T" type="real" value="1"/>
									<Property name="Y0[k-1]" type="real" value="0"/>
									<Property name="U0[k-1]" type="real" value="0"/>
									<Property name="Low limit" type="real" value="0"/>
									<Property name="High limit" type="real" value="0"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="229"/>
									<Property name="Top" type="int" value="182"/>
									<Property name="Width" type="int" value="60"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="0" begin_idx="0" end_id="5" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="85"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="85"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="5" begin_idx="0" end_id="3" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="29"/>
									<Segment index="1" value="0"/>
									<Segment index="2" value="29"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="3" begin_idx="0" end_id="1" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="90"/>
									<Segment index="1" value="-19"/>
									<Segment index="2" value="90"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
				<PATH ID="6" block_id="15" index="-1" name="Model Procesu  Gp">
					<Paths/>
					<Objects>
						<Object ID="0" flip="false" index="0" name="" type="subpath_input">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="116"/>
									<Property name="Top" type="int" value="240"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="1" flip="false" index="1" name="" type="subpath_output">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Index" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12639424"/>
									<Property name="Left" type="int" value="1141"/>
									<Property name="Top" type="int" value="232"/>
									<Property name="Width" type="int" value="30"/>
									<Property name="Height" type="int" value="20"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="2" flip="false" index="2" name="2nd order inertia 1" type="second_order_inertia">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="K1" type="real" value="2"/>
									<Property name="K2" type="real" value="10"/>
									<Property name="T1" type="real" value="2"/>
									<Property name="T2" type="real" value="10"/>
									<Property name="Y0[k-1]" type="real" value="0"/>
									<Property name="Y0[k-2]" type="real" value="0"/>
									<Property name="U0[k-1]" type="real" value="0"/>
									<Property name="U0[k-2]" type="real" value="0"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="494"/>
									<Property name="Top" type="int" value="222"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="4" flip="false" index="3" name="Transfer function (cont.) 1" type="cld_transfer_function">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Nominator order" type="uint" value="1"/>
									<Property name="Denominator order" type="uint" value="2"/>
									<Property name="Nominator" size="1" type="double_vect">
										<Row index="0" value="1"/>
									</Property>
									<Property name="Denominator" size="2" type="double_vect">
										<Row index="0" value="1"/>
										<Row index="1" value="-25"/>
									</Property>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="686"/>
									<Property name="Top" type="int" value="225"/>
									<Property name="Width" type="int" value="120"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="5" flip="false" index="4" name="N-step delay 1" type="integer_delay">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="12632256"/>
									<Property name="Left" type="int" value="316"/>
									<Property name="Top" type="int" value="230"/>
									<Property name="Width" type="int" value="100"/>
									<Property name="Height" type="int" value="45"/>
								</Properties>
							</Display>
						</Object>
						<Object ID="6" flip="false" index="5" name="Constant 1" type="constant">
							<Transform>
								<Properties>
									<Property name="Processing rate" type="int" value="1"/>
									<Property name="Value" type="real" value="40"/>
									<Property name="Min" type="real" value="0"/>
									<Property name="Max" type="real" value="1"/>
									<Property name="Limit output" type="bool" value="false"/>
								</Properties>
							</Transform>
							<Display>
								<Properties>
									<Property name="Color" type="color" value="15780518"/>
									<Property name="Left" type="int" value="149"/>
									<Property name="Top" type="int" value="376"/>
									<Property name="Width" type="int" value="100"/>
									<Property name="Height" type="int" value="30"/>
								</Properties>
							</Display>
						</Object>
					</Objects>
					<Connections>
						<Connection begin_id="2" begin_idx="0" end_id="4" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="60"/>
									<Segment index="1" value="3"/>
									<Segment index="2" value="17"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="4" begin_idx="0" end_id="1" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="193"/>
									<Segment index="1" value="-5"/>
									<Segment index="2" value="147"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="6" begin_idx="0" end_id="5" end_idx="1" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="19"/>
									<Segment index="1" value="-131"/>
									<Segment index="2" value="53"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="0" begin_idx="0" end_id="5" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="71"/>
									<Segment index="1" value="-5"/>
									<Segment index="2" value="104"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
						<Connection begin_id="5" begin_idx="0" end_id="2" end_idx="0" type="1">
							<Display type="1">
								<Segments size="3">
									<Segment index="0" value="54"/>
									<Segment index="1" value="-8"/>
									<Segment index="2" value="29"/>
								</Segments>
							</Display>
							<Properties/>
						</Connection>
					</Connections>
				</PATH>
			</Paths>
			<Objects>
				<Object ID="1" flip="false" index="0" name="alfa" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0.1"/>
							<Property name="Min" type="real" value="0"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="107"/>
							<Property name="Top" type="int" value="411"/>
							<Property name="Width" type="int" value="50"/>
							<Property name="Height" type="int" value="20"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="2" flip="false" index="1" name="beta" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0.2"/>
							<Property name="Min" type="real" value="0"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="103"/>
							<Property name="Top" type="int" value="468"/>
							<Property name="Width" type="int" value="50"/>
							<Property name="Height" type="int" value="20"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="7" flip="false" index="2" name="ElNL" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="65535"/>
							<Property name="Left" type="int" value="331"/>
							<Property name="Top" type="int" value="364"/>
							<Property name="Width" type="int" value="150"/>
							<Property name="Height" type="int" value="75"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="11" flip="false" index="3" name="PID 1" type="pid_controller">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="k" type="real" value="-2"/>
							<Property name="Ti" type="real" value="22"/>
							<Property name="Td" type="real" value="1.125"/>
							<Property name="Differentiate inertia" type="real" value="0.01"/>
							<Property name="Output low limit" type="real" value="-2"/>
							<Property name="Output high limit" type="real" value="2"/>
							<Property name="Initial output" type="real" value="0"/>
							<Property name="Bias" type="real" value="0"/>
							<Property name="Setpoint derivative" type="bool" value="true"/>
							<Property name="Setpoint proportional" type="bool" value="true"/>
							<Property name="Stop integrate on limits" type="bool" value="true"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="255"/>
							<Property name="Left" type="int" value="126"/>
							<Property name="Top" type="int" value="290"/>
							<Property name="Width" type="int" value="80"/>
							<Property name="Height" type="int" value="45"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="13" flip="false" index="4" name="madel zaklocenia  Gz" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="16776960"/>
							<Property name="Left" type="int" value="673"/>
							<Property name="Top" type="int" value="187"/>
							<Property name="Width" type="int" value="150"/>
							<Property name="Height" type="int" value="45"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="15" flip="false" index="5" name="Model Procesu  Gp" type="subpath">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="16776960"/>
							<Property name="Left" type="int" value="624"/>
							<Property name="Top" type="int" value="325"/>
							<Property name="Width" type="int" value="150"/>
							<Property name="Height" type="int" value="45"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="17" flip="false" index="6" name="Constant 1" type="constant">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Value" type="real" value="0"/>
							<Property name="Min" type="real" value="-1"/>
							<Property name="Max" type="real" value="1"/>
							<Property name="Limit output" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="596"/>
							<Property name="Top" type="int" value="194"/>
							<Property name="Width" type="int" value="50"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="18" flip="false" index="7" name="Display 2" type="display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="2500"/>
							<Property name="Inputs count" type="uint" value="2"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="667"/>
							<Property name="Top" type="int" value="58"/>
							<Property name="Width" type="int" value="170"/>
							<Property name="Height" type="int" value="60"/>
							<Property name="Chart min" type="real" value="0"/>
							<Property name="Chart max" type="real" value="0.206119446044297"/>
							<Property name="Autoscale" type="bool" value="true"/>
							<Property name="Scale each sep." type="bool" value="false"/>
							<Property name="Precision" type="int" value="3"/>
							<Property name="Digits" type="int" value="3"/>
							<Property name="Type" type="enum" value="1">
								<Enumeration index="0" name="Fixed"/>
								<Enumeration index="1" name="General"/>
								<Enumeration index="2" name="Scientific"/>
							</Property>
							<Property name="Bkg color" type="color" value="-16777198"/>
							<Property name="Axes color" type="color" value="8421504"/>
							<Property name="Bad sample color" type="color" value="255"/>
							<Property name="Show sample nr" type="bool" value="false"/>
							<Property name="Color scheme" type="enum" value="0">
								<Enumeration index="0" name="dark"/>
								<Enumeration index="1" name="light"/>
							</Property>
						</Properties>
					</Display>
				</Object>
				<Object ID="22" flip="false" index="8" name="Sum 4" type="sum">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Signs" type="string" value="++"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="-16777214"/>
							<Property name="Left" type="int" value="860"/>
							<Property name="Top" type="int" value="304"/>
							<Property name="Width" type="int" value="60"/>
							<Property name="Height" type="int" value="60"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="23" flip="false" index="9" name="Display 1" type="display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="900"/>
							<Property name="Inputs count" type="uint" value="1"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="1008"/>
							<Property name="Top" type="int" value="315"/>
							<Property name="Width" type="int" value="170"/>
							<Property name="Height" type="int" value="45"/>
							<Property name="Chart min" type="real" value="-0.492779880555331"/>
							<Property name="Chart max" type="real" value="1.54176475435084"/>
							<Property name="Autoscale" type="bool" value="false"/>
							<Property name="Scale each sep." type="bool" value="false"/>
							<Property name="Precision" type="int" value="3"/>
							<Property name="Digits" type="int" value="3"/>
							<Property name="Type" type="enum" value="1">
								<Enumeration index="0" name="Fixed"/>
								<Enumeration index="1" name="General"/>
								<Enumeration index="2" name="Scientific"/>
							</Property>
							<Property name="Bkg color" type="color" value="0"/>
							<Property name="Axes color" type="color" value="8421504"/>
							<Property name="Bad sample color" type="color" value="255"/>
							<Property name="Show sample nr" type="bool" value="false"/>
							<Property name="Color scheme" type="enum" value="0">
								<Enumeration index="0" name="dark"/>
								<Enumeration index="1" name="light"/>
							</Property>
						</Properties>
					</Display>
				</Object>
				<Object ID="14" flip="false" index="10" name="Random 1" type="random">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Minimum" type="real" value="-0.5"/>
							<Property name="Maximum" type="real" value="0.5"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="467"/>
							<Property name="Top" type="int" value="83"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="16" flip="false" index="11" name="Periodical signal 1" type="src_periodic">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Frequency" type="real" value="0.001"/>
							<Property name="Phase" type="real" value="0"/>
							<Property name="Amplitude" type="real" value="1.1"/>
							<Property name="Bias" type="real" value="0"/>
							<Property name="Function" type="enum" value="1">
								<Enumeration index="0" name="sawtooth"/>
								<Enumeration index="1" name="sinus"/>
								<Enumeration index="2" name="square"/>
							</Property>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="15780518"/>
							<Property name="Left" type="int" value="141"/>
							<Property name="Top" type="int" value="188"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="30"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="21" buffer_type="1" flip="false" index="12" name="Internal variable 1" type="internal_out_var">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer size" type="int" value="1"/>
							<Property name="Buffer name" type="string" value="Buffer"/>
							<Property name="Auto commit" type="bool" value="false"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="311"/>
							<Property name="Top" type="int" value="285"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="37"/>
						</Properties>
					</Display>
				</Object>
				<Object ID="19" flip="false" index="13" name="XY Display 1" type="xy_display">
					<Transform>
						<Properties>
							<Property name="Processing rate" type="int" value="1"/>
							<Property name="Buffer length" type="uint" value="1000"/>
						</Properties>
					</Transform>
					<Display>
						<Properties>
							<Property name="Color" type="color" value="12639424"/>
							<Property name="Left" type="int" value="547"/>
							<Property name="Top" type="int" value="427"/>
							<Property name="Width" type="int" value="100"/>
							<Property name="Height" type="int" value="66"/>
							<Property name="X axis min" type="real" value="-0.925899177789815"/>
							<Property name="X axis max" type="real" value="1.03776272897627"/>
							<Property name="Y axis min" type="real" value="-1.04"/>
							<Property name="Y axis max" type="real" value="1.04"/>
							<Property name="Chart bkg color" type="color" value="-16777198"/>
							<Property name="Chart axes color" type="color" value="-16777187"/>
							<Property name="Chart plot color" type="color" value="15793151"/>
							<Property name="Chart bad sample color" type="color" value="255"/>
						</Properties>
					</Display>
				</Object>
			</Objects>
			<Connections>
				<Connection begin_id="13" begin_idx="0" end_id="22" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="29"/>
							<Segment index="1" value="115"/>
							<Segment index="2" value="13"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="22" begin_idx="0" end_id="11" end_idx="1" type="1">
					<Display type="1">
						<Segments size="5">
							<Segment index="0" value="22"/>
							<Segment index="1" value="183"/>
							<Segment index="2" value="-877"/>
							<Segment index="3" value="-197"/>
							<Segment index="4" value="66"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="1" begin_idx="0" end_id="7" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="135"/>
							<Segment index="1" value="-21"/>
							<Segment index="2" value="44"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="2" begin_idx="0" end_id="7" end_idx="2" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="153"/>
							<Segment index="1" value="-60"/>
							<Segment index="2" value="30"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="7" begin_idx="0" end_id="18" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="102"/>
							<Segment index="1" value="-303"/>
							<Segment index="2" value="89"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="17" begin_idx="0" end_id="13" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="12"/>
							<Segment index="1" value="0"/>
							<Segment index="2" value="20"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="11" begin_idx="0" end_id="21" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="52"/>
							<Segment index="1" value="-9"/>
							<Segment index="2" value="58"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="22" begin_idx="0" end_id="23" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="46"/>
							<Segment index="1" value="3"/>
							<Segment index="2" value="47"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="7" begin_idx="0" end_id="15" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="120"/>
							<Segment index="1" value="-54"/>
							<Segment index="2" value="28"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="15" begin_idx="0" end_id="22" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="44"/>
							<Segment index="1" value="-3"/>
							<Segment index="2" value="47"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="7" begin_idx="0" end_id="19" end_idx="1" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="29"/>
							<Segment index="1" value="70"/>
							<Segment index="2" value="42"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="16" begin_idx="0" end_id="7" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="49"/>
							<Segment index="1" value="179"/>
							<Segment index="2" value="46"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="16" begin_idx="0" end_id="19" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="296"/>
							<Segment index="1" value="246"/>
							<Segment index="2" value="15"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
				<Connection begin_id="16" begin_idx="0" end_id="18" end_idx="0" type="1">
					<Display type="1">
						<Segments size="3">
							<Segment index="0" value="223"/>
							<Segment index="1" value="-125"/>
							<Segment index="2" value="208"/>
						</Segments>
					</Display>
					<Properties/>
				</Connection>
			</Connections>
		</PATH>
	</Paths>
</CalcPaths_Export_File>
